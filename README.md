This project is a prototype of a strategy game in the making, which I also use to play with new Unity features like ECS and ShaderGraph, and cover them with my own layer of utils and mini-frameworks.

Hex Map Generation 
======

This is the project's most prominent feature right now. The system uses 2D tilemaps from the [Tiled editor](https://www.mapeditor.org/) as a config to generate the tilemaps with 3D tiles constrained to 2D plane in Unity, both on runtime and editor time.

![Image](config_to_tiles.png)

At runtime, tilemap is a ECS framework where tile = entity, meant to be extended with the future gameplay per-tile data. Hybrid ECS is used for rendering.

At editor time, Component System is used together with editor scripting, to generate preview GameObjects for the tiles. They will then be converted in the Start() to ECS representation from the first approach. This way you can further tweak individual tiles in the editor after placing them using Tiled map, constrained only by the amount of changes that are supported by your GameObject to Entity conversion.

Math
--------
Cube coordinate system is used for hexes (perfectly explained [here](https://www.redblobgames.com/grids/hexagons/)). HexMath static class holds the implementation.







