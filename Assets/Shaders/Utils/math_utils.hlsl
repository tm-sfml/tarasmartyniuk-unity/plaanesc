#ifndef MATH_UTILS
#define MATH_UTILS

const float eps = 0.01;
bool AreNearlyEqualF(float f1, float f2)
{
	return abs(f1 - f2) <= eps;
}

#endif