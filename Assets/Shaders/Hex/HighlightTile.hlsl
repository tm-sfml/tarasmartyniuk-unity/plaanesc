#ifndef HIGHLIGHT_TILE_INCLUDED 
#define HIGHLIGHT_TILE_INCLUDED

//#define COMP_DEBUG


#ifdef COMP_DEBUG
// debugging using compute buffer
uniform RWStructuredBuffer<float4> _DebugBuffer : register(u1);
#endif

// integral part (first 3 digits) is written as Color.R, fract part (also 3 digits) as Color.G
// unity lighted shaders use half3 for fragments, and clip to [0,1], so we have only 
// 3 digits of precision and cannot use the integral part at all - so 3 digits per float color value
float3 DebugOutput(float value, uint integralDigits = 3)
{
	// only 3 last digits, sorry
	if (integralDigits > 3 || integralDigits == 0)
	{
		return -1.f;
	}
	
	float max = pow(10, integralDigits);
	if (value >= max)
	{
		value %= max;
	}
	
	float integralCompressed = value / max;
	return float3(integralCompressed, frac(value), 0.f);
}

//int3 Cube(int x, int z)
//{
//	return int3(x, -x - z, z);
//}

//// hex math
//// fractCoords - Cube coords with float values</param>
//int3 CubeRound(float3 fractCoords)
//{
//	float3 rounded = round(fractCoords);
//	float3 diff = abs(rounded - fractCoords);

//	if (diff.x > diff.y && diff.x > diff.z)
//	{
//		rounded.x = -rounded.y - rounded.z;
//	}
//	else if (diff.z > diff.y)
//	{
//		rounded.z = -rounded.x - rounded.y;
//	}

//	return Cube((int) rounded.x, (int) rounded.z);
//}

float3 PlanePointToHexCoord(float2 p, float hexRadius, float2x2 inverseAxialBasis)
{
    // x, z cube
	float2 fractCoords = mul(inverseAxialBasis, p) / hexRadius;
    // fract coords are still subject to coord system constraint
	float fractCoordsY = -fractCoords.x - fractCoords.y;
	//return CubeRound(float3(fractCoords.x, fractCoordsY, fractCoords.y));
	return float3(fractCoords.x, fractCoordsY, fractCoords.y);
}

//float3 OffsetToCube(float2 offset)
//{
//	float x = offset.x;
//        // - y since our cube +Z is inverted with regards to what Amit uses
//	float z = -offset.y - (offset.x - (offset.x & 1)) / 2;
//	float y = -x - z;
//	return float3(x, y, z);
//}

float CubeDistanceToOrigin(float3 cube)
{
	return max(max(cube.x, cube.y), cube.z);
}

void HighlightTile_float(
	float4 Color, float4 HiglightColor, float MinRadius,
	float3 Normal, float3 Position, 
	// matrix2x2
	float4 InverseAxialBasis,
	out float3 Albedo, 
	out float3 Emission)
{
	Albedo = Color;
	
	float hdrIntensityMultiplier = (HiglightColor.x + HiglightColor.y + HiglightColor.z) / 3.f;
	//_DebugBuffer[0] = float3(hdrIntensityMultiplier, .5, .5);
	
	// HACK, pass Hex Radius
	const float radius = 1.f;
	//float3 offsetCoords = OffsetToCube(Position.xz);
	
	
	float2x2 invAxialBasis = { InverseAxialBasis.xy, InverseAxialBasis.zw};
#ifdef COMP_DEBUG
	_DebugBuffer[0] = InverseAxialBasis;
	_DebugBuffer[1] = float4(invAxialBasis[0], invAxialBasis[1]);
	
	
#endif
	
	
	
	float2 pos = { Position.x, -Position.z };
	
	float3 cube = PlanePointToHexCoord(pos, radius, invAxialBasis);
	
	Albedo = cube * 255;
	float dist = CubeDistanceToOrigin(cube);
	
	//float dist = length(Position.xz);
	MinRadius = .2f;
	
	float desiredIntensity = 0.f;
	if (dist > MinRadius)
	{
		desiredIntensity = (dist - MinRadius) / (radius - MinRadius);
	}
	
	Emission = 0;//HiglightColor.xyz * desiredIntensity;
}
#endif