﻿using System;
using UnityEngine;
using UnityEngine.Rendering.Universal;

[Serializable]
public struct RenderSettings
{
    public Material OverrideMaterial;
    [Space]
    public RenderLayerMask RenderLayerMask;
}

[Serializable]
public struct BlurSettings
{
    public Material BlurMaterial;
    public int DownSample;
    public int PassesCount;
}

public class OutlineFeature : ScriptableRendererFeature
{
    [SerializeField] RenderPassEvent _renderPassEvent;
    [SerializeField] Material _outlineMaterial;
    [SerializeField] RenderSettings _renderSettings;
    [SerializeField] BlurSettings _blurSettings;


    // synced with shader
    const string _renderTextureName = "_OutlineRenderTexture";
    const string _bluredTextureName = "_OutlineBluredTexture";

    RenderTargetHandle _bluredTexture;
    RenderTargetHandle _renderTexture;

    OutlineRenderObjectsPass _renderPass;
    BlurPass _blurPass;
    OutlinePass _outlinePass;

    OutlineFeature()
    {
        _renderSettings.RenderLayerMask = RenderLayerMask.Outline;
        _blurSettings.DownSample = 1;
        _blurSettings.PassesCount = 2;
    }


    public override void Create()
    {
        _renderTexture.Init(_renderTextureName);
        _bluredTexture.Init(_bluredTextureName);

        _renderPass = new OutlineRenderObjectsPass(_renderTexture, _renderSettings);
        _blurPass = new BlurPass(_renderTexture.Identifier(), _bluredTexture, _blurSettings);
        _outlinePass = new OutlinePass(_outlineMaterial);

        _renderPass.renderPassEvent = _renderPassEvent;
        _blurPass.renderPassEvent = _renderPassEvent;
        _outlinePass.renderPassEvent = _renderPassEvent;
    }

    public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
    {
        //_renderPass.CameraDepth = renderer.cameraDepthTarget;
        _renderPass.Renderer = renderer;
        renderer.EnqueuePass(_renderPass);
        renderer.EnqueuePass(_blurPass);
        renderer.EnqueuePass(_outlinePass);
    }
}
