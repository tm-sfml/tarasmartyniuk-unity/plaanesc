﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class OutlineRenderObjectsPass : ScriptableRenderPass
{
     RenderTargetHandle _destination;
     Material _overrideMaterial;
     List<ShaderTagId> _shaderTagIdList = new List<ShaderTagId>() { new ShaderTagId("UniversalForward") };
     FilteringSettings _filteringSettings;

    public ScriptableRenderer Renderer { private get; set; }

    public OutlineRenderObjectsPass(RenderTargetHandle destination, RenderSettings renderSettings)
    {
        _destination = destination;
        _overrideMaterial = renderSettings.OverrideMaterial;
       LayerMask everything = -1;
        _filteringSettings = new FilteringSettings(RenderQueueRange.opaque, everything, (uint) renderSettings.RenderLayerMask);
    }

    public override void Configure(CommandBuffer cmdBuffer, RenderTextureDescriptor cameraTextureDescriptor)
    {
        cmdBuffer.GetTemporaryRT(_destination.id, cameraTextureDescriptor);
        ConfigureTarget(_destination.Identifier(), Renderer.cameraDepthTarget);
        ConfigureClear(ClearFlag.Color, Color.clear);
    }

    public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
    {
        SortingCriteria sortingCriteria = renderingData.cameraData.defaultOpaqueSortFlags;
        DrawingSettings drawingSettings = CreateDrawingSettings(_shaderTagIdList, ref renderingData, sortingCriteria);
        drawingSettings.overrideMaterial = _overrideMaterial;

        var renderStateBlock = new RenderStateBlock(RenderStateMask.Nothing);
        context.DrawRenderers(renderingData.cullResults, ref drawingSettings, ref _filteringSettings, ref renderStateBlock);
    }
}
