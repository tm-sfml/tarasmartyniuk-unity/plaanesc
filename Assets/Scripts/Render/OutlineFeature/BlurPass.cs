﻿using System;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class BlurPass : ScriptableRenderPass
{
    int _tmpBlurRTId1 = Shader.PropertyToID("_TempBlurTexture1");
    int _tmpBlurRTId2 = Shader.PropertyToID("_TempBlurTexture2");

    RenderTargetIdentifier _tmpBlurRT1;
    RenderTargetIdentifier _tmpBlurRT2;

    RenderTargetIdentifier _source;
    RenderTargetHandle _destination;

    BlurSettings _blurSettings;

    public BlurPass(RenderTargetIdentifier source, RenderTargetHandle destination, BlurSettings blurSettings)
    {
        _source = source;
        _destination = destination;
        _blurSettings = blurSettings;
    }

    public override void Configure(CommandBuffer cmdBuffer, RenderTextureDescriptor cameraTextureDescriptor)
    {
        var width = Mathf.Max(1, cameraTextureDescriptor.width >> _blurSettings.DownSample);
        var height = Mathf.Max(1, cameraTextureDescriptor.height >> _blurSettings.DownSample);
        var blurTextureDesc = new RenderTextureDescriptor(width, height, RenderTextureFormat.ARGB32, 0, 0);

        _tmpBlurRT1 = new RenderTargetIdentifier(_tmpBlurRTId1);
        _tmpBlurRT2 = new RenderTargetIdentifier(_tmpBlurRTId2);

        cmdBuffer.GetTemporaryRT(_tmpBlurRTId1, blurTextureDesc, FilterMode.Bilinear);
        cmdBuffer.GetTemporaryRT(_tmpBlurRTId2, blurTextureDesc, FilterMode.Bilinear);
        cmdBuffer.GetTemporaryRT(_destination.id, blurTextureDesc, FilterMode.Bilinear);

        ConfigureTarget(_destination.Identifier());
    }

    public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
    {
        var cmdBuffer = CommandBufferPool.Get("BlurPass");

        if (_blurSettings.PassesCount > 0)
        {
            cmdBuffer.Blit(_source, _tmpBlurRT1, _blurSettings.BlurMaterial, 0);
            for (int i = 0; i < _blurSettings.PassesCount - 1; i++)
            {
                cmdBuffer.Blit(_tmpBlurRT1, _tmpBlurRT2, _blurSettings.BlurMaterial, 0);
                var t = _tmpBlurRT1;
                _tmpBlurRT1 = _tmpBlurRT2;
                _tmpBlurRT2 = t;
            }
            cmdBuffer.Blit(_tmpBlurRT1, _destination.Identifier());
        }
        else
            cmdBuffer.Blit(_source, _destination.Identifier());

        context.ExecuteCommandBuffer(cmdBuffer);
        CommandBufferPool.Release(cmdBuffer);
    }
}
