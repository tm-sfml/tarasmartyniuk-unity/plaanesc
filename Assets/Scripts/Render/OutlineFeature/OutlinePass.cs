﻿using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class OutlinePass : ScriptableRenderPass
{
     string _profilerTag = "Outline";
     Material _material;

    public OutlinePass(Material material)
    {
        _material = material;
    }

    public override void OnCameraSetup(CommandBuffer cmd, ref RenderingData renderingData)
    {
        base.OnCameraSetup(cmd, ref renderingData);
    }

    public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
    {
        var cmdBufferBuffer = CommandBufferPool.Get(_profilerTag);

        using (new ProfilingScope(cmdBufferBuffer, new ProfilingSampler(_profilerTag)))
        {
            var mesh = RenderingUtils.fullscreenMesh;
            cmdBufferBuffer.DrawMesh(mesh, Matrix4x4.identity, _material, 0, 0);
        }

        context.ExecuteCommandBuffer(cmdBufferBuffer);
        CommandBufferPool.Release(cmdBufferBuffer);
    }
}
