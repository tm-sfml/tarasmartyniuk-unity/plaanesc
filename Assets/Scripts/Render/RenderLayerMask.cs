﻿public enum RenderLayerMask
{
    Default = 1,
    Outline = 2
}
