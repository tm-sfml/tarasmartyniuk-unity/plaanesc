﻿using ImGuiNET;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

class Battle3CDebugImguiPage : IImguiPage
{
    public bool IsOpen { get => m_isOpen; set => m_isOpen = value; }
    bool m_isOpen;
    List<(string Label, string Value)> m_3CMemebers = new List<(string Label, string Value)>();

    public void Show()
    {
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;

        if (!ImGui.Begin("Battle3CSystemDebug", ref m_isOpen))
        {
            ImGui.End();
            return;
        }

        ImguiDebugUtils.ShowTogglePauseCheckBox();
        ImguiDebugUtils.ShowSingletonComponentHeader<Battle3CComponent>();

        if (ImGui.CollapsingHeader("Battle3CSystem members", ImGuiTreeNodeFlags.DefaultOpen))
        {
            var system = World.DefaultGameObjectInjectionWorld.GetExistingSystem<Battle3CSystem>();
            system.GetDebugState(m_3CMemebers);

            foreach ((var label, var value) in m_3CMemebers)
            {
                ImGui.Text($"{label}: ");
                ImGui.SameLine();
                ImGui.PushStyleColor(ImGuiCol.Text, Color.yellow.ToUint());
                ImGui.TextWrapped(value);
                ImGui.PopStyleColor();
            }
        }
        ImGui.End();
    }
}
