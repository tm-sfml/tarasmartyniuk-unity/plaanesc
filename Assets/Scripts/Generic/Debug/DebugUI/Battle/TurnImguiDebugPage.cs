﻿using ImGuiNET;

class TurnImguiDebugPage : IImguiPage
{
    public bool IsOpen { get => m_isOpen; set => m_isOpen = value; }
    private bool m_isOpen;

    public void Show()
    {
        if (!ImGui.Begin("Turn Debug", ref m_isOpen))
        {
            ImGui.End();
            return;
        }
        ImguiDebugUtils.ShowTogglePauseCheckBox();
        ImguiDebugUtils.ShowSingletonComponentHeader<BattleTurnComponent>();
        ImGui.End();
    }
}
