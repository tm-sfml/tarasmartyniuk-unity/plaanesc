﻿using ImGuiNET;
using Newtonsoft.Json;
using System.Linq;
using Unity.Entities;
using UnityEngine;

class HighlightSystemImguiDebugPage : IImguiPage
{
    public bool IsOpen { get => m_isOpen; set => m_isOpen = value; }
    bool m_isOpen;

    public void Show()
    {
        if (!ImGui.Begin("Highlight System Debug", ref m_isOpen))
        {
            ImGui.End();
            return;
        }

        ImguiDebugUtils.ShowTogglePauseCheckBox();

        var highlightEntity = EntityQueryUtils.GetFirstEntityWithComponent<HexHighlightRequestComponent>();
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;

        var currentHighlightedHexes = entityManager.GetBuffer<CurrentHighlightedHexesBufferElement>(highlightEntity).AsNativeArray();
        var hexesToHighlight = entityManager.GetBuffer<HexesToHighlightBufferElement>(highlightEntity).AsNativeArray();
        var hexesToDeHighlight = entityManager.GetBuffer<HexesToDehighlightBufferElement>(highlightEntity).AsNativeArray();

        ImGui.TextWrapped($"currentHighlightedHexes: {JsonConvert.SerializeObject(currentHighlightedHexes)}");
        ImGui.TextWrapped($"hexesToHighlight: {JsonConvert.SerializeObject(hexesToHighlight)}");
        ImGui.TextWrapped($"hexesToDeHighlight: {JsonConvert.SerializeObject(hexesToDeHighlight)}");
        ImGui.End();
    }
}
