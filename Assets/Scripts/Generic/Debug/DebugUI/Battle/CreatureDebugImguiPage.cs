﻿using ImGuiNET;
using Newtonsoft.Json;
using System;
using Unity.Entities;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.InputSystem;

class CreatureDebugImguiPage : IImguiPage
{
    public bool IsOpen
    {
        get => m_isOpen;
        set
        {
            m_isOpen = value;
            if (value)
            {
                m_lockCreatureUnderMouseAction.Enable();
            }
            else
            {
                m_lockCreatureUnderMouseAction.Disable();
            }
        }
    }

    InputAction m_lockCreatureUnderMouseAction;

    public CreatureDebugImguiPage(InputAction lockCreatureUnderMouseAction)
    {
        m_lockCreatureUnderMouseAction = lockCreatureUnderMouseAction;
        m_lockCreatureUnderMouseAction.performed += (_ =>
        {
            var entityUnderMouse = GetCreatureEntityUnderMouse();
            if (entityUnderMouse == Entity.Null)
            {
                Debug.LogError("Trying to lock entity under mouse when it is null");
                return;
            }

            m_creatureEntityIndexInput = Convert.ToString(entityUnderMouse.Index);
            m_creatureEntityVersionInput = Convert.ToString(entityUnderMouse.Version);
        });


        m_creatureEntityIndexInput = "";
        m_creatureEntityVersionInput = "";
    }

    bool m_isOpen;
    bool m_displayCreatureUnderMouse;

    string m_creatureEntityIndexInput;
    string m_creatureEntityVersionInput;

    public void Show()
    {
        if (!ImGui.Begin("Creature Debug", ref m_isOpen))
        {
            ImGui.End();
            return;
        }
        ImguiDebugUtils.ShowTogglePauseCheckBox();
        ImGui.Checkbox("Manual creature entity (displaying creature under mouse otherwise)", ref m_displayCreatureUnderMouse);

        if (ImGui.CollapsingHeader("Creature under mouse", ImGuiTreeNodeFlags.DefaultOpen))
        {
            var entityToDisplay = GetCreatureEntityUnderMouse();
            ImGui.Text($"Creature: {entityToDisplay}");
            DrawCreatureData(entityToDisplay);
        }

        if (ImGui.CollapsingHeader("Find creature manually", ImGuiTreeNodeFlags.DefaultOpen))
        {
            uint maxSize = 10;
            ImGui.InputText("Entity Index:", ref m_creatureEntityIndexInput, maxSize);
            bool parsed = int.TryParse(m_creatureEntityIndexInput, out int index);

            ImGui.InputText("Entity Version:", ref m_creatureEntityVersionInput, maxSize);
            parsed &= int.TryParse(m_creatureEntityVersionInput, out int version);

            if (parsed)
            {
                Entity entityToDisplay = new Entity { Index = index, Version = version };
                DrawCreatureData(entityToDisplay);
            }
        }

        ImGui.End();
    }

    void DrawCreatureData(Entity creature)
    {
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;

        if (!entityManager.Exists(creature))
        {
            if (creature != Entity.Null)
            {
                ImGui.TextColored(Color.red, "Entity does not exist");
            }
            return;
        }

        try
        {
            if (ImGui.CollapsingHeader("Creature Component", ImGuiTreeNodeFlags.DefaultOpen))
            {
                var creatureComponent = entityManager.GetComponentData<CreatureComponent>(creature);
                var json = JsonConvert.SerializeObject(creatureComponent, Formatting.Indented);
                ImGui.Text(json);
            }
            if (ImGui.CollapsingHeader("Action Points Component", ImGuiTreeNodeFlags.DefaultOpen))
            {
                var actionPointsComponent = entityManager.GetComponentData<ActionPointsComponent>(creature);
                var json = JsonConvert.SerializeObject(actionPointsComponent, Formatting.Indented);
                ImGui.Text(json);
            }
            if (ImGui.CollapsingHeader("Abilities", ImGuiTreeNodeFlags.DefaultOpen))
            {
                var abilities = entityManager.GetBuffer<AbilityBufferElement>(creature).AsNativeArray();
                if (abilities.Length != 0)
                {
                    var json = JsonConvert.SerializeObject(abilities, Formatting.Indented);
                    ImGui.Text($"abilities: {json}");

                    static Type AbilityIdToComponentType(AbilityType id)
                    {
                        return id switch
                        {
                            AbilityType.Attack => typeof(AttackAbilityComponent),
                            _ => null,
                        };
                    }

                    foreach (var ability in abilities)
                    {
                        Type abilityComponentType = AbilityIdToComponentType(ability.Id);
                        if (abilityComponentType != null)
                        {
                            var component = typeof(EntityManager).GetMethod("GetComponentData").MakeGenericMethod(abilityComponentType)
                                .Invoke(entityManager, new object[] { creature });

                            var casted = Convert.ChangeType(component, abilityComponentType);
                            if (ImGui.CollapsingHeader(abilityComponentType.Name, ImGuiTreeNodeFlags.DefaultOpen))
                            {
                                ImGui.Text(JsonConvert.SerializeObject(casted, Formatting.Indented));
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
    }

    Entity GetCreatureEntityUnderMouse()
    {
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        var hexOrigin = EntityQueryUtils.GetFirstEntityWithComponentLogFailure<HexMapComponent>();
        var hexComponent = entityManager.GetComponentData<HexMapComponent>(hexOrigin);
        var coordToHex = HexUtils.GetCoordToHexArray(hexOrigin);
        (var _, Entity hexUnderMouse) = Battle3CSystem.GetCreatureAndHexUnderMouse(coordToHex, hexComponent,
            HexUtils.GetHexCoordAtScreenPoint(Input.mousePosition, hexComponent), entityManager);

        Entity entityToDisplay = Entity.Null;
        if (hexUnderMouse != Entity.Null)
        {
            var battleHexComponent = entityManager.GetComponentData<BattleHexComponent>(hexUnderMouse);
            entityToDisplay = battleHexComponent.OccupyingCreature;
        }

        return entityToDisplay;
    }
}
