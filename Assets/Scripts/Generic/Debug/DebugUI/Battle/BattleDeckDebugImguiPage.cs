﻿using ImGuiNET;
using System;
using Unity.Entities;
using UnityEngine;

class BattleDeckDebugImguiPage : IImguiPage
{
    public bool IsOpen
    {
        get => m_isOpen; set
        {
            m_isOpen = value;
            if (m_isOpen)
            {
                Reset();
            }
        }
    }
    bool m_isOpen;
    int m_currentSelectedTeamIndex;
    bool m_showCurrentActingTeamDeck;

    public void Show()
    {
        if (!ImGui.Begin("BattleDeckDebug", ref m_isOpen))
        {
            ImGui.End();
            return;
        }

        ImGui.Checkbox("Show current acting team deck", ref m_showCurrentActingTeamDeck);

        Team deckTeam;
        if (m_showCurrentActingTeamDeck)
        {
            deckTeam = EntityQueryUtils.GetSingleton<BattleTurnComponent>().CurrentActingTeam;
        }
        else
        {
            var teamValues = Enum.GetNames(typeof(Team));
            ImGui.Combo("Team", ref m_currentSelectedTeamIndex, teamValues, teamValues.Length);
            bool success = Enum.TryParse(teamValues[m_currentSelectedTeamIndex], out deckTeam);
            Debug.Assert(success);
        }

        ImguiDebugUtils.ShowTogglePauseCheckBox();

        var battleDataEntity = BattleDataUtils.GetBattleDataEntityForTeam(deckTeam);
        if (battleDataEntity != Entity.Null)
        {
            var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            var battleDeckEntity = entityManager.FindChildWithComponent<BattleDeckConfigComponent>(battleDataEntity);
            ImguiDebugUtils.ShowBufferHeader<DrawPileBufferElement>(battleDeckEntity);
            ImguiDebugUtils.ShowBufferHeader<HandBufferElement>(battleDeckEntity);
            ImguiDebugUtils.ShowBufferHeader<DiscardPileBufferElement>(battleDeckEntity);
            ImguiDebugUtils.ShowComponentHeader(entityManager.GetComponentData<BattleDeckConfigComponent>(battleDeckEntity));

        }
        ImGui.End();
    }

    void Reset()
    {
        var turnComponent = EntityQueryUtils.GetSingleton<BattleTurnComponent>();
        m_currentSelectedTeamIndex = (int) turnComponent.CurrentActingTeam;
        m_showCurrentActingTeamDeck = true;
    }
}
