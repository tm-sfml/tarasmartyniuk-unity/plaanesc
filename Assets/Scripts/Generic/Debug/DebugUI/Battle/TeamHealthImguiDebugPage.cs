﻿using ImGuiNET;
using Newtonsoft.Json;

class TeamHealthImguiDebugPage : IImguiPage
{
    public bool IsOpen { get => m_isOpen; set => m_isOpen = value; }
    private bool m_isOpen;

    public void Show()
    {
        if (!ImGui.Begin("Team Health", ref m_isOpen))
        {
            ImGui.End();
            return;
        }
        ImguiDebugUtils.ShowTogglePauseCheckBox();

        void ShowHealth(Team team)
        {
            (var health, bool _) = EntityQueryUtils.GetSingleComponentLogFailure((TeamHealthComponent teamHealth) => teamHealth.Team == team);
            if (ImGui.CollapsingHeader(team.ToString(), ImGuiTreeNodeFlags.DefaultOpen))
            {
                var json = JsonConvert.SerializeObject(health, Formatting.Indented);
                ImGui.Text(json);
            }
        }
        ShowHealth(Team.Player);
        ShowHealth(Team.Enemy);
        ImGui.End();
    }
}
