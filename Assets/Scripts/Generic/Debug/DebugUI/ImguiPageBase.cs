﻿
public interface IImguiPage
{
    bool IsOpen { get; set; }
    // returns a bool denoting if the page is left open at the end of the method
    void Show();
}
