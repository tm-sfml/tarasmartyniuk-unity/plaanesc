﻿using ImGuiNET;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using Unity.Entities;

public static class ImguiDebugUtils
{
    public static void ShowSingletonComponentHeader<TComponent>()
    where TComponent : struct, IComponentData => 
        ShowComponentHeader(EntityQueryUtils.GetSingleton<TComponent>());

    public static void ShowSingletonBufferHeader<TComponent>()
    where TComponent : struct, IBufferElementData
    {
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        ShowBufferHeader<TComponent>(entityManager.GetSingletonBuffer<TComponent>());
    }

    public static void ShowComponentHeader<TComponent>(TComponent component)
        where TComponent : struct, IComponentData
    {
        if (ImGui.CollapsingHeader(typeof(TComponent).Name, ImGuiTreeNodeFlags.DefaultOpen))
        {
            var json = JsonConvert.SerializeObject(component, Formatting.Indented, new StringEnumConverter());
            ImGui.Text(json);
        }
    }

    public static void ShowBufferHeader<TComponent>(Entity entity)
        where TComponent : struct, IBufferElementData
    {
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        var buffer = entityManager.GetBuffer<TComponent>(entity);
        ShowBufferHeader(buffer);
    }

    public static void ShowBufferHeader<TComponent>(DynamicBuffer<TComponent> buffer)
        where TComponent : struct, IBufferElementData
    {
        if (ImGui.CollapsingHeader(typeof(TComponent).Name, ImGuiTreeNodeFlags.DefaultOpen))
        {
            ImGui.TextWrapped(JsonConvert.SerializeObject(buffer.AsNativeArray()));
        }
    }

    public static void ShowComponentText<TComponent>(TComponent component)
        where TComponent : struct, IComponentData
    {
        ImGui.Text($"{typeof(TComponent).Name}:");
        var json = JsonConvert.SerializeObject(component, Formatting.Indented, new StringEnumConverter());
        ImGui.Text(json);
    }


    public static void ShowTogglePauseCheckBox()
    {
        bool isPausedInput = PauseManager.Instance.IsPaused;
        ImGui.Checkbox("Game Paused:", ref isPausedInput);

        if (isPausedInput != PauseManager.Instance.IsPaused)
        {
            PauseManager.Instance.ApplyPause(isPausedInput);
        }
    }
}
