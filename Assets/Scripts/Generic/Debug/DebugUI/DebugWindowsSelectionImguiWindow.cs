﻿using ImGuiNET;
using System.Collections.Generic;
using UnityEngine.InputSystem;

// window for searching and opening any other debug window
// search is done with a text-input that filters the list of buttons for all available debug windows
public class DebugWindowsSelectionImguiWindow
{
    public bool IsOpen { get => m_isOpen; set => m_isOpen = value; }

    bool m_isOpen;
    ImGuiTextFilter m_filterData;

    // returns index of the selected DebugWindowName (the one for which a button is pressed)
    public int Show(List<string> debugWindowNames)
    {
        if (!ImGui.Begin("Debug Pages Finder", ref m_isOpen))
        {
            ImGui.End();
            return -1;
        }

        ImguiDebugUtils.ShowTogglePauseCheckBox();

        var filter = new ImGuiTextFilterPtr(ref m_filterData);
        ImGui.Text("Filter usage:\n" +
                    "  \"\"         display all lines\n" +
                    "  \"xxx\"      display lines containing \"xxx\"\n" +
                    "  \"xxx,yyy\"  display lines containing \"xxx\" or \"yyy\"\n" +
                    "  \"-xxx\"     hide lines containing \"xxx\"");
        filter.Draw();

        for (int i = 0; i < debugWindowNames.Count; i++)
        {
            if (filter.PassFilter(debugWindowNames[i]))
            {
                if (ImGui.Button(debugWindowNames[i]))
                {
                    return i;
                }
            }
        }

        ImGui.End();
        return -1;
    }
}
