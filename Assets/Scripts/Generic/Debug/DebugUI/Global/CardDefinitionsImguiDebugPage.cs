﻿using ImGuiNET;
using Newtonsoft.Json;
using Unity.Entities;
using UnityEngine;
using System;
using Newtonsoft.Json.Converters;

class CardDefinitionsImguiDebugPage : IImguiPage
{
    public bool IsOpen { get => m_isOpen; set => m_isOpen = value; }
    bool m_isOpen;

    public void Show()
    {
        if (!ImGui.Begin("Card Definitions Debug", ref m_isOpen))
        {
            ImGui.End();
            return;
        }

        ImguiDebugUtils.ShowTogglePauseCheckBox();

        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        var definitions = entityManager.GetSingletonBuffer<CardDefinitionBufferElement>();


        for (int i = 0; i < definitions.Length; i++)
        {
            string cardDefName = Enum.GetName(typeof(CardDefinitionId), EnumUtils.FromInt<CardDefinitionId>(i));
            if (ImGui.CollapsingHeader(cardDefName))
            {
                var cardDefComponent = entityManager.GetComponentData<CardDefinitionComponent>(definitions[i].Entity);
                ImguiDebugUtils.ShowComponentText(cardDefComponent);
                switch (cardDefComponent.CardBehavior)
                {
                    case CardBehavior.Creature:
                        ImguiDebugUtils.ShowComponentText(entityManager.GetComponentData<CreatureCardDefinitionComponent>(definitions[i].Entity));
                        break;
                    case CardBehavior.Spell_DirectDamageSingleTarget:
                        ImguiDebugUtils.ShowComponentText(entityManager.GetComponentData<Spell_DirectDamageDefinitionComponent>(definitions[i].Entity));
                        break;
                    default:
                        Debug.LogError("invalid card behavior");
                        break;
                }
            }
        }
    
        ImGui.End();
    }
}
