﻿using UnityEngine;
using ImGuiNET;
using System.Collections.Generic;
using UnityEngine.InputSystem;

public class DebugImgui : MonoBehaviour
{
    [SerializeField] InputAction ShowDebugWindowFinderAction;
    [SerializeField] InputAction Creature_LockCreatureUnderMouseAction;

    DebugWindowsSelectionImguiWindow m_debugWindowsSelectionWindow = new DebugWindowsSelectionImguiWindow();
    List<(string Name, IImguiPage Page)> m_debugPages = new List<(string, IImguiPage)>();
    List<string> m_debugPagesNames;
    bool m_unpausingNeeded;

    void Awake()
    {
        m_debugPages.Add(("Battle3C", new Battle3CDebugImguiPage()));
        m_debugPages.Add(("Creature", new CreatureDebugImguiPage(Creature_LockCreatureUnderMouseAction)));
        m_debugPages.Add(("HexHighlight System", new HighlightSystemImguiDebugPage()));
        m_debugPages.Add(("Turns", new TurnImguiDebugPage()));
        m_debugPages.Add(("BattleDeck", new BattleDeckDebugImguiPage()));
        m_debugPages.Add(("TeamHealth", new TeamHealthImguiDebugPage()));
        m_debugPages.Add(("CardDefinitions", new CardDefinitionsImguiDebugPage()));

        m_debugPagesNames = new List<string>(m_debugPages.Count);
        ShowDebugWindowFinderAction.performed += _ =>
        {
            m_debugWindowsSelectionWindow.IsOpen = !m_debugWindowsSelectionWindow.IsOpen;
            PauseManager.Instance.ApplyPause(true);
        };
        ShowDebugWindowFinderAction.Enable();
    }


    void OnEnable()
    {
        ImGuiUn.Layout += OnLayout;
    }

    void OnDisable()
    {
        ImGuiUn.Layout -= OnLayout;
    }

    void OnLayout()
    {
        if (m_debugWindowsSelectionWindow.IsOpen)
        {
            FillDebugPagesNames();
            int clickedPageNameIndex = m_debugWindowsSelectionWindow.Show(m_debugPagesNames);
            if (clickedPageNameIndex != -1)
            {
                var page = m_debugPages[clickedPageNameIndex].Page;
                page.IsOpen = true;
                m_debugWindowsSelectionWindow.IsOpen = false;
            }
            m_unpausingNeeded = true;
        }

        bool anyPageOpen = m_debugWindowsSelectionWindow.IsOpen;
        foreach (var (_, page) in m_debugPages)
        {
            if (page.IsOpen)
            {
                page.Show();
                m_unpausingNeeded = true;
                anyPageOpen = true;
            }
        }

        // all windows have just been closed
        if (!anyPageOpen && m_unpausingNeeded)
        {
            PauseManager.Instance.ApplyPause(false);
            Debug.Log("unpaused");
            m_unpausingNeeded = false;
        }
    }

    void FillDebugPagesNames()
    {
        m_debugPagesNames.Clear();
        foreach (var (name, _) in m_debugPages)
        {
            m_debugPagesNames.Add(name);
        }
    }
}
