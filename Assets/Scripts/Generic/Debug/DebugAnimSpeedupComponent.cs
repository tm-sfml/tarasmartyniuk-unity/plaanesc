using Unity.Entities;

[GenerateAuthoringComponent]
public struct DebugAnimSpeedupComponent : IComponentData
{
    public bool IsEnabled;
    public float SpeedupFactor;

    public static float GetDebugSpeed(float originalSpeed)
    {
        (var component, bool _) = EntityQueryUtils.GetFirstMatchingComponentLogFailure<DebugAnimSpeedupComponent>();
        return component.GetSpeed(originalSpeed);
    }

    public float GetSpeed(float originalSpeed)
    {
        return IsEnabled ? 
            originalSpeed / SpeedupFactor : originalSpeed;
    }
}
