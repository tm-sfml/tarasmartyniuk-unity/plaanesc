﻿using UnityEngine;
using Unity.Entities;

// handles pause functionality and input in-game
public class PauseManager : MonobehaviorSingletonBase<PauseManager>
{
    public bool IsTimePaused { get;  set;}
    public bool AreSystemsPaused { get;  set;}
    public bool IsPaused => IsTimePaused && AreSystemsPaused;


    public void ApplyPause(bool isPaused)
    {
        ApplyPauseForTime(isPaused);
        ApplyPauseForSystems(isPaused);
    }

    public void ApplyPauseForTime(bool isPaused)
    {
        Time.timeScale = isPaused ? 0f : 1f;
        IsTimePaused = isPaused;
    }

    public void ApplyPauseForSystems(bool isPaused)
    {
        foreach(var system in World.DefaultGameObjectInjectionWorld.Systems)
        {
            system.Enabled = !isPaused;
        }
        AreSystemsPaused = isPaused;
    }

    new void Awake()
    {
        base.Awake();
        DontDestroyOnLoad(this);    
    }

    void Update()
    {
        if (Input.GetButton("Cancel") && !IsTimePaused && !AreSystemsPaused)
        {
            //ApplyPause(true);
            // todo: use an event
            var objects = Resources.FindObjectsOfTypeAll<PauseMenuController>();
            var pauseMenu = objects[0].gameObject;
            pauseMenu.SetActive(true);
        }
    }


}
