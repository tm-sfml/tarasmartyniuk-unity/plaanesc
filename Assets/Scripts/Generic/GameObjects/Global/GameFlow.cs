﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Unity.Entities;

public class GameFlow : MonobehaviorSingletonBase<GameFlow>
{
    [SerializeField] int MainMenuSceneIndex;
    [SerializeField] int GameSceneIndex;

    public void TransitionFromMainMenuToGame() => ChangeScene(GameSceneIndex);
    public void TransitionFromGameToMainMenu() => ChangeScene(MainMenuSceneIndex);

    void Start()
    {
        SceneManager.sceneLoaded += (Scene _, LoadSceneMode __) => {
            SetEnabledForAllSystems(true);
        };
    }


    /// <summary>
    /// pauses all ecs systems until the scene is loaded
    /// </summary>
    void ChangeScene(int sceneIndex)
    {
        var world = World.DefaultGameObjectInjectionWorld;
        SetEnabledForAllSystems(false);

        SceneManager.LoadSceneAsync(sceneIndex);
        world.EntityManager.DestroyEntity(world.EntityManager.GetAllEntities());
    }

    void SetEnabledForAllSystems(bool areEnabled)
    {
        var world = World.DefaultGameObjectInjectionWorld;
        foreach (var system in world.Systems)
        {
            system.Enabled = areEnabled;
        }
    }
}
