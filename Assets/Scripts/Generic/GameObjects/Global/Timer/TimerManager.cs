﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class TimerManager : MonobehaviorSingletonBase<TimerManager>
{
    public const int k_InvalidTimerHandle = -1;
    public const int k_InitialCapacity = 100;

    List<(Timer Timer, bool IsTaken)> m_timers = new List<(Timer, bool)>(k_InitialCapacity);

    public int AquireTimer()
    {
        int firstAvailableIndex = m_timers.FindIndex((ValueTuple<Timer, bool> t) => !t.Item2);
        if (firstAvailableIndex == -1)
        {
            firstAvailableIndex = m_timers.Count;
            m_timers.Add(default);
        }
        (var timer, bool _) = m_timers[firstAvailableIndex];
        m_timers[firstAvailableIndex] = (timer, true);

        return firstAvailableIndex;
    }

    public RangeInt AquireTimers(int count)
    {
        int firstAvailableIndex = m_timers.FindIndex((ValueTuple<Timer, bool> t) => !t.Item2);

        int aquiredTimersStartIndex = 0;
        int numElementsToAdd = 0;

        if(firstAvailableIndex == -1)
        {
            aquiredTimersStartIndex = m_timers.Count;
            numElementsToAdd = count;
        }
        else
        {
            aquiredTimersStartIndex = firstAvailableIndex;
            numElementsToAdd = count - (m_timers.Count - firstAvailableIndex);
        }
        m_timers.AddRange(numElementsToAdd, (default, true));

        return new RangeInt(aquiredTimersStartIndex, m_timers.Count - 1);
    }

    public void ReleaseTimer(int timerHandle)
    {
        if (!m_timers.IsValidIndex(timerHandle))
        {
            Debug.LogError("invalid timerHandle");
            return;
        }

        (var timer, bool isTaken) = m_timers[timerHandle];
        if (!isTaken)
        {
            Debug.LogError("timer not taken");
            return;
        }
        m_timers[timerHandle] = (timer, false);
    }

    public (Timer, bool) GetTimerData(int handle)
    {
        if (!m_timers.IsValidIndex(handle))
        {
            return (default, false);
        }

        return (m_timers[handle].Timer, true);
    }

    public void SetTimer(int handle, float duration)
    {
        if (!m_timers.IsValidIndex(handle))
        {
            Debug.LogError("invalid timer handle");
            return;
        }

        (var timer, bool isTaken) = m_timers[handle];
        if (!isTaken)
        {
            Debug.LogError("timer not taken");
            return;
        }

        timer.Set(duration);
        m_timers[handle] = (timer, isTaken);
    }

    public void RestartTimer(int handle)
    {
        if (!m_timers.IsValidIndex(handle))
        {
            Debug.LogError("invalid timer handle");
            return;
        }

        (var timer, bool isTaken) = m_timers[handle];
        if (!isTaken)
        {
            Debug.LogError("timer not taken");
            return;
        }

        Debug.Assert(timer.IsSet);

        timer.Set(timer.Duration);
        m_timers[handle] = (timer, isTaken);
    }

    void Update()
    {
        for (int i = 0; i < m_timers.Count; i++)
        {
            (var timer, bool isTaken) = m_timers[i];

            if (!isTaken)
            {
                continue;
            }
            timer.Update(Time.deltaTime);
            m_timers[i] = (timer, isTaken);
        }
    }
}
