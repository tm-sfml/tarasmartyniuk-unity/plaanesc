﻿using UnityEngine;

public struct Timer
{
    public float Duration { get;  set; }
    public float TimePassed { get;  set; }

    public float TimeLeft => Duration - TimePassed;
    public bool IsExpired
    {
        get
        {
            if (!IsSet)
            {
                Debug.LogError("timer not set");
                return false;
            }
            return TimePassed > Duration;
        }
    }

    public bool IsSet => Duration != 0;

    public void Set(float duration)
    {
        Duration = duration;
        TimePassed = 0;
    }

    public void Unset()
    {
        Duration = 0;
        TimePassed = 0;
    }

    public void Update(float dt)
    {
        TimePassed += dt;
    }
}

