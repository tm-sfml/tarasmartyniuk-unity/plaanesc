﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// prints shader uniforms at editor/runtime
/// </summary>
[ExecuteInEditMode]
public class ShaderLog : MonoBehaviour
{
    [SerializeField] int ComputeBufferDisplaySize = 1;
    const int kComputeBufferSize = 10;
    ComputeBuffer m_computeBuffer;

    void Awake()
    {
        Setup();
    }

    void OnValidate()
    {

    }

    // should be called automatically on editor start
    public void Setup()
    {
        if (!TryGetComponent(out MeshRenderer renderer) || m_computeBuffer != null)
        {
            return;
        }

        // TODO: make a float4 to support all lengths
        m_computeBuffer = new ComputeBuffer(kComputeBufferSize, sizeof(float) * 4, ComputeBufferType.Default);
        renderer.sharedMaterial.SetBuffer("_DebugBuffer", m_computeBuffer);

        renderer.sharedMaterial.SetPass(0);
        Graphics.ClearRandomWriteTargets();
        Graphics.SetRandomWriteTarget(1, m_computeBuffer, false);

    }

    public void PrintRenderTarget()
    {
        if (!TryGetComponent(out MeshRenderer renderer))
        {
            return;
        }

        float i = renderer.sharedMaterial.GetFloat("_MyVar");
        var c = renderer.sharedMaterial.GetColor("_MainColor");

        int width = 1;
        int height = 1;

        RenderTexture buffer = new RenderTexture(
                               width,
                               height,
                               0,                            // No depth/stencil buffer
                               RenderTextureFormat.ARGB32,   // Standard colour format
                               RenderTextureReadWrite.Linear // No sRGB conversions
                           );
        Graphics.Blit(null, buffer, renderer.sharedMaterial);

        Texture2D outputTex = new Texture2D(width, height, TextureFormat.ARGB32, false);

        RenderTexture.active = buffer;           // If not using a scene camera
        outputTex.ReadPixels(
                  new Rect(0, 0, width, height), // Capture the whole texture
                  0, 0,                          // Write starting at the top-left texel
                  false                          // No mipmaps
        );

        Debug.Log($"render target: {string.Join(",", outputTex.GetPixels())}");
    }

    public void PrintComputeBuffer()
    {
        if (!TryGetComponent(out MeshRenderer renderer))
        {
            return;
        }

        // Optim: member arr
        var data = new Vector4[] { };
        Array.Resize(ref data, ComputeBufferDisplaySize);
        m_computeBuffer.GetData(data);

        var formattedVectors = data.Select((Vector4 v) => v.ToString("0.0000"));
        Debug.Log($"Compute BUFF: {string.Join(",", formattedVectors)}");
    }

     void OnDestroy()
    {
        if (m_computeBuffer != null)
        {
            m_computeBuffer.Dispose();
        }
    }

}
