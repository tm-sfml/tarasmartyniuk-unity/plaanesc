﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ShaderLog))]
public class ShaderLogEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var obj = (ShaderLog) target;

        if (GUILayout.Button("PrintRenderTarget"))
        {
            obj.PrintRenderTarget();
        }

        if (GUILayout.Button("PrintComputeBuffer"))
        {
            obj.PrintComputeBuffer();
        }

        if (GUILayout.Button("Setup"))
        {
            obj.Setup();
        }
    }
}
