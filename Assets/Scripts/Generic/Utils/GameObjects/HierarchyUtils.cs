﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class HierarchyUtils
{
    // refact: make extention?
    public static void DestroyChildren(GameObject gameObject, bool destroyImmediate = false)
    {
        for (int i = gameObject.transform.childCount; i > 0; --i)
        {
            var currChild = gameObject.transform.GetChild(0).gameObject;
            if (destroyImmediate)
            {
                Object.DestroyImmediate(currChild);
            }
            else
            {
                Object.Destroy(currChild);
            }
        }
    }

}
