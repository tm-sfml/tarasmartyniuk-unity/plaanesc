﻿using UnityEngine;

public static class MathUtils
{
    public static Vector3 Project(Ray ray, Plane plane)
    {
        plane.Raycast(ray, out float rayIntersectDistance);
        return ray.GetPoint(rayIntersectDistance);
    }

    // unity-style (Y for top)
    public static Vector3 To3D(this Vector2 xz, float y = 0f) => new Vector3(xz.x, y, xz.y);
    public static Vector2 To2D(this Vector3 xyz) => new Vector3(xyz.x, xyz.z);

}
