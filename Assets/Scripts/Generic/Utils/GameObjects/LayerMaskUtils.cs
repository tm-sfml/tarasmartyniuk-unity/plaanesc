﻿using UnityEngine;

public static class LayerMaskUtils 
{
    const int Everything = -1;

    // non-params version of LayerMask.GetMask
    public static int GetLayerMask(string layerName)
    {
        var layer = LayerMask.NameToLayer(layerName);
        if (layer == Everything)
        {
            Debug.Log($"no layer named {layerName}");
            return -1;
        }
        return 1 << layer;
    }

    public static int GetLayerMask(UnityLayer layer)
    {
        return 1 << (int) layer;
    }

}
