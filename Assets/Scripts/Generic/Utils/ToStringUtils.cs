﻿using Unity.Entities;

public static class ToStringUtils
{
    public static string StringifyNull<T>(T obj)
    {
        return obj == null ? "null" : obj.ToString();
    }
}
