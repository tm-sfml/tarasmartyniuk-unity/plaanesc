﻿using Unity.Entities;
using Unity.Transforms;

public static class EntityHierarchyUtils
{
    public static Entity FindChildWithComponent<T>(this EntityManager entityManager, Entity entity)
    {
        if (!entityManager.HasComponent<Child>(entity))
        {
            return Entity.Null;
        }

        var children = entityManager.GetBuffer<Child>(entity);
        foreach (var child in children)
        {
            if (entityManager.HasComponent<T>(child.Value))
            {
                return child.Value;
            }
        }
        return Entity.Null;
    }

    public static TComponent GetComponentDataInChildren<TComponent>(this EntityManager entityManager, Entity entity)
        where TComponent : struct, IComponentData
    {
        var child = entityManager.FindChildWithComponent<TComponent>(entity);
        return entityManager.GetComponentData<TComponent>(child);
    }

    // searches through LinkedEntityGroup buffer
    public static Entity FindLinkedWithComponent<T>(this EntityManager entityManager, Entity entity)
    {
        if (!entityManager.HasComponent<LinkedEntityGroup>(entity))
        {
            return Entity.Null;
        }

        var linkedGroupBuffer = entityManager.GetBuffer<LinkedEntityGroup>(entity);
        foreach (var linked in linkedGroupBuffer)
        {
            if (entityManager.HasComponent<T>(linked.Value))
            {
                return linked.Value;
            }
        }
        return Entity.Null;
    }

    public static void SetParent(this EntityManager entityManager, Entity parent, Entity child)
    {
        entityManager.AddComponentData(child, new Parent { Value = parent });
        entityManager.AddComponent<LocalToParent>(child);
    }

    public static void SetParent(this EntityCommandBuffer commandBuffer, Entity parent, Entity child)
    {
        commandBuffer.AddComponent(child, new Parent { Value = parent});
        commandBuffer.AddComponent<LocalToParent>(child);
    }

    
}
