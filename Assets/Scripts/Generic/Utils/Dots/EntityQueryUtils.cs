﻿using System;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

public delegate void RefAction<T>(ref T item);

public static class EntityQueryUtils
{
    // using DefaultGameObjectInjectionWorld
    public static TComponent GetSingleton<TComponent>() where TComponent : struct, IComponentData
    {
        var query = World.DefaultGameObjectInjectionWorld.EntityManager.CreateEntityQuery(typeof(TComponent));
        return query.GetSingleton<TComponent>();
    }

    public static DynamicBuffer<TComponent> GetSingletonBuffer<TComponent>(this EntityManager entityManager) where TComponent : struct, IBufferElementData
    {
        var entity = GetSingletonEntity<TComponent>();
        return entityManager.GetBuffer<TComponent>(entity);
    }

    public static void SetSingleton<TComponent>(TComponent component) where TComponent : struct, IComponentData
    {
        var query = World.DefaultGameObjectInjectionWorld.EntityManager.CreateEntityQuery(typeof(TComponent));
        query.SetSingleton(component);
    }

    public static Entity GetSingletonEntity<TComponent>() where TComponent : struct
    {
        var query = World.DefaultGameObjectInjectionWorld.EntityManager.CreateEntityQuery(typeof(TComponent));
        return query.GetSingletonEntity();
    }

    public static (TComponent, bool success) GetFirstMatchingComponent<TComponent>(this EntityQuery query)
        where TComponent : struct, IComponentData
    {
        using (NativeArray<TComponent> matching = query.ToComponentDataArray<TComponent>(Allocator.TempJob))
        {
            return matching.Length == 0 ? default : (matching[0], true);
        }
    }

    // using DefaultGameObjectInjectionWorld
    public static (TComponent, bool success) GetFirstMatchingComponent<TComponent>()
    where TComponent : struct, IComponentData
    {
        var query = World.DefaultGameObjectInjectionWorld.EntityManager.CreateEntityQuery(typeof(TComponent));
        return query.GetFirstMatchingComponent<TComponent>();
    }

    public static (TComponent, bool success) GetFirstMatchingComponentLogFailure<TComponent>(this EntityQuery query)
    where TComponent : struct, IComponentData
    {
        (TComponent, bool success) findRes = query.GetFirstMatchingComponent<TComponent>();
        if (!findRes.success)
        {
            Debug.LogError($"{typeof(TComponent).Name} component missing");
        }
        return findRes;
    }

    // using DefaultGameObjectInjectionWorld
    public static (TComponent, bool success) GetFirstMatchingComponentLogFailure<TComponent>()
    where TComponent : struct, IComponentData
    {
        var query = World.DefaultGameObjectInjectionWorld.EntityManager.CreateEntityQuery(typeof(TComponent));
        return query.GetFirstMatchingComponentLogFailure<TComponent>();
    }

    /// <returns>Null if not found</returns>
    public static Entity GetFirstEntityWithComponent(this EntityQuery query)
    {
        using (NativeArray<Entity> matching = query.ToEntityArray(Allocator.TempJob))
        {
            return matching.Length == 0 ? Entity.Null : matching[0];
        }
    }

    public static Entity GetFirstEntityWithComponent<TComponent>()
    {
        var query = World.DefaultGameObjectInjectionWorld.EntityManager.CreateEntityQuery(typeof(TComponent));
        return query.GetFirstEntityWithComponent();
    }

    public static Entity GetFirstEntityWithComponentLogFailure<TComponent>(this EntityQuery query)
    {
        Entity entity = query.GetFirstEntityWithComponent();
        if (entity == Entity.Null)
        {
            Debug.LogError($"{typeof(TComponent).Name} component missing");
        }
        return entity;
    }

    public static Entity GetFirstEntityWithComponentLogFailure<TComponent>()
    {
        var query = World.DefaultGameObjectInjectionWorld.EntityManager.CreateEntityQuery(typeof(TComponent));
        return query.GetFirstEntityWithComponentLogFailure<TComponent>();
    }

    // this will produce garbage if predicate captures
    public static Entity GetSingleEntityWithComponent<TFilterComponent>(this EntityQueryBuilder builder, Predicate<TFilterComponent> predicate)
        where TFilterComponent : struct, IComponentData
    {
        Entity result = Entity.Null;
        builder.ForEach((Entity entity, ref TFilterComponent filterComponent) =>
        {
            if (predicate(filterComponent))
            {
                if (result != Entity.Null)
                {
                    Debug.LogError("more than 1 entity for given filter");
                }
                result = entity;
            }
        });
        return result;
    }

    public static Entity GetSingleEntityWithComponent<TFilterComponent>(Predicate<TFilterComponent> predicate)
        where TFilterComponent : struct, IComponentData => 
        CreateEntityQueryBuilder().GetSingleEntityWithComponent(predicate);

    public static Entity GetSingleEntityWithComponentLogFailure<TFilterComponent>(this EntityQueryBuilder builder, Predicate<TFilterComponent> predicate)
        where TFilterComponent : struct, IComponentData
    {
        Entity result = GetSingleEntityWithComponent(builder, predicate);
        if (result == Entity.Null)
        {
            Debug.LogError($"no entity for given filter");
        }
        return result;
    }

    public static Entity GetSingleEntityWithComponentLogFailure<TFilterComponent>(Predicate<TFilterComponent> predicate)
        where TFilterComponent : struct, IComponentData => 
        CreateEntityQueryBuilder().WithAll<TFilterComponent>().GetSingleEntityWithComponentLogFailure(predicate);

    public static (TComponent, bool) GetSingleComponentLogFailure<TComponent>(this EntityQueryBuilder builder, Predicate<TComponent> filter)
        where TComponent : struct, IComponentData
    {
        TComponent? result = null;
        builder.ForEach((ref TComponent component) =>
        {
            if (filter(component))
            {
                if (result != null)
                {
                    Debug.LogError("more than 1 entity for given filter");
                    return;
                }
                result = component;
            }
        });
        return (result.GetValueOrDefault(), result.HasValue);
    }

    public static (TComponent, bool) GetSingleComponentLogFailure<TComponent>(Predicate<TComponent> filter)
        where TComponent : struct, IComponentData => 
        CreateEntityQueryBuilder().WithAll<TComponent>().GetSingleComponentLogFailure(filter);

    public static EntityQueryBuilder CreateEntityQueryBuilder()
    {
        return World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<EntityQueryBuilderCreatorSystem>().CreateQueryBuilder();
    }
}

/// <summary>
/// workaround to be able to use EntityQueryBuilders outside of systems
/// </summary>
public class EntityQueryBuilderCreatorSystem : ComponentSystem
{
    public EntityQueryBuilder CreateQueryBuilder() => Entities;

    protected override void OnUpdate() { }
}
