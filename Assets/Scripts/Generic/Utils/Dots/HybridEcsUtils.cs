﻿using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public struct EntityGameObjectPair
{
    public Entity Entity;
    public GameObject GameObject;
}

public static class HybridEcsUtils
{
    public static GameObject GetLinkedGameObject(this EntityManager entityManager, Entity entity)
    {
        return entityManager.GetComponentObject<Transform>(entity).gameObject;
    }

    // instantiates both gameobject and entity from prefabs and syncs them together
    public static EntityGameObjectPair InstantiateHybrid(this EntityManager entityManager, EntityGameObjectPair prefab, float3 position)
    {
        var spawnedGo = Object.Instantiate(prefab.GameObject, position, Quaternion.identity);
        var spawnedEntity = entityManager.Instantiate(prefab.Entity);

        entityManager.SetComponentData(spawnedEntity, new Translation { Value = position });
        entityManager.SyncHybrid(spawnedGo, spawnedEntity);

        return new EntityGameObjectPair { Entity = spawnedEntity, GameObject = spawnedGo };
    }

    // converts gameObjectPrefab to the EntityGameObjectPair.Entity
    public static EntityGameObjectPair InstantiateHybrid(this EntityManager entityManager, GameObject gameObjectPrefab, float3 position)
    {
        using var blobAssetStore = new BlobAssetStore();
        var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, blobAssetStore);
        Entity converted = GameObjectConversionUtility.ConvertGameObjectHierarchy(gameObjectPrefab, settings);
        return InstantiateHybrid(entityManager, new EntityGameObjectPair { Entity = converted, GameObject = gameObjectPrefab }, position);
    }

    public static void DestroyHybrid(this EntityManager entityManager, Entity entity)
    {
        Object.Destroy(entityManager.GetLinkedGameObject(entity));
        entityManager.DestroyEntity(entity);
    }

    public static void SyncHybrid(this EntityManager entityManager, GameObject gameObject, Entity entity)
    {
        entityManager.InjectOriginalComponentsObjects(entity, gameObject.transform);
        entityManager.AddComponentData(entity, new CopyTransformToGameObject());
    }

    // modified from Entities conversion method, because original ignores Monobehaviors
    public static void InjectOriginalComponentsObjects(this EntityManager entityManager, Entity entity, Transform transform)
    {
        foreach (var com in transform.GetComponents<Component>())
        {
            if (com is GameObjectEntity || com is ConvertToEntity || com is StopConvertToEntity)
            {
                continue;
            }

            entityManager.AddComponentObject(entity, com);
        }
    }
}