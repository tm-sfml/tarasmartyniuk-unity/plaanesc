﻿using Unity.Mathematics;

public static class float3values
{
    public static float3 up => new float3(0, 1, 0);
    public static float3 right => new float3(1, 0, 0);
    public static float3 forward => new float3(0, 0, 1);
    public static float3 back => new float3(0, 0, -1);


}

public static class math_utl
{
    public static float distance_xz(float3 v1, float3 v2)
    {
        return math.distance(new float2(v1.x, v1.z), new float2(v2.x, v2.z));
    }
}

public static class math_conv
{
    /// <summary>
    /// converts in unity-style (Y for top)
    /// </summary>
    public static float3 to3D(this float2 xz, float y = 0f) => new float3(xz.x, y, xz.y);

    public static float4x4 to4x4(this float2x2 from)
    {
        return new float4x4(math.float4(from.c0, float2.zero), 
            math.float4(from.c0, float2.zero), 
            float4.zero, float4.zero);
    }

    public static float2x2 to2x2(this float4x4 from)
    {
        return new float2x2(from.c0.xy, from.c1.xy);
    }
}

