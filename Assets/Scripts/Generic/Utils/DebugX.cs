﻿using UnityEngine;

public static class DebugX
{
    const float Alpha = .2f;

    // DebugX.cs Version 2.0 By ARKMS
    // Hayden Scott-Baron (Dock) - http://starfruitgames.com
    // Adds a number of useful Debug Draw features
    public static void DrawCube(Vector3 pos, Color col, Vector3 scale, float _duration = 0f)
    {
        Vector3 halfScale = scale * 0.5f;

        Vector3[] points = new Vector3[]
        {
            pos + new Vector3(halfScale.x,      halfScale.y,    halfScale.z),
            pos + new Vector3(-halfScale.x,     halfScale.y,    halfScale.z),
            pos + new Vector3(-halfScale.x,     -halfScale.y,   halfScale.z),
            pos + new Vector3(halfScale.x,      -halfScale.y,   halfScale.z),
            pos + new Vector3(halfScale.x,      halfScale.y,    -halfScale.z),
            pos + new Vector3(-halfScale.x,     halfScale.y,    -halfScale.z),
            pos + new Vector3(-halfScale.x,     -halfScale.y,   -halfScale.z),
            pos + new Vector3(halfScale.x,      -halfScale.y,   -halfScale.z),
        };

        Debug.DrawLine(points[0], points[1], col, _duration);
        Debug.DrawLine(points[1], points[2], col, _duration);
        Debug.DrawLine(points[2], points[3], col, _duration);
        Debug.DrawLine(points[3], points[0], col, _duration);
    }

    public static void DrawRect(Rect rect, Color col, float _duration = 0f)
    {
        Vector3 pos = new Vector3(rect.x + rect.width / 2, rect.y + rect.height / 2, 0.0f);
        Vector3 scale = new Vector3(rect.width, rect.height, 0.0f);

        DebugX.DrawRect(pos, col, scale, _duration);
    }

    public static void DrawRect(Vector3 pos, Color col, Vector3 scale, float _duration = 0f)
    {
        Vector3 halfScale = scale * 0.5f;

        Vector3[] points = new Vector3[]
        {
            pos + new Vector3(halfScale.x,      halfScale.y,    halfScale.z),
            pos + new Vector3(-halfScale.x,     halfScale.y,    halfScale.z),
            pos + new Vector3(-halfScale.x,     -halfScale.y,   halfScale.z),
            pos + new Vector3(halfScale.x,      -halfScale.y,   halfScale.z),
        };

        Debug.DrawLine(points[0], points[1], col, _duration);
        Debug.DrawLine(points[1], points[2], col, _duration);
        Debug.DrawLine(points[2], points[3], col, _duration);
        Debug.DrawLine(points[3], points[0], col, _duration);
    }

    public static void DrawPoint(Vector3 pos, Color col, float scale = 1f, float _duration = 0f)
    {
        Vector3[] points = new Vector3[]
        {
            pos + (Vector3.up * scale),
            pos - (Vector3.up * scale),
            pos + (Vector3.right * scale),
            pos - (Vector3.right * scale),
            pos + (Vector3.forward * scale),
            pos - (Vector3.forward * scale)
        };

        Debug.DrawLine(points[0], points[1], col, _duration);
        Debug.DrawLine(points[2], points[3], col, _duration);
        Debug.DrawLine(points[4], points[5], col, _duration);

        Debug.DrawLine(points[0], points[2], col, _duration);
        Debug.DrawLine(points[0], points[3], col, _duration);
        Debug.DrawLine(points[0], points[4], col, _duration);
        Debug.DrawLine(points[0], points[5], col, _duration);

        Debug.DrawLine(points[1], points[2], col, _duration);
        Debug.DrawLine(points[1], points[3], col, _duration);
        Debug.DrawLine(points[1], points[4], col, _duration);
        Debug.DrawLine(points[1], points[5], col, _duration);

        Debug.DrawLine(points[4], points[2], col, _duration);
        Debug.DrawLine(points[4], points[3], col, _duration);
        Debug.DrawLine(points[5], points[2], col, _duration);
        Debug.DrawLine(points[5], points[3], col, _duration);
    }

    public static void DrawCirle(Vector3 _pos, Color _col, float _radio, float _duration = 0f)
    {
        Vector3 p1 = Vector3.zero, p2 = Vector3.zero;
        p1.z = p2.z = _pos.z;
        float PI2 = 2f * Mathf.PI;

        for (float theta = 0f; theta < PI2; theta += 0.2f)
        {
            p1.x = _pos.x + _radio * Mathf.Cos(theta);
            p1.y = _pos.y + _radio * Mathf.Sin(theta);

            if (theta > 0f)// ignore first pass
                Debug.DrawLine(p1, p2, _col, _duration);

            if (theta + 0.1f < PI2)
            {
                p2 = p1;
            }
        }
        //Debug.DrawLine(p1, p2, _col, _duration);
        p2 = p1;
        p1.x = _pos.x + _radio * Mathf.Cos(PI2);
        p1.y = _pos.y + _radio * Mathf.Sin(PI2);
        Debug.DrawLine(p1, p2, _col, _duration);
    }

    public static GameObject DrawSphere(Vector3 center, float radius, Color color)
    {
        var go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        go.transform.position = center;

        const float defaultSphereRadius = 0.5f;
        float scaleForRequestedRadius = radius / defaultSphereRadius;
        go.transform.localScale = Vector3.one * scaleForRequestedRadius;

        var renderer = go.GetComponent<Renderer>();
        var material = renderer.material;
        StandardShaderUtils.ChangeRenderMode(material, BlendMode.Transparent);
        color.a = Alpha;
        material.color = color;

        Object.Destroy(go.GetComponent<SphereCollider>());
        return go;
    }
}
