﻿using System.Runtime.CompilerServices;

public class EnumUtils
{
    // compensates for the "Invalid" element that is always 0th
    public static int ToInt<T>(T e) where T : struct, System.Enum
    {
        return Unsafe.As<T, int>(ref e) - 1;
    }

    // compensates for the "Invalid" element that is always 0th
    public static T FromInt<T>(int i) where T : struct, System.Enum
    {
        int tmp = i + 1;
        return Unsafe.As<int, T>(ref tmp);
    }
}
