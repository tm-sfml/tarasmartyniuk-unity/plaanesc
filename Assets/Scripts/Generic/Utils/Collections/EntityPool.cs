﻿using System;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

class EntityPool : IDisposable
{
    NativeList<Entity> m_entities;
    EntityManager m_entityManager;
    Entity m_prefab;

    public EntityPool(int size, Entity prefab, EntityManager entityManager)
    {
        if (prefab == Entity.Null)
        {
            Debug.LogError("prefab is null");
            return;
        }
        if (size < 1)
        {
            Debug.LogError("size cannot be < 1");
            return;
        }

        m_entityManager = entityManager;
        m_prefab = prefab;
        m_entities = new NativeList<Entity>(size, Allocator.Persistent);
        m_entities.ResizeUninitialized(size);
        Instantiate(0);
    }

    public Entity TakeNextFreeEntity()
    {
        Entity firstFreeEntity = Entity.Null;
        foreach (Entity entity in m_entities)
        {
            if (!m_entityManager.GetEnabled(entity))
            {
                firstFreeEntity = entity;
                break;
            }
        }

        if (firstFreeEntity == Entity.Null)
        {
            // optim: make configurable resize step, and choose sensible default
            int firstNewEntityIndex = m_entities.Length;
            m_entities.ResizeUninitialized(m_entities.Length * 2);
            Instantiate(firstNewEntityIndex);
            firstFreeEntity = m_entities[firstNewEntityIndex];
        }

        m_entityManager.SetEnabled(firstFreeEntity, true);
        return firstFreeEntity;
    }

    // TODO: TakeNextFreeEntity array overload

    public void ReturnEntity(Entity entity)
    {
        m_entityManager.SetEnabled(entity, false);
    }

    public void ReturnAll()
    {
        foreach (Entity entity in m_entities)
        {
            m_entityManager.SetEnabled(entity, false);
        }
    }

    void Instantiate(int startIndex)
    {
        NativeArray<Entity> newEntitiesSlice = m_entities.AsArray().GetSubArray(startIndex, m_entities.Length - startIndex);
        m_entityManager.Instantiate(m_prefab, newEntitiesSlice);

        foreach (var entity in newEntitiesSlice)
        {
            m_entityManager.SetEnabled(entity, false);
        }
    }

    public void Dispose() => m_entities.Dispose();
}
