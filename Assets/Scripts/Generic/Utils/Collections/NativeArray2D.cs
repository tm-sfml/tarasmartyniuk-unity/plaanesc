﻿using System;
using Unity.Collections;
using Unity.Mathematics;

/// adapter that interprets array(buffer) as 2D, and owns underlying NativeArray
/// TODO: support resize with NativeList
public struct NativeArray2D<T> : IDisposable where T : struct
{
    public Slice2D<T> Slice 
    {
        get => m_slice;
        private set => m_slice = value;
    }

    public NativeArray<T> FlatArray { get; private set; }

    // separate field to compile indexer assignment
    Slice2D<T> m_slice;

    public NativeArray2D(int size2D, in NativeArray<T> array)
    {
        FlatArray = array;
        m_slice = new Slice2D<T>(size2D, array);
    }

    public T this[int2 index]
    {
        get => Slice[index];
        set => m_slice[index] = value;
    }

    public bool IsValidIndex(int2 index) => Slice.IsValidIndex(index);

    public void Fill(in T value) => Slice.Fill(value);
    public void Dispose() => FlatArray.Dispose();
}
