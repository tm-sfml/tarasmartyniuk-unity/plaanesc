﻿using Unity.Collections;
using Unity.Mathematics;

/// <summary>
/// adapter that interprets array(buffer) as 2D, without owning it
/// </summary>
public struct Slice2D<TElem> where TElem : struct
{
    public NativeSlice<TElem> FlatSlice => m_flatSlice;

    public int Size1D => m_flatSlice.Length / Size2D;
    public int Size2D { get;  set; }

    NativeSlice<TElem> m_flatSlice;

    public Slice2D(int size2D, NativeSlice<TElem> flatSlice)
    {
        m_flatSlice = flatSlice;
        Size2D = size2D;
    }

    public Slice2D(int size2D, NativeArray<TElem> flatArray)
        : this(size2D, new NativeSlice<TElem>(flatArray)) { }

    public TElem this[int2 index]
    {
        get => m_flatSlice[FlattenIndex(index.x, index.y)];
        set => m_flatSlice[FlattenIndex(index.x, index.y)] = value;
    }

    public TElem this[int x, int y]
    {
        get => m_flatSlice[FlattenIndex(x, y)];
        set => m_flatSlice[FlattenIndex(x, y)] = value;
    }

    public bool IsValidIndex(int x, int y)
    {
        return x >= 0 && y >= 0 &&
            x < Size1D && y < Size2D;
    }

    public bool IsValidIndex(int2 index) => IsValidIndex(index.x, index.y);

    public void Fill(in TElem value)
    {
        FlatSlice.Fill(value);
    }

    int FlattenIndex(int x, int y)
    {
        return x * Size2D + y;
    }
}

public static class SliceExtentions
{
    public static void Fill<T>(this NativeSlice<T> slice, in T value) where T : struct
    {
        for (int i = 0; i < slice.Length; i++)
        {
            slice[i] = value;
        }
    }
}
