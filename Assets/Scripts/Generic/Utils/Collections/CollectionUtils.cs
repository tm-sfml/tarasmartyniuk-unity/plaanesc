﻿using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using Unity.Entities;
using System;

public static class CollectionUtils
{
    public static bool IsValidIndex<T>(this List<T> list, int index)
    {
        return index >= 0 && index < list.Count;
    }

    public static bool IsValidIndex<T>(this NativeArray<T> array, int index) where T : struct
    {
        return index >= 0 && index < array.Length;
    }

    public static bool IsValidIndex<T>(this NativeList<T> list, int index) where T : struct
    {
        return index >= 0 && index < list.Length;
    }

    public static bool Exists<T>(this NativeArray<T> buffer, Predicate<T> predicate) where T : struct
    {
        return buffer.FindIndex(predicate) != -1;
    }

    public static bool Exists<T>(this DynamicBuffer<T> buffer, Predicate<T> predicate) where T : struct
    {
        return buffer.AsNativeArray().Exists(predicate);
    }

    public static int FindIndex<T>(this NativeArray<T> buffer, Predicate<T> predicate) where T : struct
    {
        for (int i = 0; i < buffer.Length; i++)
        {
            if (predicate(buffer[i]))
            {
                return i;
            }
        }

        return -1;
    }

    public static int FindIndex<T>(this DynamicBuffer<T> buffer, Predicate<T> predicate) where T : struct
    {
        return buffer.AsNativeArray().FindIndex(predicate);
    }

    public static int IndexOf<T>(this DynamicBuffer<T> buffer, T element) where T : struct, IEquatable<T>
    {
        return buffer.AsNativeArray().IndexOf(element);
    }

    public static void AddRange<T>(this List<T> list, int count, T element = default)
    {
        int newCount = list.Count + count;
        if (newCount > list.Capacity)
        {
            list.Capacity = newCount;
        }

        list.AddRange(Enumerable.Repeat(element, count));
    }

    public static void GetKeys<TKey, TValue>(this Dictionary<TKey, TValue> dict, List<TKey> outKeys)
    {
        outKeys.Clear();
        outKeys.Capacity = dict.Keys.Count;

        foreach (var key in dict.Keys)
        {
            outKeys.Add(key);
        }
    }
}
