﻿using Unity.Collections;
using Unity.Entities;

// treats buffer as queue so that last buffer element is the first to deque, and first is last
// no on-the-fly enqueue - buffer should be filled with all elements at once in the right order
public static class DynamicBufferQueueAdapter
{
    public static T Dequeue<T>(DynamicBuffer<T> from) where T : struct
    {
        int lastIndex = from.Length - 1;
        T element = from[lastIndex];
        from.RemoveAtSwapBack(lastIndex);
        return element;
    }

    // transfers N elements from queue to other buffer (not clearing it)
    public static void DequeueAndAddN<T>(DynamicBuffer<T> from, int numberToDequeue, DynamicBuffer<T> result) 
        where T : struct
    {
        if (numberToDequeue == 0)
        {
            return;
        }

        result.EnsureCapacity(result.Length + numberToDequeue);

        int start = from.Length - numberToDequeue;
        for (int i = start; i < from.Length; i++)
        {
            result.Add(from[i]);
        }

        from.RemoveRangeSwapBack(start, numberToDequeue);
    }
}
