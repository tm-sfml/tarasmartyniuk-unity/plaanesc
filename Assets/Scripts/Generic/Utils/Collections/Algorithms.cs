﻿using System;
using Unity.Collections;
using Unity.Entities;

public static class Algorithms
{
    public static void Shuffle<T>(this NativeArray<T> array) where T : struct => 
        Shuffle(array, new Random());

    public static void Shuffle<T>(this NativeArray<T> array, Random rng) where T : struct
    {
        int n = array.Length;
        while (n > 1)
        {
            int k = rng.Next(n--);
            T temp = array[n];
            array[n] = array[k];
            array[k] = temp;
        }
    }
}
