﻿using UnityEngine;
using UnityEngine.EventSystems;

// if needed: hack to get LastPointerData, 
// this needs to be placed instead of default StandaloneInputModule on EventSystem
public class StandaloneInputModuleCustom : StandaloneInputModule
{
    new public PointerEventData GetLastPointerEventData(int deviceId = kMouseLeftId)
    {
        return GetLastPointerEventData(deviceId);
    }

    public GameObject GameObjectUnderPointer(int deviceId = kMouseLeftId)
    {
        PointerEventData lastPointer = GetLastPointerEventData(deviceId);
        return lastPointer?.pointerCurrentRaycast.gameObject;
    }
}
