﻿using UnityEngine;
using UnityEngine.EventSystems;
using Unity.Entities;
using Unity.Transforms;

public enum AllowedCardTarget
{
    Invalid,
    UnocupiedHex,
    // any creature or team health hex
    AnyTeamDamageableEntity
}

/// <summary>
/// single card in hand
/// </summary>
public class DraggableCard : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public GameObject DraggedCardBlob { get; set; }
    public int CardBattleDeckId { get; set; }

    const int kOnDragDebugMessagePosition = 1;

    public void OnBeginDrag(PointerEventData eventData)
    {
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        var cardDef = entityManager.GetComponentData<CardDefinitionComponent>(GetCardDefinitionEntity());

        var battleDataEntity = BattleDataUtils.GetBattleDataEntityForTeam(EntityQueryUtils.GetSingleton<BattleTurnComponent>().CurrentActingTeam);
        var manaEntity = entityManager.FindChildWithComponent<ManaComponent>(battleDataEntity);
        var manaComponent = entityManager.GetComponentData<ManaComponent>(manaEntity);
        if (manaComponent.CurrentMana >= cardDef.ManaCost)
        {
            DraggedCardBlob.SetActive(true);
            World.DefaultGameObjectInjectionWorld.GetExistingSystem<Battle3CSystem>().Enabled = false;
        }
        else // forbig drag
        {
            eventData.pointerDrag = null;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        // if on hex, snap to its center - only if valid
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        var hexMapEntity = EntityQueryUtils.GetSingletonEntity<HexMapComponent>();
        var hexMapComponent = entityManager.GetComponentData<HexMapComponent>(hexMapEntity);
        CubeCoords coords = HexUtils.GetHexCoordAtScreenPoint(eventData.position, hexMapComponent);
        Slice2D<HexEntityBufferElement> coordToHex = HexUtils.GetCoordToHexArray(hexMapEntity);

        (Entity _, Entity hexUnderCursor) = Battle3CSystem.GetCreatureAndHexUnderMouse(coordToHex, hexMapComponent, coords, entityManager);
        var cardDefEntity = GetCardDefinitionEntity();
        var cardDef = entityManager.GetComponentData<CardDefinitionComponent>(cardDefEntity);
        if (hexUnderCursor == Entity.Null || !CardEffects.IsHexAllowedForCard(hexUnderCursor, cardDef.CardBehavior))
        {
            HangBlobUnderCursor(eventData.position);
            return;
        }

        var translation = entityManager.GetComponentData<Translation>(hexUnderCursor);
        var offset = DraggedCardBlob.GetComponent<MeshFilter>().sharedMesh.bounds.extents.y;
        DraggedCardBlob.transform.position = translation.Value + float3values.up * offset;

        OnScreenDebug.Instance.Output(new DebugOutputPrintCommand
        {
            Message = $"snapped to {coords}",
            Position = kOnDragDebugMessagePosition,
            IsPersistent = true
        });
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        var hexMapEntity = EntityQueryUtils.GetSingletonEntity<HexMapComponent>();
        var hexMapComponent = entityManager.GetComponentData<HexMapComponent>(hexMapEntity);
        CubeCoords coords = HexUtils.GetHexCoordAtScreenPoint(eventData.position, hexMapComponent);
        Slice2D<HexEntityBufferElement> coordToHex = HexUtils.GetCoordToHexArray(hexMapEntity);

        (Entity _, Entity hexUnderCursor) = Battle3CSystem.GetCreatureAndHexUnderMouse(coordToHex, hexMapComponent, coords, entityManager);
        var cardDefEntity = GetCardDefinitionEntity();

        var cardDef = entityManager.GetComponentData<CardDefinitionComponent>(cardDefEntity);
        if (hexUnderCursor != Entity.Null && CardEffects.IsHexAllowedForCard(hexUnderCursor, cardDef.CardBehavior))
        {
            CardEffects.UseCard(hexUnderCursor, cardDefEntity, 
                EntityQueryUtils.GetSingleton<BattleTurnComponent>().CurrentActingTeam, entityManager);
            BattleDeck.DiscardCard(CardBattleDeckId, GetCurrentBattleDeckEntity(), entityManager);
        }

        OnScreenDebug.Instance.RemoveMessage(kOnDragDebugMessagePosition);
        World.DefaultGameObjectInjectionWorld.GetExistingSystem<Battle3CSystem>().Enabled = true;
        DraggedCardBlob.SetActive(false);
    }

    Entity GetCurrentBattleDeckEntity()
    {
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;

        var currentBattleDataEntity = BattleDataUtils.GetBattleDataEntityForTeam(
            EntityQueryUtils.GetSingleton<BattleTurnComponent>().CurrentActingTeam);
        return entityManager.FindChildWithComponent<BattleDeckConfigComponent>(currentBattleDataEntity);
    }

    Entity GetCardDefinitionEntity()
    {
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;

        var battleDeckEntity = GetCurrentBattleDeckEntity();
        var cardBattleData = BattleDeck.GetCardFromBattleDeck(CardBattleDeckId, battleDeckEntity, entityManager);
        var idTocardDefinition = entityManager.GetSingletonBuffer<CardDefinitionBufferElement>();
        return idTocardDefinition[EnumUtils.ToInt(cardBattleData.DefinitionId)].Entity;
    }

    void HangBlobUnderCursor(Vector3 cursorPosition)
    {
        var ray = Camera.main.ScreenPointToRay(cursorPosition);
        HexUtils.HexPlane.Raycast(ray, out float distanceToHexes);

        DraggedCardBlob.transform.position = ray.GetPoint(distanceToHexes / 2f);
    }

}
