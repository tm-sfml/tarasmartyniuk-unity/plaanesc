﻿using Unity.Entities;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

// updates values for creature floating huds
public class CreatureHudSystem : ComponentSystem
{
    CreatureHudSystemConfig m_config;

    protected override void OnStartRunning()
    {
        base.OnStartRunning();
        var query = EntityManager.CreateEntityQuery(typeof(CreatureHudSystemConfig));
        (m_config, _) = query.GetFirstMatchingComponentLogFailure<CreatureHudSystemConfig>();
    }

    protected override void OnUpdate()
    {
        Entities.ForEach((Entity entity, ref CreatureComponent creatureComponent, Transform transform) => {

            var canvas = transform.GetComponentInChildren<Canvas>();
            if (canvas== null)
            {
                Debug.LogError($"no hud canvas on creature {transform.gameObject}");
                return;
            }
            var label = canvas.GetComponentInChildren<TextMeshProUGUI>();
            if (label == null)
            {
                Debug.LogError($"no hud label on creature {transform.gameObject}");
                return;
            }

            label.text = $"{creatureComponent.Attack}/{creatureComponent.Health}";

            var image = canvas.GetComponentInChildren<Image>();
            if (image == null)
            {
                Debug.LogError($"no hud image on creature {transform.gameObject}");
                return;
            }

            Color color = Color.black;
            switch (creatureComponent.Team)
            {
                case Team.Player:
                    color = m_config.PlayerCreatureColor;
                    break;
                case Team.Enemy:
                    color = m_config.EnemyCreatureColor;
                    break;
                default:
                    Debug.LogError("invalid team");
                    break;
            }
            image.color = color;
        });
    }
}