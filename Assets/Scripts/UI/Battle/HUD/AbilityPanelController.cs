﻿using Unity.Entities;
using UnityEngine;
using UnityEngine.UI;

public class AbilityPanelController : MonoBehaviour
{
    Entity m_currentDisplayedAbilitiesCreature = Entity.Null;

    public static void OnEndTurnButtonClick()
    {
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        // tmp hack, to reuse the button for battle & world map
        bool isInBattle = World.DefaultGameObjectInjectionWorld.GetExistingSystem<Battle3CSystem>().Enabled;
        if (isInBattle)
        {
            var turnEntity = EntityQueryUtils.GetSingletonEntity<BattleTurnComponent>();
            entityManager.SetComponentData(turnEntity,
                new BattleTurnComponent(entityManager.GetComponentData<BattleTurnComponent>(turnEntity))
                {
                    EndTurnRequested = true
                });
        }
        else
        {
            WorldTurns.EndTurn();
        }
    }

    void Update()
    {
        var battle3CEntity = EntityQueryUtils.GetFirstEntityWithComponentLogFailure<Battle3CComponent>();
        if (battle3CEntity == Entity.Null)
        {
            return;
        }

        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        var battle3CComponent = entityManager.GetComponentData<Battle3CComponent>(battle3CEntity);
        bool creatureSelected = battle3CComponent.SelectedCreature != Entity.Null;

        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(creatureSelected);
        }

        if (!creatureSelected || battle3CComponent.SelectedCreature == m_currentDisplayedAbilitiesCreature)
        {
            return;
        }

        // TODO: init a button for each ability ID
        var firstButton = transform.GetChild(0);
        var button = firstButton.GetComponent<Button>();
        button.onClick.AddListener(() => StartTargeting(AbilityType.Attack));
        m_currentDisplayedAbilitiesCreature = battle3CComponent.SelectedCreature;
    }

    void StartTargeting(AbilityType abilityType)
    {
        var battle3CEntity = EntityQueryUtils.GetFirstEntityWithComponentLogFailure<Battle3CComponent>();
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        var battle3CComponent = entityManager.GetComponentData<Battle3CComponent>(battle3CEntity);

        if (battle3CComponent.CurrentTargetingAbility != AbilityType.Invalid)
        {
            return;
        }

        battle3CComponent.CurrentTargetingAbility = abilityType;
        entityManager.SetComponentData(battle3CEntity, battle3CComponent);
    }
}
