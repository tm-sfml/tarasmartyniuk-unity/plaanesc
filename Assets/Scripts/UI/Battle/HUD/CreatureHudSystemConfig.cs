﻿using Unity.Entities;
using UnityEngine;

[GenerateAuthoringComponent]
public struct CreatureHudSystemConfig : IComponentData
{
    public Color PlayerCreatureColor;
    public Color EnemyCreatureColor;
}
