using UnityEngine;
using TMPro;
using Unity.Entities;

public class ManaHud : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI Text;

    void Update()
    {
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        var turnComponent = EntityQueryUtils.GetSingleton<BattleTurnComponent>();
        var battleDataEntity = BattleDataUtils.GetBattleDataEntityForTeam(turnComponent.CurrentActingTeam);
        var manaEntity = entityManager.FindChildWithComponent<ManaComponent>(battleDataEntity);

        // hack, to compensate for child buffer being added one frame after (will be gone when battle will not start right away)
        if (manaEntity == Entity.Null)
        {
            return;
        }
        var manaComponent = entityManager.GetComponentData<ManaComponent>(manaEntity);
        Text.text = $"{manaComponent.CurrentMana}/{manaComponent.CurrentMaxMana}";
    }
}
