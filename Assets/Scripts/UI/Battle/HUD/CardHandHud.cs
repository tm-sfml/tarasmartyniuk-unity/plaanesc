﻿using Unity.Entities;
using UnityEngine;
using TMPro;
using System;

public class CardHandHud : MonoBehaviour
{
    [SerializeField] GameObject DraggedCardBlob;
    [SerializeField] GameObject CardPrefab;

    void Start()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            var child = transform.GetChild(i);
            var card = child.GetComponent<DraggableCard>();
            card.DraggedCardBlob = DraggedCardBlob;
        }
    }

    void Update()
    {
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        var turnComponent = EntityQueryUtils.GetSingleton<BattleTurnComponent>();
        var battleDataEntity = BattleDataUtils.GetBattleDataEntityForTeam(turnComponent.CurrentActingTeam);
        var battleDeckEntity = entityManager.FindChildWithComponent<BattleDeckConfigComponent>(battleDataEntity);

        // hack, to compensate for child buffer being added one frame after (will be gone when battle will not start right away)
        if (battleDeckEntity == Entity.Null)
        {
            return;
        }

        var hand = entityManager.GetBuffer<HandBufferElement>(battleDeckEntity);

        for (int i = 0; i < hand.Length; i++)
        {
            GameObject card;
            if (i < transform.childCount)
            {
                card = transform.GetChild(i).gameObject;
                card.SetActive(true);
            }
            else
            {
                card = Instantiate(CardPrefab, transform);
            }

            var draggableCard = card.GetComponent<DraggableCard>();
            draggableCard.CardBattleDeckId = hand[i].Value.BattleDeckId;

            var idTocardDefinition = entityManager.GetSingletonBuffer<CardDefinitionBufferElement>();
            var defEntity = idTocardDefinition[EnumUtils.ToInt(hand[i].Value.DefinitionId)].Entity;
            var cardDef = entityManager.GetComponentData<CardDefinitionComponent>(defEntity);

            void SetText(int i, string value)
            {
                var text = card.transform.GetChild(i).GetComponent<TextMeshProUGUI>();
                text.text = value;
            }

            SetText(0, Convert.ToString(cardDef.ManaCost));
            SetText(1, Convert.ToString(hand[i].Value.DefinitionId));
        }

        for (int i = hand.Length; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }
}
