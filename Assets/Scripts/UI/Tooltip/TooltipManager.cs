﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;


public class TooltipManager : MonoBehaviour
{
    [SerializeField] GameObject TextOnlyTooltip;
    [SerializeField] Canvas ParentCanvas;

    List<RaycastResult> m_raycastResults = new List<RaycastResult>();

    void Start()
    {
        TextOnlyTooltip.SetActive(false);
    }

    void Update()
    {
        var pointerData = new PointerEventData(EventSystem.current) { position = Input.mousePosition };
        EventSystem.current.RaycastAll(pointerData, m_raycastResults);

        TooltipText firstTooltipText = null;
        RaycastResult tooltipableResult = m_raycastResults.Find((RaycastResult result) =>
            result.gameObject.TryGetComponent(out firstTooltipText));

        GameObject tooltipableObject = tooltipableResult.gameObject;
        if (firstTooltipText == null || tooltipableObject == null)
        {
            TextOnlyTooltip.SetActive(false);
            return;
        }

        var tooltipRect = TextOnlyTooltip.GetComponent<RectTransform>();
        Vector2 tooltipOffset = (tooltipRect.rect.size / 2) * ChooseTooltipOffsetDirection();

        TextOnlyTooltip.GetComponent<RectTransform>().position = Input.mousePosition + (Vector3) tooltipOffset;
        TextOnlyTooltip.SetActive(true);
    }

    // get the side to which to display tooltip for it to fully fit
    // priority: bottom > top; right > left
    Vector2 ChooseTooltipOffsetDirection()
    {
        var canvasRect = ParentCanvas.GetComponent<RectTransform>();
        Rect tooltipDimentions = TextOnlyTooltip.GetComponent<RectTransform>().rect;

        float horizOffset = 0;

        float spaceToRight = canvasRect.rect.width - Input.mousePosition.x;
        float spaceToLeft = Input.mousePosition.x;

        if (spaceToRight > tooltipDimentions.width)
        {
            horizOffset = 1;
        }
        else if (spaceToLeft > tooltipDimentions.width)
        {
            horizOffset = -1;
        }

        float vertOffset = 0;
        float spaceToUp = canvasRect.rect.height - Input.mousePosition.y;
        float spaceToDown = Input.mousePosition.y;
        if (spaceToDown > tooltipDimentions.height)
        {
            vertOffset = -1;
        }
        else if (spaceToUp > tooltipDimentions.height)
        {
            vertOffset = 1;
        }

        return new Vector2(horizOffset, vertOffset);
    }

}
