using Unity.Entities;
using UnityEngine;
using TMPro;
using System;

public class DilemmaHexLabel : MonoBehaviour
{
    void Start()
    {
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        var hexMapEntity = EntityQueryUtils.GetSingletonEntity<HexMapComponent>();
        var coordToHex = HexUtils.GetCoordToHexArray(hexMapEntity);
        var hexCoords = HexUtils.GetProjectionHexCoords(transform.position, entityManager.GetComponentData<HexMapComponent>(hexMapEntity));

        var hex = coordToHex[HexMath.CubeToOffset(hexCoords)].Hex;

        var label = GetComponentInChildren<TextMeshProUGUI>();
        var dilemmaType = entityManager.GetComponentData<DilemmaComponent>(hex).DilemmaEncounterId;
        label.text = $"Dilemma: {Convert.ToString(dilemmaType)}";
    }
}
