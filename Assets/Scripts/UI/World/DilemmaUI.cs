using System.Text;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Unity.Mathematics;

// refactor to a single flow singleton if more than 1 flow-breaking UI will be used
public class DilemmaUI : MonobehaviorSingletonBase<DilemmaUI>
{
    [SerializeField] GameObject ButtonPrefab;
    Entity m_dilemmaHex;

    public void Show(Entity dilemmaHex)
    {
        gameObject.SetActive(true);
        m_dilemmaHex = dilemmaHex;
        World.DefaultGameObjectInjectionWorld.GetExistingSystem<WorldMap3CSystem>().Enabled = false;

        var optionToEffects = GetOptionToEffectsMap(dilemmaHex);

        var description = new StringBuilder();
        int i = 0;
        for (; i < optionToEffects.Size1D; i++)
        {
            if (i >= transform.childCount)
            {
                Instantiate(ButtonPrefab, transform);
            }

            var button = transform.GetChild(i);
            button.GetComponent<Button>().onClick.RemoveAllListeners();
            int optionIndexCapture = i;
            button.GetComponent<Button>().onClick.AddListener(() => OnOptionButtonClick(optionIndexCapture));

            description.Clear();
            for (int j = 0; j < optionToEffects.Size2D; j++)
            {
                var effectDescription = GetEffectDescription(optionToEffects[i, j]);
                if (effectDescription == null)
                {
                    Debug.LogError("Invalid effect definition");
                    continue;
                }

                description.Append(effectDescription);
                if (j != optionToEffects.Size2D - 1)
                {
                    description.Append("; ");
                }
            }

            button.GetComponentInChildren<TextMeshProUGUI>().text = description.ToString();
        }

        for (; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }

    override protected void Awake()
    {
        base.Awake();
        gameObject.SetActive(false);
    }

    Slice2D<DilemmaEncounterEffectBufferElement> GetOptionToEffectsMap(Entity dilemmaHex)
    {
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        var dilemmaComponent = entityManager.GetComponentData<DilemmaComponent>(dilemmaHex);
        var effects = entityManager.GetBuffer<DilemmaEncounterEffectBufferElement>(dilemmaHex);
        return new Slice2D<DilemmaEncounterEffectBufferElement>(dilemmaComponent.MaxEffectsCount, new NativeSlice<DilemmaEncounterEffectBufferElement>(effects.AsNativeArray()));
    }

    void OnOptionButtonClick(int optionIndex)
    {
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        var optionToEffects = GetOptionToEffectsMap(m_dilemmaHex);
        for (int i = 0; i < optionToEffects.Size2D; i++)
        {
            switch (optionToEffects[optionIndex, i].Type)
            {
            case DilemmaEncounterEffectType.GoldChange:
                Debug.Log($"applied effect# {i}");
                break;
            default:
                // effects arrays are padded with Invalid values to the MaxEffectsCount - skipping the padding
                goto loopEnd; // goto because cannot break loop inside of switch
            }
        }
        loopEnd:

        gameObject.SetActive(false);
        World.DefaultGameObjectInjectionWorld.GetExistingSystem<WorldMap3CSystem>().Enabled = true;
    }

    static string GetEffectDescription(DilemmaEncounterEffectBufferElement effectDefinition)
    {
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;

        switch (effectDefinition.Type)
        {
        case DilemmaEncounterEffectType.GoldChange:
            var goldChangeComponent = entityManager.GetComponentData<GoldChangeDilemmaEffectComponent>(effectDefinition.EffectData);
            string goldAction = goldChangeComponent.GoldChange < 0 ?
                "Lose" : "Gain";
            return $"{goldAction} {math.abs(goldChangeComponent.GoldChange)} gold";
        default:
            return null;
        }
    }
}
