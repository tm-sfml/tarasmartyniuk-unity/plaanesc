﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    [SerializeField] Button PlayButton;
    [SerializeField] Button QuitButton;

    void Start()
    {
        PlayButton.onClick.AddListener(GameFlow.Instance.TransitionFromMainMenuToGame);
        QuitButton.onClick.AddListener(Application.Quit);
        PlayButton.Select();
    }

    public void StealSelection(BaseEventData data)
    {
        var pointerEnterData = (PointerEventData) data;
        // button can be blocked by text
        var button = pointerEnterData.pointerEnter.GetComponentInParent<Button>();
        EventSystem.current.SetSelectedGameObject(button.gameObject);
    }
}
