﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PauseMenuController : MonoBehaviour
{
    [SerializeField] Button ContinueButton;
    [SerializeField] Button GoToMainMenuButton;

    void OnEnable()
    {
        if (ContinueButton.onClick.GetPersistentEventCount() == 0)
        {
            ContinueButton.onClick.AddListener(() => {
                PauseManager.Instance.ApplyPause(false);
                gameObject.SetActive(false);

                ContinueButton.OnDeselect(null);
            });

            GoToMainMenuButton.onClick.AddListener(() => {
                PauseManager.Instance.ApplyPause(false);
                GameFlow.Instance.TransitionFromGameToMainMenu();
            });
        }

        // hack:
        IEnumerator Select()
        {
            yield return new WaitForEndOfFrame();
            ContinueButton.Select();
        }
        StartCoroutine(Select());
    }

    public void StealSelection(BaseEventData data)
    {
        var pointerEnterData = (PointerEventData) data;
        // button can be blocked by text
        var button = pointerEnterData.pointerEnter.GetComponentInParent<Button>();
        EventSystem.current.SetSelectedGameObject(button.gameObject);
    }
}
