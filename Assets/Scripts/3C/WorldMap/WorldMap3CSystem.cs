﻿using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.InputSystem;

public class WorldMap3CSystem : ComponentSystem
{
    Battle3CConfig m_config;

    BattleHexNavigationSystem m_navigationSystem;
    List<HexMoveGraphNode> m_coordsInRange = new List<HexMoveGraphNode>();
    List<CubeCoords> m_path = new List<CubeCoords>();
    CubeCoords? m_currentPathDestination;

    protected override void OnCreate()
    {
        base.OnCreate();
        Enabled = false;
    }

    protected override void OnStartRunning()
    {
        base.OnStartRunning();

        m_config = GetSingleton<Battle3CConfig>();
        m_navigationSystem = World.GetExistingSystem<BattleHexNavigationSystem>();
    }

    protected override void OnUpdate()
    {
        Entity hexMapEntity = GetSingletonEntity<HexMapComponent>();
        var creatureMovementEntity = GetSingletonEntity<Battle3CComponent>();
        var creatureMovementComponent = EntityManager.GetComponentData<CreatureMovementComponent>(creatureMovementEntity);
        if (creatureMovementComponent.IsCreatureMoveInProgress)
        {
            return;
        }

        var hexMapComponent = EntityManager.GetComponentData<HexMapComponent>(hexMapEntity);
        Slice2D<HexEntityBufferElement> coordToHex = HexUtils.GetCoordToHexArray(hexMapEntity);
        CubeCoords coordsUnderMouse = HexUtils.GetHexCoordAtScreenPoint(Input.mousePosition, hexMapComponent);

        Entity hexUnderMouse = Entity.Null;
        int2 coords = HexMath.CubeToOffset(coordsUnderMouse);
        if (coordToHex.IsValidIndex(coords))
        {
            hexUnderMouse = coordToHex[coords].Hex;
        }
        Entity playerAvatar = GetSingletonEntity<PlayerAvatarTag>();

        if (m_coordsInRange.Count == 0)
        {
            HighlightMoveArea(playerAvatar, coordToHex, hexMapComponent);
        }

        if (hexUnderMouse != Entity.Null)
        {
            CubeCoords? pathDestination = TryGetPathDestination(hexUnderMouse, hexMapComponent);
            if (!pathDestination.HasValue)
            {
                m_path.Clear();
                return;
            }

            if (Mouse.current.rightButton.wasReleasedThisFrame)
            {
                CoroutineStarter.Instance.StartCoroutine(CreatureMovement.MoveAlongPathCoroutine(
                    playerAvatar, m_path, () =>
                    {
                        var hexMapEntity = GetSingletonEntity<HexMapComponent>();
                        var coordToHex = HexUtils.GetCoordToHexArray(hexMapEntity);
                        var playerAvatar = GetSingletonEntity<PlayerAvatarTag>();

                        var hexMapComponent = EntityManager.GetComponentData<HexMapComponent>(hexMapEntity);
                        HighlightMoveArea(playerAvatar, coordToHex, hexMapComponent);

                        var currentTile = coordToHex[HexMath.CubeToOffset(m_path[m_path.Count - 1])].Hex;
                        WorldHexEffects.OnEnteringHex(currentTile);

                    }, hexMapComponent, EntityManager));
            }
            else
            {
                Debug.Assert(hexUnderMouse != Entity.Null);
                if ((pathDestination != m_currentPathDestination || m_path.Count == 0) && hexUnderMouse != Entity.Null)
                {
                    var translation = EntityManager.GetComponentData<Translation>(playerAvatar);
                    CubeCoords selectedCreatureCoords = HexUtils.GetProjectionHexCoords(translation.Value, hexMapComponent);
                    m_navigationSystem.GetPath(selectedCreatureCoords, pathDestination.Value, HexMapType.World, coordToHex, m_path);

                    m_currentPathDestination = pathDestination.Value;
                }
            }
        }
    }

    CubeCoords? TryGetPathDestination(Entity hexUnderMouse, HexMapComponent hexMapComponent)
    {
        var pathDestination = new CubeCoords?();

        if (hexUnderMouse != Entity.Null &&
            EntityManager.GetComponentData<WorldHexComponent>(hexUnderMouse).IsPassable)
        {
            CubeCoords coordsUnderMouse = HexUtils.GetHexCoordAtScreenPoint(Input.mousePosition, hexMapComponent);
            pathDestination = coordsUnderMouse;
        }

        if (pathDestination.HasValue)
        {
            int i = m_coordsInRange.FindIndex((HexMoveGraphNode node) => node.Coords == pathDestination.Value);
            if (i == -1)
            {
                pathDestination = null;
            }
        }

        return pathDestination;
    }

    void HighlightMoveArea(Entity playerAvatar, Slice2D<HexEntityBufferElement> coordToHex, HexMapComponent hexMapComponent)
    {
        int actionPoints = EntityManager.GetComponentData<ActionPointsComponent>(playerAvatar).CurrentMovementPoints;
        CubeCoords creatureCoords = HexUtils.GetProjectionHexCoords(EntityManager.GetComponentData<Translation>(playerAvatar).Value, hexMapComponent);
        m_navigationSystem.GetTilesInRange(creatureCoords, actionPoints,
            GetTilesInRangeSettings.PassableHexes(HexMapType.World), coordToHex, m_coordsInRange);

        HexMap3CUtils.HighlightCoordsInRange(creatureCoords, m_config.MovementAreaColor, m_coordsInRange, coordToHex, EntityManager);
    }
}