﻿using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

public static class HexMap3CUtils
{
    public static void HighlightCoordsInRange(CubeCoords creatureCoords, Color color, List<HexMoveGraphNode> coordsInRange,
        Slice2D<HexEntityBufferElement> coordToHex, EntityManager entityManager)
    {
        var highlightEntity = EntityQueryUtils.GetSingletonEntity<HexHighlightRequestComponent>();
        entityManager.SetComponentData(highlightEntity, new HexHighlightRequestComponent(entityManager.GetComponentData<HexHighlightRequestComponent>(highlightEntity))
        {
            DehighlightAllRequested = true
        });
        Entity hexUnderCreature = coordToHex[HexMath.CubeToOffset(creatureCoords)].Hex;

        var highlightRequestBuffer = entityManager.GetBuffer<HexesToHighlightBufferElement>(highlightEntity);
        foreach (var node in coordsInRange)
        {
            highlightRequestBuffer.Add(new HexesToHighlightBufferElement { Hex = coordToHex[HexMath.CubeToOffset(node.Coords)].Hex, HighlightColor = color });
        }

        highlightRequestBuffer.Add(new HexesToHighlightBufferElement { Hex = hexUnderCreature, HighlightColor = color });
    }
}
