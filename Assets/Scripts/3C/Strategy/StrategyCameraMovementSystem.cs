﻿using UnityEngine;
using Unity.Entities;
using UnityEngine.InputSystem;

public class StrategyCameraMovementSystem : ComponentSystem
{
    PlayerInput m_playerInput;

    protected override void OnStartRunning()
    {
        base.OnStartRunning();
        m_playerInput = Object.FindObjectOfType<PlayerInput>();
        if (m_playerInput == null)
        {
            Debug.Log("PlayerInput not found");
        }
    }

    protected override void OnUpdate()
    {
        if (m_playerInput == null)
        {
            return;
        }

        Entities.ForEach((ref StrategyCameraMovementComponent cameraMovement, Transform transform) => {
            Vector2 cameraMoveDelta = m_playerInput.actions["CameraMove"].ReadValue<Vector2>();
            
            Vector3 forwardXZ = Vector3.ProjectOnPlane(transform.forward, Vector3.up).normalized;
            Vector3 cameraDisplacement = forwardXZ * cameraMoveDelta.y + 
                transform.right * cameraMoveDelta.x;
            transform.position += cameraDisplacement * cameraMovement.Speed * Time.DeltaTime;

            float rotationDelta = m_playerInput.actions["CameraRotate"].ReadValue<float>();
            transform.Rotate(Vector3.up * rotationDelta * cameraMovement.RotationSpeed * Time.DeltaTime, Space.World);
        });
    }
}
