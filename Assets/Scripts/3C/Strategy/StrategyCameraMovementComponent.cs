﻿using Unity.Entities;

[GenerateAuthoringComponent]
struct StrategyCameraMovementComponent : IComponentData
{
    public float Speed;
    public float RotationSpeed;
}
