﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Unity.Entities;

[GenerateAuthoringComponent]
public struct Battle3CComponent : IComponentData
{
    public Entity SelectedCreature;
    // used to re-highlight move area after the non-interactable period during ability animation
    public bool MoveAreaReHighlightNeeded;

    // of the selected creature
    public AbilityType CurrentTargetingAbility;
    public bool AbilityTargetingStarted;

    public Battle3CComponent(Battle3CComponent other) => this = other;
}
