﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

// creature selection/highlight, targeting previews
public class Battle3CSystem : ComponentSystem
{
    Battle3CConfig m_config;

    GameObject m_highlightedCreature;

    BattleHexNavigationSystem m_navigationSystem;
    List<HexMoveGraphNode> m_coordsInRange = new List<HexMoveGraphNode>();
    List<CubeCoords> m_path = new List<CubeCoords>();
    CubeCoords? m_currentPathDestination;

    public static (Entity Creature, Entity Hex) GetCreatureAndHexUnderMouse(Slice2D<HexEntityBufferElement> coordToHex,
        HexMapComponent hexMapComponent, CubeCoords coordsUnderMouse, EntityManager entityManager)
    {
        Entity creatureUnderMouse = Entity.Null;
        Entity hexUnderMouse = Entity.Null;

        if (!EventSystem.current.IsPointerOverGameObject())
        {
            bool mouseOnCreature = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hitResult,
                maxDistance: float.MaxValue, LayerMaskUtils.GetLayerMask(UnityLayer.Creature));

            if (mouseOnCreature)
            {
                int2 creatureCoords = HexMath.CubeToOffset(HexUtils.GetProjectionHexCoords(hitResult.transform.position, hexMapComponent));

                if (coordToHex.IsValidIndex(creatureCoords))
                {
                    hexUnderMouse = coordToHex[creatureCoords].Hex;
                }
                else
                {
                    Debug.LogError("creature gameobject not above hex");
                }
            }
        }

        if (hexUnderMouse == Entity.Null)
        {
            int2 coords = HexMath.CubeToOffset(coordsUnderMouse);
            if (coordToHex.IsValidIndex(coords))
            {
                hexUnderMouse = coordToHex[coords].Hex;
            }
        }
        else
        {
            var battleHexComponent = entityManager.GetComponentData<BattleHexComponent>(hexUnderMouse);

            if (battleHexComponent.OccupyingCreature != Entity.Null)
            {
                creatureUnderMouse = battleHexComponent.OccupyingCreature;
            }
        }

        return (creatureUnderMouse, hexUnderMouse);
    }

    public void GetDebugState(List<(string Label, string Value)> outValues)
    {
        outValues.Clear();
        outValues.Add(("m_highlightedCreature", ToStringUtils.StringifyNull(m_highlightedCreature)));
        outValues.Add(("m_path", JsonConvert.SerializeObject(m_path)));
        outValues.Add(("m_coordsInRange", JsonConvert.SerializeObject(m_coordsInRange, Formatting.Indented)));
        outValues.Add(("m_currentPathDestination", ToStringUtils.StringifyNull(m_currentPathDestination)));
    }

    protected override void OnCreate()
    {
        base.OnCreate();
        Enabled = false;
    }

    protected override void OnStartRunning()
    {
        base.OnStartRunning();

        m_config = GetSingleton<Battle3CConfig>();
        m_navigationSystem = World.GetExistingSystem<BattleHexNavigationSystem>();
    }

    protected override void OnUpdate()
    {
        Entity hexMapEntity = GetSingletonEntity<HexMapComponent>();
        var battle3CEntity = GetSingletonEntity<Battle3CComponent>();
        var creatureMovementComponent = EntityManager.GetComponentData<CreatureMovementComponent>(battle3CEntity);
        if (creatureMovementComponent.IsCreatureMoveInProgress)
        {
            return;
        }

        var battle3CComponent = EntityManager.GetComponentData<Battle3CComponent>(battle3CEntity);
        var hexMapComponent = EntityManager.GetComponentData<HexMapComponent>(hexMapEntity);
        Slice2D<HexEntityBufferElement> coordToHex = HexUtils.GetCoordToHexArray(hexMapEntity);
        CubeCoords coordsUnderMouse = HexUtils.GetHexCoordAtScreenPoint(Input.mousePosition, hexMapComponent);
        (Entity creatureUnderMouse, Entity hexUnderMouse) = GetCreatureAndHexUnderMouse(
            coordToHex, hexMapComponent, coordsUnderMouse, EntityManager);

        var creatureUnderMouseGo = creatureUnderMouse == Entity.Null ?
            null : EntityManager.GetLinkedGameObject(creatureUnderMouse);

        if (battle3CComponent.CurrentTargetingAbility != AbilityType.Invalid)
        {
            HandleInputForAbilityTargeting(hexUnderMouse, ref battle3CComponent, hexMapComponent, coordToHex);
            EntityManager.SetComponentData(battle3CEntity, battle3CComponent);
            return;
        }

        bool hovering = creatureUnderMouse != Entity.Null && hexUnderMouse != Entity.Null;
        if (hovering)
        {
            // ignore selected creature
            if (creatureUnderMouse != battle3CComponent.SelectedCreature)
            {
                var creatureRenderer = creatureUnderMouseGo.GetComponentInChildren<Renderer>();
                if (creatureRenderer != null)
                {
                    var creatureComponent = EntityManager.GetComponentData<CreatureComponent>(creatureUnderMouse);

                    (var turnComponent, bool _) = EntityQueryUtils.GetFirstMatchingComponentLogFailure<BattleTurnComponent>();

                    if (Mouse.current.leftButton.wasPressedThisFrame &&
                        creatureComponent.Team == turnComponent.CurrentActingTeam)
                    {
                        if (battle3CComponent.SelectedCreature != Entity.Null)
                        {
                            DeselectCurrent(ref battle3CComponent);
                        }

                        creatureRenderer.renderingLayerMask = (int) RenderLayerMask.Outline;

                        if (EntityManager.GetComponentData<IsInteractableComponent>(creatureUnderMouse).Value)
                        {
                            HighlightMoveArea(creatureUnderMouse, coordToHex, hexMapComponent);
                        }

                        battle3CComponent.SelectedCreature = creatureUnderMouse;
                        m_highlightedCreature = null;
                    }
                    else if (m_highlightedCreature != creatureUnderMouseGo)
                    {
                        if (m_highlightedCreature != null)
                        {
                            DehighlightCreature(m_highlightedCreature, hexMapComponent, coordToHex);
                        }
                        m_highlightedCreature = creatureUnderMouseGo;

                        Color color = creatureComponent.Team == Team.Player ?
                            m_config.OwnCreatureHoverColor : m_config.EnemyHoverColor;

                        var highlightRequestBuffer = EntityManager.GetBuffer<HexesToHighlightBufferElement>(
                            GetSingletonEntity<HexesToHighlightBufferElement>());
                        highlightRequestBuffer.Add(new HexesToHighlightBufferElement { Hex = hexUnderMouse, HighlightColor = color });
                    }
                }
                else
                {
                    Debug.LogError("no creature renderer");
                }
            }
        }
        else if (m_highlightedCreature != null)
        {
            DehighlightCreature(m_highlightedCreature, hexMapComponent, coordToHex);
            m_highlightedCreature = null;
        }

        HandleInputForSelectedCreature(creatureUnderMouseGo, hexUnderMouse, ref battle3CComponent, hexMapComponent, coordToHex);

        if (Mouse.current.leftButton.wasPressedThisFrame &&
            !EventSystem.current.IsPointerOverGameObject() &&
            creatureUnderMouse != battle3CComponent.SelectedCreature &&
            battle3CComponent.SelectedCreature != Entity.Null)
        {
            DeselectCurrent(ref battle3CComponent);
        }

        EntityManager.SetComponentData(battle3CEntity, battle3CComponent);
    }

    void HandleInputForAbilityTargeting(Entity hexUnderMouse,
        ref Battle3CComponent battle3CComponent, HexMapComponent hexMapComponent, Slice2D<HexEntityBufferElement> coordToHex)
    {
        if (!EntityManager.GetComponentData<IsInteractableComponent>(battle3CComponent.SelectedCreature).Value)
        {
            return;
        }

        if (!battle3CComponent.AbilityTargetingStarted)
        {
            m_path.Clear();
            Debug.Assert(m_highlightedCreature == null);

            var abilities = EntityManager.GetBuffer<AbilityBufferElement>(battle3CComponent.SelectedCreature);

            var currentTargetingAbility = battle3CComponent.CurrentTargetingAbility;
            int index = abilities.FindIndex((AbilityBufferElement element) =>
            {
                return element.Id == currentTargetingAbility;
            });

            if (index == -1)
            {
                Debug.LogError($"no requested ability {battle3CComponent.CurrentTargetingAbility} on selected creature");
                return;
            }
            var requestedAbility = abilities[index];

            // case 1: single target ability
            var attackAbilityData = EntityManager.GetComponentData<AttackAbilityComponent>(battle3CComponent.SelectedCreature);
            var translation = EntityManager.GetComponentData<Translation>(battle3CComponent.SelectedCreature);
            CubeCoords selectedCreatureCoords = HexUtils.GetProjectionHexCoords(translation.Value, hexMapComponent);


            var turnComponent = GetSingleton<BattleTurnComponent>();
            m_navigationSystem.GetTilesInRange(
                selectedCreatureCoords, attackAbilityData.Range, 
                GetTilesInRangeSettings.TargetableHexes(turnComponent.CurrentActingTeam.Enemy()), 
                coordToHex, m_coordsInRange);
            HexMap3CUtils.HighlightCoordsInRange(selectedCreatureCoords, m_config.TargetingAreaColor, m_coordsInRange, coordToHex, EntityManager);

            battle3CComponent.AbilityTargetingStarted = true;
        }

        static void CancelTargeting(ref Battle3CComponent component)
        {
            component.CurrentTargetingAbility = AbilityType.Invalid;
            component.AbilityTargetingStarted = false;
        }

        if (Mouse.current.rightButton.wasReleasedThisFrame)
        {
            CancelTargeting(ref battle3CComponent);
            HighlightMoveArea(battle3CComponent.SelectedCreature, coordToHex, hexMapComponent);
            return;
        }

        var highlightEntity = GetEntityQuery(typeof(HexHighlightRequestComponent)).GetFirstEntityWithComponentLogFailure<HexHighlightRequestComponent>();
        var currentlyHighlightedHexes = EntityManager.GetBuffer<CurrentHighlightedHexesBufferElement>(highlightEntity);
        var highlightBuffer = EntityManager.GetBuffer<HexesToHighlightBufferElement>(highlightEntity);

        for (int i = 0; i < currentlyHighlightedHexes.Length; i++)
        {
            ref var hex = ref currentlyHighlightedHexes.ElementAt(i);
            if (hex.Hex == hexUnderMouse)
            {
                continue;
            }

            if (HexHighlightSystem.GetHighlightColor(hex.Hex, EntityManager) == m_config.TargetingHexColor)
            {
                highlightBuffer.Add(new HexesToHighlightBufferElement { Hex = hex.Hex, HighlightColor = m_config.TargetingAreaColor });
            }
        }

        if (hexUnderMouse == Entity.Null)
        {
            return;
        }
        CubeCoords hexUnderMouseCoords = HexUtils.GetProjectionHexCoords(EntityManager.GetComponentData<Translation>(hexUnderMouse).Value, hexMapComponent);
        if (!m_coordsInRange.Exists((HexMoveGraphNode node) => node.Coords == hexUnderMouseCoords))
        {
            return;
        }

        if (Mouse.current.leftButton.wasReleasedThisFrame && !EventSystem.current.IsPointerOverGameObject())
        {
            if (Damage.IsHexDamageable(hexUnderMouse, EntityManager))
            {
                CreatureAttackAbility.StartAttackAbility(battle3CComponent.SelectedCreature, hexUnderMouse, EntityManager);

                EntityManager.SetComponentData(highlightEntity,
                    new HexHighlightRequestComponent(EntityManager.GetComponentData<HexHighlightRequestComponent>(highlightEntity))
                    {
                        DehighlightAllRequested = true
                    });

                var translation = EntityManager.GetComponentData<Translation>(battle3CComponent.SelectedCreature);
                int2 coordsUnderCreature = HexMath.CubeToOffset(
                    HexUtils.GetProjectionHexCoords(translation.Value, hexMapComponent));
                highlightBuffer.Add(new HexesToHighlightBufferElement
                {
                    Hex = coordToHex[coordsUnderCreature].Hex,
                    HighlightColor = m_config.MovementAreaColor
                });

                battle3CComponent.MoveAreaReHighlightNeeded = true;
                CancelTargeting(ref battle3CComponent);
            }
        }
        else // hovering
        {
            highlightBuffer.Add(new HexesToHighlightBufferElement
            {
                Hex = hexUnderMouse,
                HighlightColor = m_config.TargetingHexColor
            });
        }
    }

    void HighlightMoveArea(Entity creature, Slice2D<HexEntityBufferElement> coordToHex, HexMapComponent hexMapComponent)
    {
        int actionPoints = EntityManager.GetComponentData<ActionPointsComponent>(creature).CurrentMovementPoints;
        CubeCoords creatureCoords = HexUtils.GetProjectionHexCoords(EntityManager.GetComponentData<Translation>(creature).Value, hexMapComponent);
        m_navigationSystem.GetTilesInRange(creatureCoords, actionPoints, 
            GetTilesInRangeSettings.PassableHexes(HexMapType.Battle), coordToHex, m_coordsInRange);

        HexMap3CUtils.HighlightCoordsInRange(creatureCoords, m_config.MovementAreaColor, m_coordsInRange, coordToHex, EntityManager);
    }

    void HandleInputForSelectedCreature(GameObject creatureUnderMouse, Entity hexUnderMouse,
        ref Battle3CComponent battle3CComponent, HexMapComponent hexMapComponent, Slice2D<HexEntityBufferElement> coordToHex)
    {
        if (battle3CComponent.SelectedCreature == Entity.Null)
        {
            return;
        }

        if (!EntityManager.GetComponentData<IsInteractableComponent>(battle3CComponent.SelectedCreature).Value)
        {
            return;
        }

        if (battle3CComponent.MoveAreaReHighlightNeeded)
        {
            HighlightMoveArea(battle3CComponent.SelectedCreature, coordToHex, hexMapComponent);
            battle3CComponent.MoveAreaReHighlightNeeded = false;
        }

        (CubeCoords? pathDestination, bool findPathToAdjacentHex) = TryGetPathDestination(creatureUnderMouse, hexUnderMouse, battle3CComponent.SelectedCreature, hexMapComponent);

        if (!pathDestination.HasValue)
        {
            m_path.Clear();
            return;
        }


        if (Mouse.current.rightButton.wasReleasedThisFrame)
        {
            CoroutineStarter.Instance.StartCoroutine(CreatureMovement.MoveAlongPathCoroutine(
                battle3CComponent.SelectedCreature, m_path, () =>
                {
                    var hexMapEntity = GetSingletonEntity<HexMapComponent>();
                    var coordToHex = HexUtils.GetCoordToHexArray(hexMapEntity);
                    var battle3CEntity = GetSingletonEntity<Battle3CComponent>();
                    var battle3CComponent = EntityManager.GetComponentData<Battle3CComponent>(battle3CEntity);
                    CreatureBattleActions.OnMove(m_path[0], m_path[m_path.Count - 1], battle3CComponent.SelectedCreature,
                        EntityManager, coordToHex, m_path.Count - 1);

                    var hexMapComponent = EntityManager.GetComponentData<HexMapComponent>(hexMapEntity);
                    HighlightMoveArea(battle3CComponent.SelectedCreature, coordToHex, hexMapComponent);
                }, 
                hexMapComponent, EntityManager));
        }
        else
        {
            Debug.Assert(hexUnderMouse != Entity.Null);
            if ((pathDestination != m_currentPathDestination || m_path.Count == 0) && hexUnderMouse != Entity.Null)
            {
                var translation = EntityManager.GetComponentData<Translation>(battle3CComponent.SelectedCreature);
                CubeCoords selectedCreatureCoords = HexUtils.GetProjectionHexCoords(translation.Value, hexMapComponent);
                m_navigationSystem.GetPath(selectedCreatureCoords, pathDestination.Value, HexMapType.Battle, coordToHex, m_path, findPathToAdjacentHex);

                m_currentPathDestination = pathDestination.Value;
            }
        }
    }

    (CubeCoords?, bool) TryGetPathDestination(GameObject creatureUnderMouse, Entity hexUnderMouse,
        Entity selectedCreature, HexMapComponent hexMapComponent)
    {
        var pathDestination = new CubeCoords?();
        bool findPathToAdjacentHex = false;
        var selectedCreatureGo = EntityManager.GetLinkedGameObject(selectedCreature);
        if (creatureUnderMouse != selectedCreatureGo)
        {
            if (creatureUnderMouse == null)
            {
                if (hexUnderMouse != Entity.Null &&
                    EntityManager.GetComponentData<BattleHexComponent>(hexUnderMouse).IsPassable)
                {
                    CubeCoords coordsUnderMouse = HexUtils.GetHexCoordAtScreenPoint(Input.mousePosition, hexMapComponent);
                    pathDestination = coordsUnderMouse;
                }
            }
            else
            {
                pathDestination = HexUtils.GetProjectionHexCoords(creatureUnderMouse.transform.position, hexMapComponent);
                findPathToAdjacentHex = true;
            }
        }

        if (pathDestination.HasValue)
        {
            int i = m_coordsInRange.FindIndex((HexMoveGraphNode node) => node.Coords == pathDestination.Value);
            if (i == -1)
            {
                pathDestination = null;
            }
        }

        return (pathDestination, findPathToAdjacentHex);
    }

    static Entity GetHexUnderCreature(GameObject creature, HexMapComponent hexMapComponent, Slice2D<HexEntityBufferElement> coordToHex)
    {
        int2 creatureCoords = HexMath.CubeToOffset(HexUtils.GetProjectionHexCoords(creature.transform.position, hexMapComponent));

        return coordToHex.IsValidIndex(creatureCoords) ?
            coordToHex[creatureCoords].Hex : Entity.Null;
    }

    void DehighlightCreature(GameObject creature, HexMapComponent hexMapComponent, Slice2D<HexEntityBufferElement> coordToHex)
    {
        if (creature.TryGetComponent(out Renderer renderer))
        {
            renderer.renderingLayerMask = (int) RenderLayerMask.Default;
        }

        var entity = GetEntityQuery(typeof(HexHighlightRequestComponent)).GetFirstEntityWithComponentLogFailure<HexHighlightRequestComponent>();
        var hexesToDehighlight = EntityManager.GetBuffer<HexesToDehighlightBufferElement>(entity);
        Entity hexUnderCreature = GetHexUnderCreature(creature, hexMapComponent, coordToHex);
        hexesToDehighlight.Add(new HexesToDehighlightBufferElement { Hex = hexUnderCreature });


        Debug.Log("DehighlightCreature");
    }

    void DeselectCurrent(ref Battle3CComponent battle3CComponent)
    {
        var selectedCreatureGo = EntityManager.GetLinkedGameObject(battle3CComponent.SelectedCreature);
        var renderer = selectedCreatureGo.GetComponentInChildren<Renderer>();
        renderer.renderingLayerMask = (int) RenderLayerMask.Default;

        var entity = GetEntityQuery(typeof(HexHighlightRequestComponent)).GetFirstEntityWithComponentLogFailure<HexHighlightRequestComponent>();
        EntityManager.SetComponentData(entity, new HexHighlightRequestComponent(EntityManager.GetComponentData<HexHighlightRequestComponent>(entity))
        {
            DehighlightAllRequested = true
        });

        battle3CComponent.SelectedCreature = Entity.Null;
        m_coordsInRange.Clear();
        m_path.Clear();
        m_currentPathDestination = null;
    }
}
