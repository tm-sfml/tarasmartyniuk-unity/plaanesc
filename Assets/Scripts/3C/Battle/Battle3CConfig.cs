﻿using Unity.Entities;
using UnityEngine;

[GenerateAuthoringComponent]
public struct Battle3CConfig : IComponentData
{
    [ColorUsage(true, true)]
    public Color OwnCreatureHoverColor;
    [ColorUsage(true, true)]
    public Color EnemyHoverColor;
    [ColorUsage(true, true)]
    public Color MovementAreaColor;
    [ColorUsage(true, true)]
    public Color TargetingAreaColor;
    [ColorUsage(true, true)]
    public Color TargetingHexColor;
    public Entity PathMarkerPrefab;
}
