﻿using Unity.Entities;

public static class DeckUtils
{
    public static Entity FindDeckEntity(DeckId id)
    {
        var builder = EntityQueryUtils.CreateEntityQueryBuilder().WithAll<DeckBufferElement, DeckIdComponent>();
        return builder.GetSingleEntityWithComponentLogFailure(
            (DeckIdComponent deckId) => deckId.Value == id);
    }
}
