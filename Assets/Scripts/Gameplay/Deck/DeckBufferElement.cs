﻿using Unity.Entities;

// stores all instanced card properties (definition + modifiers)
// persists between battles, through game run
public struct DeckBufferElement : IBufferElementData
{
    public CardDefinitionId DefinitionId;
}
