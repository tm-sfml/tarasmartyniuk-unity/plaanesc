﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct DeckIdComponent : IComponentData
{
    public DeckId Value;
}
