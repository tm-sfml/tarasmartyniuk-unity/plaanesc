﻿using System;
using Unity.Entities;

// default card properties, without any modifiers
// stored in as a map {card id: definition}
[Serializable]
public struct CardDefinitionBufferElement : IBufferElementData
{
    public Entity Entity;
}
