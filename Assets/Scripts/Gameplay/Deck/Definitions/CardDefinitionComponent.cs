﻿using Unity.Entities;
using System;

[Serializable]
public struct CardDefinitionComponent : IComponentData
{
    public CardBehavior CardBehavior;
    public int ManaCost;
}
