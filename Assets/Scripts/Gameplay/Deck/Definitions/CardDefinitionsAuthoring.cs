﻿using System;
using UnityEngine;
using Unity.Entities;
using System.IO;
using Newtonsoft.Json.Linq;


public class CardDefinitionsAuthoring : MonoBehaviour, IConvertGameObjectToEntity
{
    [SerializeField] string JsonFilename;

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        string tilemapFilename = Path.Combine(Application.streamingAssetsPath, JsonFilename);
        JArray definitions = JArray.Parse(File.ReadAllText(tilemapFilename));
        dstManager.AddBuffer<CardDefinitionBufferElement>(entity);

        int i = 0;
        foreach (var definitionJsonToken in definitions)
        {
            var cardDefinitionComponent = definitionJsonToken["CardDefinitionComponent"].ToObject<CardDefinitionComponent>();

            var definitionEntity = dstManager.CreateEntity();
            dstManager.AddComponentData(definitionEntity, cardDefinitionComponent);

            switch (cardDefinitionComponent.CardBehavior)       
            {
                case CardBehavior.Creature:
                    var creatureDefComponent = definitionJsonToken["CreatureCardDefinitionComponent"].ToObject<CreatureCardDefinitionComponent>();
                    dstManager.AddComponentData(definitionEntity, creatureDefComponent);
                    break;

                case CardBehavior.Spell_DirectDamageSingleTarget:
                    var spellDefComponent = definitionJsonToken["Spell_DirectDamageDefinitionComponent"].ToObject<Spell_DirectDamageDefinitionComponent>();
                    dstManager.AddComponentData(definitionEntity, spellDefComponent);
                    break;

                default:
                    Debug.LogError($"invalid card behavior ({cardDefinitionComponent.CardBehavior})for definition with id {definitionJsonToken["DefinitionId"]}");
                    break;
            }

            var buffer = dstManager.GetBuffer<CardDefinitionBufferElement>(entity);
            buffer.Add(new CardDefinitionBufferElement { Entity = definitionEntity });

            bool success = Enum.TryParse(definitionJsonToken.Value<string>("DefinitionId"), out CardDefinitionId cardDefinitionId);
            Debug.Assert(success && EnumUtils.ToInt(cardDefinitionId) == i, "card definition list index must be the same as its ToInt(\"DefinitionId\") value");
            i++;
        }

        Destroy(this);
    }
}
