﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct CreatureCardDefinitionComponent : IComponentData
{
    public CreatureDefinitionId CreatureId;
}
