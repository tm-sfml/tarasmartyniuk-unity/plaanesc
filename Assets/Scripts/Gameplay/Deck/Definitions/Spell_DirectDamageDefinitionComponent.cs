﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct Spell_DirectDamageDefinitionComponent : IComponentData
{
    public int Damage;
}
