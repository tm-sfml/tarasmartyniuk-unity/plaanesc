﻿
public enum CardDefinitionId
{
    Invalid,
    RangedWarrior,
    Mage,
    EnemyRangedWarrior,
    EnemyMage,
    SpellPlaceholder
}
