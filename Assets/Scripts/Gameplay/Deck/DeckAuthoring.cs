using UnityEngine;
using Unity.Entities;
using System;
using System.Collections.Generic;

[Serializable]
struct StartingDeckEntry
{
    public CardDefinitionId CardId;
    public int Count;
}

public class DeckAuthoring : MonoBehaviour, IConvertGameObjectToEntity
{
    [SerializeField] List<StartingDeckEntry> Deck;

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        var buffer = dstManager.AddBuffer<DeckBufferElement>(entity);
        foreach (var entry in Deck)
        {
            for (int i = 0; i < entry.Count; i++)
            {
                buffer.Add(new DeckBufferElement { DefinitionId = entry.CardId });
            }
        }
    }
}
