﻿using UnityEngine;
using UnityEngine.AI;
using Unity.Entities;

public class CreepAIStateMoveToFightArea : IAIState<CreepAISharedDataComponent>
{
    public override void OnTransition(ref CreepAISharedDataComponent sharedData, Entity agent, EntityManager entityManager)
    {
        var navigation = entityManager.GetComponentObject<NavMeshAgent>(agent);
        if (navigation == null)
        {
            Debug.LogError("navigation component missing");
            return;
        }

        var transform = entityManager.GetComponentObject<Transform>(sharedData.MoveMarker);
        navigation.SetDestination(transform.position);
    }

    public override void Update(ref CreepAISharedDataComponent sharedData, ref StateMachineComponent stateData, Entity agent, EntityManager entityManager)
    {
        var navigation = entityManager.GetComponentObject<NavMeshAgent>(agent);
        if (navigation == null)
        {
            Debug.LogError("navigation component missing");
            return;
        }

        if (!navigation.pathPending && navigation.remainingDistance <= .2f)
        {
            navigation.isStopped = true;
            stateData.SetCurrentStateAs(CreepAIState.FightCreeps);
        }
    }
}
