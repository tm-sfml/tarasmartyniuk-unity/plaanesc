﻿using Unity.Entities;
using System.Collections.Generic;


// state machine systems define the set of states,
// handle stage change and delegating execution to current state
public class CreepAIStateMachineSystem : ComponentSystem
{
    Dictionary<CreepAIState, IAIState<CreepAISharedDataComponent>> m_states;

    public CreepAIStateMachineSystem() : base()
    {
        m_states = new Dictionary<CreepAIState, IAIState<CreepAISharedDataComponent>>()
        {
            { CreepAIState.MoveToFightArea, new CreepAIStateMoveToFightArea() }
        };
    }

    protected override void OnStartRunning()
    {
        AIStateMachineImpl.RunInitialStates(m_states, Entities, EntityManager);
    }

    protected override void OnUpdate()
    {
        AIStateMachineImpl.HandleStateChange(m_states, Entities, EntityManager);
    }
}
