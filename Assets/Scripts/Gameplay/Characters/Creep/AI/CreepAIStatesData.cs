﻿public enum CreepAIState
{
    MoveToFightArea,
    FightCreeps,
    AttackShrine,
    Count,
    Invalid
}
