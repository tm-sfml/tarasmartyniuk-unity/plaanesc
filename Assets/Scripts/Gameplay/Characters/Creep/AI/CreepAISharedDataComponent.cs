﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct CreepAISharedDataComponent : IComponentData
{
    public Entity FightAreaMarker;
    public Entity MoveMarker;
}
