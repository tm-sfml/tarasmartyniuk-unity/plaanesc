﻿using UnityEngine;
using Unity.Entities;

public abstract class AIBackgroundService<TSharedStateData>
{
    public float TickInterval { get; set; }

    public abstract void Tick(ref TSharedStateData sharedData, Entity agent, EntityManager entityManager);
}