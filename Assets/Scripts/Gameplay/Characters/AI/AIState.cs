﻿using Unity.Entities;
using Unity.Collections;
using UnityEngine;

/// <summary>
/// should be stateless
/// for each agent, ai data is organized as follows:
/// entity { AIStateChangeData, SharedStateData, State1Data ... StateNData }
/// </summary>
public class IAIState<TSharedStateData>
{
    // fixme: leaks (impl IDisposable)
    protected NativeArray<(int TimerHandle, AIBackgroundService<TSharedStateData> Service)> m_BackgroundServices;

    public virtual void OnTransition(ref TSharedStateData sharedData, Entity agentEntity, EntityManager entityManager)
    {
        RangeInt timersRange = TimerManager.Instance.AquireTimers(m_BackgroundServices.Length);
        int timerId = timersRange.start;

        for (int i = 0; i < m_BackgroundServices.Length; i++, timerId++)
        {
            (int _, var service) = m_BackgroundServices[i];
            m_BackgroundServices[i] = (timerId, service);

            TimerManager.Instance.SetTimer(timerId, service.TickInterval);
        }
    }


    // todo: For  state data, add unsafe pointer param
    public virtual void Update(ref TSharedStateData sharedData, ref StateMachineComponent stateMachineComponent, Entity agentEntity, EntityManager entityManager)
    {
        foreach ((int timerHandle, var service) in m_BackgroundServices)
        {
            (var timer, bool success) = TimerManager.Instance.GetTimerData(timerHandle);
            Debug.Assert(success);

            if (timer.IsExpired)
            {
                service.Tick(ref sharedData, agentEntity, entityManager);
                TimerManager.Instance.RestartTimer(timerHandle);
            }
        }
    }
}
