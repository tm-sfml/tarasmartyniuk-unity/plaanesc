﻿using Unity.Entities;
using UnityEngine;
using System.Runtime.CompilerServices;
using System;

[GenerateAuthoringComponent]
public struct StateMachineComponent : IComponentData
{
    // casted to custom enums
    public int CurrentState;
    [HideInInspector] 
    public bool IsChangedThisTick;

    public TStateEnum GetCurrentStateAs<TStateEnum>()
        where TStateEnum : Enum
    {
        return Unsafe.As<int, TStateEnum>(ref CurrentState);
    }

    public void SetCurrentStateAs<TStateEnum>(TStateEnum state)
        where TStateEnum : Enum
    {
        CurrentState = Unsafe.As<TStateEnum, int>(ref state);
    }
}
