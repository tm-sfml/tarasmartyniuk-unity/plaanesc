﻿#define AI_STATE_MACHINE_LOGS
using UnityEngine;
using System.Collections.Generic;
using Unity.Entities;
using System;
using System.Runtime.CompilerServices;


// utility to be reused in specific state machines
public static class AIStateMachineImpl
{
    public static void RunInitialStates<TSharedData, TStateEnum>(Dictionary<TStateEnum, IAIState<TSharedData>> states, EntityQueryBuilder entities, EntityManager entityManager)
        where TSharedData : struct, IComponentData
        where TStateEnum : Enum
    {
        entities.ForEach((Entity entity, ref TSharedData sharedData, ref StateMachineComponent stateMachineComponent) => {
            Debug.Log("found");

            var currentState = Unsafe.As<int, TStateEnum>(ref stateMachineComponent.CurrentState);

            if(!states.TryGetValue(currentState, out IAIState<TSharedData> initialState))
            {
                Debug.LogError($"state missing for {currentState}");
                return;
            }

            initialState.OnTransition(ref sharedData, entity, entityManager);
        });
    }

    public static void HandleStateChange<TSharedData, TStateEnum>(Dictionary<TStateEnum, IAIState<TSharedData>> states, EntityQueryBuilder entities, EntityManager entityManager)
        where TSharedData : struct, IComponentData
        where TStateEnum : Enum
    {
        entities.ForEach((Entity entity, ref StateMachineComponent stateData, ref TSharedData sharedData) => {
            var currStateEnum = Unsafe.As<int, TStateEnum>(ref stateData.CurrentState);

            if (!states.TryGetValue(currStateEnum, out IAIState<TSharedData> currState))
            {
                Debug.LogError($"state missing for {currStateEnum}");
                return;
            }

            if (stateData.IsChangedThisTick)
            {
                currState.OnTransition(ref sharedData, entity, entityManager);
                stateData.IsChangedThisTick = false;
            }

            currState.Update(ref sharedData, ref stateData, entity, entityManager);
        });
    }
}
