﻿using Unity.Entities;
using UnityEngine;

[GenerateAuthoringComponent]
public struct DilemmaComponent : IComponentData
{
    public DilemmaEncounterId DilemmaEncounterId;
    // serve as 2 dimentions for the DilemmaEncounterEffectBufferElement
    [HideInInspector] public int OptionsCount;
    [HideInInspector] public int MaxEffectsCount;

    public DilemmaComponent(DilemmaComponent other) => this = other;
}
