using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

// parses dilemma definitions from json and stores them as DynamicBuffers (Entity prefabs with buffer attached)
public class DilemmaDefinitions : MonoBehaviour
{
    [SerializeField] string DefinitionsJsonFilename;
    Dictionary<DilemmaEncounterId, Entity> m_dilemmaIdToPrefab = new Dictionary<DilemmaEncounterId, Entity>();

    public Entity GetDefinition(DilemmaEncounterId dilemmaId) => m_dilemmaIdToPrefab[dilemmaId];

    void Awake()
    {
        string tilemapFilename = Path.Combine(Application.streamingAssetsPath, DefinitionsJsonFilename);
        JArray definitions = JArray.Parse(File.ReadAllText(tilemapFilename));

        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;

        foreach (var definitionJsonToken in definitions)
        {
            bool success = Enum.TryParse(definitionJsonToken.Value<string>("DilemmaEncounterId"), out DilemmaEncounterId dilemmaEncounterId);
            Debug.Assert(success);
            var options = definitionJsonToken["Options"].ToObject<List<List<JToken>>>();
            int maxEffectsCount = options.Max((List<JToken> effects) => effects.Count);

            Debug.Log(JsonConvert.SerializeObject(options));
            var definitionPrefab = entityManager.CreateEntity();
            var dilemmaComponent = new DilemmaComponent
            {
                DilemmaEncounterId = dilemmaEncounterId,
                OptionsCount = options.Count,
                MaxEffectsCount = maxEffectsCount
            };
            entityManager.AddComponentData(definitionPrefab, dilemmaComponent);
            var buffer = entityManager.AddBuffer<DilemmaEncounterEffectBufferElement>(definitionPrefab);
            buffer.ResizeUninitialized(options.Count * maxEffectsCount);

            for (int i = 0; i < options.Count; i++)
            {
                for (int j = 0; j < options[i].Count; j++)
                {
                    success = Enum.TryParse(options[i][j].Value<string>("EffectType"), out DilemmaEncounterEffectType effectType);
                    Debug.Assert(success);

                    var effectDefinitionEntity = ParseEffectJson(options[i][j], effectType);
                    if (effectDefinitionEntity == Entity.Null)
                    {
                        Debug.LogWarning($"Invalid dilemma definition effect: option #{i}, effect #{j}");
                        continue;
                    }

                    buffer = entityManager.GetBuffer<DilemmaEncounterEffectBufferElement>(definitionPrefab);
                    var optionToEffects = new Slice2D<DilemmaEncounterEffectBufferElement>(dilemmaComponent.MaxEffectsCount, 
                        new NativeSlice<DilemmaEncounterEffectBufferElement>(buffer.AsNativeArray()));
                    optionToEffects[i, j] = new DilemmaEncounterEffectBufferElement
                    {
                        Type = effectType,
                        EffectData = effectDefinitionEntity
                    };
                }
            }

            m_dilemmaIdToPrefab[dilemmaEncounterId] = definitionPrefab;
        }

        static Entity ParseEffectJson(JToken effectToken, DilemmaEncounterEffectType effectType)
        {
            var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            var effectDefinitionEntity = entityManager.CreateEntity();
            var effectDataToken = effectToken["EffectData"];

            switch (effectType)
            {
            case DilemmaEncounterEffectType.GoldChange:
                entityManager.AddComponentData(effectDefinitionEntity, effectDataToken.ToObject<GoldChangeDilemmaEffectComponent>());
                break;
            default:
                entityManager.DestroyEntity(effectDefinitionEntity);
                return Entity.Null;
            }

            return effectDefinitionEntity;
        }
    }

}
