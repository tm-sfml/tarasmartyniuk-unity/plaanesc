﻿using Unity.Entities;

public struct GoldChangeDilemmaEffectComponent : IComponentData
{
    public int GoldChange;
}
