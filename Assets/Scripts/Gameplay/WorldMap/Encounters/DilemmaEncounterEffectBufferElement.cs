﻿using System;
using Unity.Entities;

[Serializable]
public struct DilemmaEncounterEffectBufferElement : IBufferElementData
{
    public DilemmaEncounterEffectType Type;
    // has a single component of a type determined by DilemmaEncounterEffectType
    public Entity EffectData;
}
