﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct WorldHexComponent : IComponentData
{
    public bool IsPassable;
    public WorldHexType Type;

    public WorldHexComponent(WorldHexComponent other) => this = other;
}


