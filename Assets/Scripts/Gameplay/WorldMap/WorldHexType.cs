﻿public enum WorldHexType
{
    Invalid,
    Normal,
    Start,
    Portal,
    DilemmaEncounter
}