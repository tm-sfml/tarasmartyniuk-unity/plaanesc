﻿using System;
using Unity.Entities;

public static class WorldHexEffects
{
    public static void OnEnteringHex(Entity hex)
    {
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;

        var worldHexComponent = entityManager.GetComponentData<WorldHexComponent>(hex);
        if (worldHexComponent.Type == WorldHexType.DilemmaEncounter)
        {
            DilemmaUI.Instance.Show(hex);
        }
    }
}
