using System;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public enum BootstrapSettings
{
    Invalid,
    BattleMap,
    WorldMap
}

[Serializable]
public struct CreatureTypeToCubeCoords
{
    public CubeCoords CubeCoords;
    public CreatureDefinitionId DefinitionId;
    public Team Team;
}

[Serializable]
public struct BattleMapBootstrapSettings
{
    public DeckId EnemyDeckId;
    public string MapJsonFilename;
    public List<CreatureTypeToCubeCoords> SpawnRequests;
}

[Serializable]
public struct WorldMapBootstrapSettings
{
    public string MapJsonFilename;
    public GameObject PlayerAvatarPrefab;
}

// allows us to startup the game in different ways/from different starting points (usually for debug purposes)
public class SceneBootstrap : MonoBehaviour
{
    [SerializeField] BootstrapSettings BootstrapSettings;
    [SerializeField] BattleMapBootstrapSettings BattleMapBootstrapSettings;
    [SerializeField] WorldMapBootstrapSettings WorldMapBootstrapSettings;
    [SerializeField] HexMapConfig HexMapConfig;

    bool m_battleStarted;
    bool m_firstFramePassed;

    void Start()
    {
        switch (BootstrapSettings)
        {
        case BootstrapSettings.BattleMap:
            break;
        case BootstrapSettings.WorldMap:
            var world = World.DefaultGameObjectInjectionWorld;
            var generationSystem = world.GetExistingSystem<HexMapGenerationSystem>();
            generationSystem.InitCoordToHexBuffer();
            generationSystem.Generate(WorldMapBootstrapSettings.MapJsonFilename, HexMapConfig.EmptyWorldHexPrefab);

            var startHexEntity = EntityQueryUtils.GetSingleEntityWithComponentLogFailure((WorldHexComponent hexComponent) =>
                { return hexComponent.Type == WorldHexType.Start; });
            if (startHexEntity == Entity.Null)
            {
                break;
            }

            var entityManager = world.EntityManager;
            float3 displacement = entityManager.GetComponentData<Translation>(startHexEntity).Value;
            Camera.main.transform.position += (Vector3) displacement;

            var translation = entityManager.GetComponentData<Translation>(startHexEntity);
            var scale = WorldMapBootstrapSettings.PlayerAvatarPrefab.transform.localScale;

            float offset = WorldMapBootstrapSettings.PlayerAvatarPrefab.GetComponentInChildren<MeshFilter>().sharedMesh.bounds.extents.y * scale.y;
            HybridEcsUtils.InstantiateHybrid(entityManager, WorldMapBootstrapSettings.PlayerAvatarPrefab, translation.Value + float3values.up * offset);

            world.GetExistingSystem<BattleHexNavigationSystem>().Init(EntityQueryUtils.GetSingleton<HexMapComponent>());
            world.GetExistingSystem<WorldMap3CSystem>().Enabled = true;
            WorldTurns.DoFirstTurnSetup();
            break;
        default:
            Debug.LogError("Invalid BootstrapSettings");
            break;
        }
    }

    void Update()
    {
        if (BootstrapSettings == BootstrapSettings.BattleMap && !m_battleStarted)
        {
            // need to wait one frame for the child buffers of entities converted from editor scene to be properly initialized
            if (m_firstFramePassed)
            {
                var world = World.DefaultGameObjectInjectionWorld;
                var entityManager = world.EntityManager;

                var generationSystem = world.GetExistingSystem<HexMapGenerationSystem>();
                generationSystem.InitCoordToHexBuffer();
                generationSystem.Generate(BattleMapBootstrapSettings.MapJsonFilename, HexMapConfig.EmptyBattleHexPrefab);

                BattleDeck.SetupDeckForBattle<PlayerBattleDataTag>(DeckId.Player, entityManager);
                BattleDeck.SetupDeckForBattle<EnemyBattleDataTag>(BattleMapBootstrapSettings.EnemyDeckId, entityManager);
                var battleTurnsSystem = world.GetExistingSystem<BattleTurnSystem>();
                battleTurnsSystem.DoFirstTurnSetup();

                world.GetExistingSystem<BattleHexNavigationSystem>().Init(EntityQueryUtils.GetSingleton<HexMapComponent>());
                world.GetExistingSystem<BattleTurnSystem>().Enabled = true;
                world.GetExistingSystem<Battle3CSystem>().Enabled = true;
                m_battleStarted = true;
            }

            m_firstFramePassed = true;

        }
    }
}
