﻿using Unity.Entities;

public static class WorldTurns
{
    public static void DoFirstTurnSetup()
    {
        var playerAvatar = EntityQueryUtils.GetSingletonEntity<PlayerAvatarTag>();
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        var actionPointsComponent = entityManager.GetComponentData<ActionPointsComponent>(playerAvatar);
        actionPointsComponent.FirstTimeSetup();
        entityManager.SetComponentData(playerAvatar, actionPointsComponent);
    }

    public static void EndTurn()
    {
        var playerAvatar = EntityQueryUtils.GetSingletonEntity<PlayerAvatarTag>();
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        var actionPointsComponent = entityManager.GetComponentData<ActionPointsComponent>(playerAvatar);
        actionPointsComponent.CurrentMovementPoints = actionPointsComponent.CurrentMaxMovementPoints;
        entityManager.SetComponentData(playerAvatar, actionPointsComponent);
    }
}