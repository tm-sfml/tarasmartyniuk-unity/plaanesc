﻿using UnityEngine;
using Unity.Entities;
using Unity.Transforms;

struct AreaMarkerComponent : IComponentData
{
    public float Radius;
}

/// <summary>
/// linked pair gameobject-entity used to mark an area(spheric), movable at runtime
/// entity uses unique tag to be found in ECS, gameobject is used for gizmos
/// </summary>
public class AreaMarkerAuthoring : MonoBehaviour, IConvertGameObjectToEntity
{
    [Range(0, float.MaxValue)]
    [SerializeField] float Radius;
    [SerializeField] Color Color = Color.white;

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        dstManager.AddComponent<CopyTransformFromGameObject>(entity);
        dstManager.AddComponentData(entity, new AreaMarkerComponent { Radius = Radius});
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color;
        Gizmos.DrawWireSphere(transform.position, Radius);    
    }
}
