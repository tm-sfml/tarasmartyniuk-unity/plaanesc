﻿using Unity.Entities;
using UnityEngine;
using System;

public struct HexHighlightRequestComponent : IComponentData
{
    public bool DehighlightAllRequested;

    public HexHighlightRequestComponent(HexHighlightRequestComponent other)
    {
        DehighlightAllRequested = other.DehighlightAllRequested;
    }
}

public struct HexesToHighlightBufferElement : IBufferElementData
{
    public Entity Hex;
    public Color HighlightColor;
}

public struct CurrentHighlightedHexesBufferElement : IBufferElementData, IEquatable<CurrentHighlightedHexesBufferElement>
{
    public Entity Hex;

    public bool Equals(CurrentHighlightedHexesBufferElement other) => Hex == other.Hex;
}

public struct HexesToDehighlightBufferElement : IBufferElementData
{
    public Entity Hex;
}


/// <summary>
/// handles:
/// - highlighting individual tiles in response to their IsHighlighted field being set
/// - dehighlight all requests
/// works only for objects with the same normal shader
/// if both dehighlight all and highlight is requested during same frame, 
/// first all current will be dehighlighted and then all the requested be highlighted
/// TODO: currentlyHighlightedHexes is not checking for duplicates - 
/// better to rewrite this to use separate component with {bool IsHighlighted} for each hex
/// </summary>
public class HexHighlightSystem : ComponentSystem
{
    const string kHighlightColorShaderProperty = "_HighlightColor";
    const string kIsHighlightedShaderProperty = "_IsHighlighted";

    public static Color GetHighlightColor(Entity entity, EntityManager entityManager)
    {
        var renderer = entityManager.GetComponentObject<MeshRenderer>(entity);
        return renderer.material.GetColor(kHighlightColorShaderProperty);
    }

    protected override void OnCreate()
    {
        base.OnCreate();
        var entity = EntityManager.CreateEntity(typeof(HexHighlightRequestComponent));
        EntityManager.AddBuffer<HexesToHighlightBufferElement>(entity);
        EntityManager.AddBuffer<CurrentHighlightedHexesBufferElement>(entity);
        EntityManager.AddBuffer<HexesToDehighlightBufferElement>(entity);
    }

    protected override void OnUpdate()
    {
        var query = GetEntityQuery(typeof(HexHighlightRequestComponent));
        var entity = query.GetFirstEntityWithComponentLogFailure<HexHighlightRequestComponent>();
        var highlightComponent = EntityManager.GetComponentData<HexHighlightRequestComponent>(entity);
        var currentlyHighlightedHexes = EntityManager.GetBuffer<CurrentHighlightedHexesBufferElement>(entity);
        var hexesToDehighlight = EntityManager.GetBuffer<HexesToDehighlightBufferElement>(entity);

        if (highlightComponent.DehighlightAllRequested)
        {
            foreach (var highlightedHex in currentlyHighlightedHexes)
            {
                Dehighlight(highlightedHex.Hex);
            }
            currentlyHighlightedHexes.Clear();
            highlightComponent.DehighlightAllRequested = false;
            EntityManager.SetComponentData(entity, highlightComponent);

            Debug.Log("Dehighlight");
        }
        else if (!hexesToDehighlight.IsEmpty)
        {
            foreach (var hex in hexesToDehighlight)
            {
                var index = currentlyHighlightedHexes.IndexOf(new CurrentHighlightedHexesBufferElement { Hex = hex.Hex });
                if (index == -1)
                {
                    Debug.Log($"dehighlighting hex that is not highlighted {hex.Hex}");
                    continue;
                }
                Dehighlight(hex.Hex);
                currentlyHighlightedHexes.RemoveAtSwapBack(index);
            }

            hexesToDehighlight.Clear();
        }

        var hexesToHighlight = EntityManager.GetBuffer<HexesToHighlightBufferElement>(entity);
        if (!hexesToHighlight.IsEmpty)
        {
            currentlyHighlightedHexes.EnsureCapacity(currentlyHighlightedHexes.Length + hexesToHighlight.Length);
            foreach (var request in hexesToHighlight)
            {
                var renderer = EntityManager.GetComponentObject<MeshRenderer>(request.Hex);

                renderer.material.SetFloat(kIsHighlightedShaderProperty, 1);
                renderer.material.SetColor(kHighlightColorShaderProperty, request.HighlightColor);

                if (!currentlyHighlightedHexes.Exists((CurrentHighlightedHexesBufferElement element) => element.Hex == request.Hex))
                {
                    currentlyHighlightedHexes.Add(new CurrentHighlightedHexesBufferElement { Hex = request.Hex });
                }
            }

            hexesToHighlight.Clear();
        }
    }

    void Dehighlight(Entity hex)
    {
        var renderer = EntityManager.GetComponentObject<MeshRenderer>(hex);
        renderer.material.SetFloat(kIsHighlightedShaderProperty, 0);
    }
}
