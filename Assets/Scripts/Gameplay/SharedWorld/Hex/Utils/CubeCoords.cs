﻿using System;

[Serializable]
public struct CubeCoords : IEquatable<CubeCoords>
{
    public static readonly CubeCoords kProbablyInvalidCoords = new CubeCoords(int.MaxValue, int.MaxValue);

    public int X;
    public int Z;
    // coordinate system constraint
    public int Y => -X - Z;


    public CubeCoords(int x, int z)
    {
        X = x;
        Z = z;
    }

    public override string ToString() => $"({X}, {Y}, {Z})";

    public bool Equals(CubeCoords other)
    {
        return this == other;
    }

    public override bool Equals(object obj)
    {
        return this == (CubeCoords) obj;
    }

    public static CubeCoords operator +(CubeCoords left, CubeCoords right)
    {
        return new CubeCoords(left.X + right.X, left.Z + right.Z);
    }

    public static CubeCoords operator -(CubeCoords left, CubeCoords right)
    {
        return new CubeCoords(left.X - right.X, left.Z - right.Z);
    }

    public static CubeCoords operator -(CubeCoords coords)
    {
        return new CubeCoords(-coords.X, -coords.Z);
    }

    public static bool operator ==(CubeCoords left, CubeCoords right)
    {
        return left.X == right.X && left.Z == right.Z;
    }

    public static bool operator !=(CubeCoords left, CubeCoords right)
    {
        return !(left == right);
    }
}
