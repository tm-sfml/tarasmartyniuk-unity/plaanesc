﻿using Unity.Mathematics;

/// <summary>
/// using cube coordinate system, flat-top hexes, odd columns shoved by +1/2 of row height
/// No offset for maps - HexMap origin must be at (0, 0)!
/// most of this is taken from https://www.redblobgames.com/grids/hexagons/
/// </summary>
public static class HexMath
{
    public static readonly float2x2 AxialBasis = new float2x2
    {
        c0 = new float2(3f / 2f, - math.sqrt(3) / 2f),
        c1 = new float2(0, - math.sqrt(3))
    };

    public static float HorizontalOffset(float radius)
    {
        return Width(radius) * 3 / 4;
    }

    public static float Width(float radius)
    {
        return 2 * radius;
    }

    public static float Height(float radius)
    {
        return math.sqrt(3) * radius;
    }

    public static int2 CubeToOffset(CubeCoords cube)
    {
        var col = cube.X;
        var row = cube.Z + (cube.X - (cube.X & 1)) / 2;

        // -row since our cube +Z is inverted with regards to what Amit uses
        return new int2(col, -row);
    }

    public static CubeCoords OffsetToCube(int2 offset)
    {
        var x = offset.x;
        // - y since our cube +Z is inverted with regards to what Amit uses
        var z = -offset.y - (offset.x - (offset.x & 1)) / 2;
        return new CubeCoords { X = x, Z = z };
    }

    /// <param name="point">on the hexmap z plane</param>
    public static CubeCoords PlanePointToHexCoord(float2 point, float hexRadius)
    {
        // x, z cube
        float2 fractCoords = math.mul(math.inverse(AxialBasis), point) / hexRadius;
        // fract coords are still subject to coord system constraint
        float fractCoordsY = -fractCoords.x - fractCoords.y;
        return CubeRound(new float3(fractCoords.x, fractCoordsY, fractCoords.y));
    }

    public static float2 GetHexCenter(CubeCoords coords, float hexRadius)
    {
        return math.mul(hexRadius * AxialBasis, new float2(coords.X, coords.Z));
    }

    /// <param name="fractCoords">Cube coords with float values</param>
    static CubeCoords CubeRound(float3 fractCoords)
    {
        float3 rounded = math.round(fractCoords);
        float3 diff = math.abs(rounded - fractCoords);

        if (diff.x > diff.y && diff.x > diff.z)
        {
            rounded.x = -rounded.y - rounded.z;
        }
        else if (diff.z > diff.y)
        {
            rounded.z = -rounded.x - rounded.y;
        }

        return new CubeCoords((int) rounded.x, (int) rounded.z);
    }

}