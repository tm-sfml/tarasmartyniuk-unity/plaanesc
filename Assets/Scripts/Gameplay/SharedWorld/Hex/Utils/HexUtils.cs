﻿using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Transforms;

public static class HexUtils
{
    public static Plane HexPlane => new Plane(inNormal: Vector3.up, d: 0f);

    public static CubeCoords GetProjectionHexCoords(float3 position, HexMapComponent hexMapComponent)
    {
        return HexMath.PlanePointToHexCoord(position.xz, hexMapComponent.HexRadius);
    }

    public static CubeCoords GetProjectionHexCoords(Entity entity, HexMapComponent hexMapComponent, EntityManager? entityManager = null)
    {
        if (!entityManager.HasValue)
        {
            entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        }
        var translation = entityManager.Value.GetComponentData<Translation>(entity);
        return HexMath.PlanePointToHexCoord(translation.Value.xz, hexMapComponent.HexRadius);
    }


    public static CubeCoords GetHexCoordAtScreenPoint(Vector3 screenPosition, HexMapComponent hexMapComponent)
    {
        float3 point = MathUtils.Project(Camera.main.ScreenPointToRay(screenPosition), HexPlane);
        return HexMath.PlanePointToHexCoord(point.xz, hexMapComponent.HexRadius);
    }

    public static Slice2D<HexEntityBufferElement> GetCoordToHexArray(Entity? hexOrigin = null, EntityManager? entityManager = null)
    {
        if (!entityManager.HasValue)
        {
            entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        }
        if (!hexOrigin.HasValue)
        {
            hexOrigin = EntityQueryUtils.GetFirstEntityWithComponentLogFailure<HexMapComponent>();
        }

        var hexBuffer = entityManager.Value.GetBuffer<HexEntityBufferElement>(hexOrigin.Value);

        var hexMap = entityManager.Value.GetComponentData<HexMapComponent>(hexOrigin.Value);
        return new Slice2D<HexEntityBufferElement>(hexMap.Dimensions.y, new NativeSlice<HexEntityBufferElement>(hexBuffer.AsNativeArray()));
    }

}
