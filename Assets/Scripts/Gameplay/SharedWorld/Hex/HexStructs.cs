﻿using Unity.Entities;

// buffer that is a map {offset coord: hex entity }
public struct HexEntityBufferElement : IBufferElementData
{
    public Entity Hex;
}