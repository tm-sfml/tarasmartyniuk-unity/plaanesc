﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
using Priority_Queue;
using System.Text;
using Unity.Collections;

public struct HexMoveGraphNode
{
    public CubeCoords Coords;
    public int StepCount;

    public HexMoveGraphNode(CubeCoords coords, int stepCount)
    {
        Coords = coords;
        StepCount = stepCount;
    }

    public void Deconstruct(out CubeCoords coords, out int stepCount)
    {
        coords = Coords;
        stepCount = StepCount;
    }
}

// its so bad to use heap classes for the nodes here, but PriorityQueue requires it(
public class HexMoveGraphPriorityNode : FastPriorityQueueNode
{
    public CubeCoords Coords;

    public HexMoveGraphPriorityNode(CubeCoords coords)
    {
        Coords = coords;
    }
}

public struct GetPathSettings
{
    public bool AllowAdjacentTiles;
    public HexMapType MapType;
}

public struct GetTilesInRangeSettings
{
    // each tile of result has a Continuous path from center
    public bool NeedsContinuousPath;
    public BattleHexNavigationSystem.HexFilter Filter;
    // invalid for Filter != TargetableHexes
    public Team TargetTeam;
    public HexMapType MapType;

    public static GetTilesInRangeSettings PassableHexes(HexMapType mapType) =>
        new GetTilesInRangeSettings
        {
            Filter = BattleHexNavigationSystem.HexFilter.PassableBattleHexes,
            NeedsContinuousPath = true,
            MapType = mapType
        };

    public static GetTilesInRangeSettings TargetableHexes(Team team) =>
        new GetTilesInRangeSettings
        {
            Filter = BattleHexNavigationSystem.HexFilter.TargetableHexes,
            NeedsContinuousPath = false,
            TargetTeam = team,
            MapType = HexMapType.Battle
        };
}

public enum HexMapType
{
    Invalid,
    World,
    Battle
}

public class BattleHexNavigationSystem : ComponentSystem
{
    public enum HexFilter
    {
        Invalid,
        // battle only
        TargetableHexes,
        PassableWorldHexes,
        PassableBattleHexes
    }

    public static readonly CubeCoords Top = new CubeCoords(0, -1);
    public static readonly CubeCoords TopRight = new CubeCoords(1, -1);
    public static readonly CubeCoords BottomRight = new CubeCoords(1, 0);
    public static readonly CubeCoords Bottom = -Top;
    public static readonly CubeCoords BottomLeft = -TopRight;
    public static readonly CubeCoords TopLeft = -BottomRight;

    public const int kNeighborsCount = 6;
    const int kToVisitCapacity = 40;

    // helpers
    List<CubeCoords> m_neighbors;
    Queue<HexMoveGraphNode> m_toVisit;

    FastPriorityQueue<HexMoveGraphPriorityNode> m_frontier;
    NativeArray2D<CubeCoords> m_cameFrom;
    NativeArray2D<int> m_costSoFar;

    // Changing the map invalidates all the helpers, so the system must be re-inited every time we generate new map
    public void Init(HexMapComponent hexMapComponent)
    {
        m_frontier = new FastPriorityQueue<HexMoveGraphPriorityNode>(hexMapComponent.Dimensions.x * hexMapComponent.Dimensions.y);

        if (m_cameFrom.FlatArray.IsCreated)
        {
            m_cameFrom.Dispose();
        }
        m_cameFrom = CreateGetPathHelperMap<CubeCoords>(hexMapComponent);

        if (m_costSoFar.FlatArray.IsCreated)
        {
            m_costSoFar.Dispose();
        }
        m_costSoFar = CreateGetPathHelperMap<int>(hexMapComponent);
    }

    // get all tiles in range from center, except those filtered out by hexFilter
    // if needsContinuousPath is true, each result tile has a Continuous path from center
    // BFS search
    public void GetTilesInRange(CubeCoords center, int range, GetTilesInRangeSettings settings,
        Slice2D<HexEntityBufferElement> hexMap,
        List<HexMoveGraphNode> outMoves)
    {
        if (!hexMap.IsValidIndex(HexMath.CubeToOffset(center)))
        {
            Debug.LogError("Invalid center");
            return;
        }
        var visited = outMoves;
        visited.Clear();
        m_toVisit.Clear();

        m_toVisit.Enqueue(new HexMoveGraphNode(center, 0));

        // every iter is O(N) for searching in visited + amortized O(N) for que/deque (array can shift)
        while (m_toVisit.Count != 0)
        {
            (CubeCoords current, int stepCount) = m_toVisit.Dequeue();

            int index = visited.FindIndex((HexMoveGraphNode node) => node.Coords == current);
            if (index != -1 && visited[index].StepCount <= stepCount)
            {
                continue;
            }

            var currentHex = hexMap[HexMath.CubeToOffset(current)].Hex;
            bool passesFilter = Filter(currentHex, settings);

            if (stepCount == range)
            {
                if (passesFilter)
                {
                    VisitCurrent();
                }
                continue;
            }

            if (current == center ||
                passesFilter || !settings.NeedsContinuousPath)
            {
                GetNeighbors(current, m_neighbors);
                foreach (CubeCoords neighbor in m_neighbors)
                {
                    int2 neighborOffset = HexMath.CubeToOffset(neighbor);
                    if (!hexMap.IsValidIndex(neighborOffset))
                    {
                        continue;
                    }

                    var neighborStepCount = stepCount + 1;
                    Debug.Assert(neighborStepCount <= range);

                    m_toVisit.Enqueue(new HexMoveGraphNode(neighbor, neighborStepCount));
                }
            }

            if (passesFilter)
            {
                VisitCurrent();
            }

            void VisitCurrent()
            {
                visited.Add(new HexMoveGraphNode(current, stepCount));
                WriteStepsToLabel(hexMap[HexMath.CubeToOffset(current)].Hex, EntityManager, stepCount);
            }
        }

        Debug.Assert(visited.Distinct().Count() == visited.Count);
    }

    // outMoves sorted from start to end
    // when allowAdjacentTiles is true, stops one tile away from end
    public bool GetPath(CubeCoords start, CubeCoords end,
        HexMapType mapType,
        Slice2D<HexEntityBufferElement> hexMap,
        List<CubeCoords> outMoves, 
        bool allowAdjacentTiles = false)
    {
        m_frontier.Clear();
#if DEBUG
        m_cameFrom.Fill(CubeCoords.kProbablyInvalidCoords);
#endif
        m_costSoFar.Fill(-1);

        m_frontier.Enqueue(new HexMoveGraphPriorityNode(start), 0);
        m_costSoFar[HexMath.CubeToOffset(start)] = 0;

        CubeCoords? pathEnd = null;

        while (m_frontier.Count != 0)
        {
            CubeCoords current = m_frontier.Dequeue().Coords;

            if (current == end)
            {
                pathEnd = end;
                break;
            }

            GetNeighbors(current, m_neighbors);
            foreach (var next in m_neighbors)
            {
                if (allowAdjacentTiles && next == end)
                {
                    pathEnd = current;
                    break;
                }

                int2 nextOffset = HexMath.CubeToOffset(next);

                if (!hexMap.IsValidIndex(nextOffset))
                {
                    continue;
                }
                var nextEntity = hexMap[nextOffset].Hex;

                if (!IsPassable(nextEntity, mapType))
                {
                    continue;
                }

                int new_cost = m_costSoFar[HexMath.CubeToOffset(current)] + 1;
                if (m_costSoFar[nextOffset] == -1 || new_cost < m_costSoFar[nextOffset])
                {
                    m_costSoFar[nextOffset] = new_cost;
                    int priority = new_cost; // + heuristic(goal, next) TODO: add heuristic (prio = dist to end + dist to start)
                    m_frontier.Enqueue(new HexMoveGraphPriorityNode(next), priority);
                    m_cameFrom[nextOffset] = current;
                }
            }
        }

        if (!pathEnd.HasValue)
        {
            return false;
        }

        // prepare result
        CubeCoords currentMove = pathEnd.Value;
        outMoves.Clear();
        int2 o = HexMath.CubeToOffset(pathEnd.Value);
        int pathLength = m_costSoFar[o];
        outMoves.Capacity = pathLength;
        while (true)
        {
            outMoves.Add(currentMove);
            if (currentMove == start)
            {
                break;
            }
            currentMove = m_cameFrom[HexMath.CubeToOffset(currentMove)];
        }

        outMoves.Reverse();
        return true;
    }

    public static void GetNeighbors(CubeCoords center, List<CubeCoords> outNeighbors)
    {
        outNeighbors.Clear();
        outNeighbors.Capacity = kNeighborsCount;
        outNeighbors.Add(center + Top);
        outNeighbors.Add(center + TopRight);
        outNeighbors.Add(center + BottomRight);
        outNeighbors.Add(center + Bottom);
        outNeighbors.Add(center + BottomLeft);
        outNeighbors.Add(center + TopLeft);
    }

    protected override void OnCreate()
    {
        base.OnCreate();

        m_neighbors = new List<CubeCoords>(kNeighborsCount);
        m_toVisit = new Queue<HexMoveGraphNode>(kToVisitCapacity);
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        m_costSoFar.Dispose();
        m_cameFrom.Dispose();
    }

    // map {coord: data}
    static NativeArray2D<T> CreateGetPathHelperMap<T>(HexMapComponent hexMapComponent) where T : struct
    {
        var size = hexMapComponent.Dimensions.x * hexMapComponent.Dimensions.y;
        var flatArray = new NativeArray<T>(size, Allocator.Persistent);
        return new NativeArray2D<T>(hexMapComponent.Dimensions.y, flatArray);
    }

    bool IsPassable(Entity hex, HexMapType mapType)
    {
        switch (mapType)
        {
        case HexMapType.World:
        {
            var hexComponent = EntityManager.GetComponentData<WorldHexComponent>(hex);
            return hexComponent.IsPassable;
        }
        case HexMapType.Battle:
        {
            var hexComponent = EntityManager.GetComponentData<BattleHexComponent>(hex);
            return hexComponent.IsPassable && hexComponent.OccupyingCreature == Entity.Null;
        }
        default:
            Debug.LogError("invalid map type");
            return false;
        }
    }

    bool Filter(Entity hex, GetTilesInRangeSettings settings)
    {
        bool result = false;
        switch (settings.Filter)
        {
        case HexFilter.TargetableHexes:
            var hexComponent = EntityManager.GetComponentData<BattleHexComponent>(hex);
            if (hexComponent.Type == BattleHexType.TeamHealth)
            {
                var teamHealth = EntityManager.GetComponentData<TeamHealthComponent>(hex);
                result = settings.TargetTeam == Team.Invalid || teamHealth.Team == settings.TargetTeam;
            }
            else if (hexComponent.OccupyingCreature != Entity.Null)
            {
                var creature = EntityManager.GetComponentData<CreatureComponent>(hexComponent.OccupyingCreature);
                result = settings.TargetTeam == Team.Invalid || creature.Team == settings.TargetTeam;
            }
            else
            {
                result = hexComponent.IsPassable;
            }
            break;
        case HexFilter.PassableBattleHexes:
            result = IsPassable(hex, settings.MapType);
            break;
        default:
            Debug.LogError("invalid filter");
            break;
        }

        return result;
    }

    [System.Diagnostics.Conditional("DEBUG")]
    static void WriteStepsToLabel(Entity hex, EntityManager EntityManager, int stepCount)
    {
        var transform = EntityManager.GetComponentObject<Transform>(hex);
        if (!transform)
        {
            return;
        }
        var text = transform.GetComponentInChildren<TextMeshProUGUI>();
        if (!text)
        {
            return;
        }

        int index = text.text.IndexOf("Steps: ");
        if (index == -1)
        {
            text.text += $"\nSteps: {stepCount}";
        }
        else
        {
            StringBuilder sb = new StringBuilder(text.text);
            var stepCountStr = stepCount.ToString();
            int start = index + 1;
            int end = start + stepCountStr.Length;
            if (sb.Length <= end)
            {
                Debug.LogError("invalid label size");
                return;
            }
            sb.Insert(start, stepCountStr);
        }
    }

    protected override void OnUpdate() { }
}
