﻿using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class HexMapGenerationSystem : ComponentSystem
{
    /// destroy all existing hexes, and generate a new map from filename
    /// we have different emptyHexPrefabs for battle and world, but they both need to map to 0 index, since this is what tiled uses for empty
    public void Generate(string jsonMapFilename, GameObject emptyHexPrefab)
    {
        var hexOrigin = GetSingletonEntity<HexMapComponent>();
        var hexMapRootGameObject = EntityManager.GetLinkedGameObject(hexOrigin);

        HierarchyUtils.DestroyChildren(hexMapRootGameObject, true);

        EntityCommandBuffer entityCommandBuffer = World.GetOrCreateSystem<EntityCommandBufferSystem>().CreateCommandBuffer();
        var oldCoordToHex = HexUtils.GetCoordToHexArray(hexOrigin, EntityManager);
        for (int i = 0; i < oldCoordToHex.FlatSlice.Length; i++)
        {
            if (EntityManager.Exists(oldCoordToHex.FlatSlice[i].Hex))
            {
                entityCommandBuffer.DestroyEntity(oldCoordToHex.FlatSlice[i].Hex);
            }
        }

        var config = hexMapRootGameObject.GetComponent<HexMapConfig>();

        var hexMapComponent = EntityManager.GetComponentData<HexMapComponent>(hexOrigin);
        int[] map;
        (map, hexMapComponent.Dimensions) = HexGenerationUtils.ParseTilemap(jsonMapFilename);
        EntityManager.SetComponentData(hexOrigin, hexMapComponent);

        var spawnedEntities = new List<(int2, Entity)>(map.Length);

        using var blobAssetStore = new BlobAssetStore();
        for (int i = 0; i < map.Length; i++)
        {
            int tiledId = map[i];
            if (!config.HexPrefabs.IsValidIndex(tiledId))
            {
                Debug.LogError($"No hex prefab for tiled tile id: {tiledId}");
                continue;
            }

            int2 coords = HexGenerationUtils.JsonTilemapIndexToOffsetCoords(i, hexMapComponent.Dimensions);
            Debug.Assert(coords.x >= 0 && coords.y >= 0);
            Debug.Assert(HexMath.CubeToOffset(HexMath.OffsetToCube(coords)).Equals(coords));

            GameObject prefab = tiledId == 0?
                emptyHexPrefab : config.HexPrefabs[tiledId];
            GameObject hexGameObject = Object.Instantiate(prefab, hexMapRootGameObject.transform);
            hexGameObject.transform.localPosition = HexGenerationUtils.GetGridOffset(coords, hexMapComponent);

            var hex = GameObjectConversionUtility.ConvertGameObjectHierarchy(hexGameObject, GameObjectConversionSettings.FromWorld(EntityManager.World, blobAssetStore));
            EntityManager.InjectOriginalComponentsObjects(hex, hexGameObject.transform);
            spawnedEntities.Add((coords, hex));

            if (EntityManager.HasComponent<WorldHexComponent>(hex))
            {
                var worldHexComponent = EntityManager.GetComponentData<WorldHexComponent>(hex);
                if (worldHexComponent.Type == WorldHexType.DilemmaEncounter)
                {
                    var dilemmaComponent = EntityManager.GetComponentData<DilemmaComponent>(hex);
                    var dilemmaDefinitions = config.GetComponent<DilemmaDefinitions>();
                    var definitionPrefab = dilemmaDefinitions.GetDefinition(dilemmaComponent.DilemmaEncounterId);

                    EntityManager.SetComponentData(hex, new DilemmaComponent(
                        EntityManager.GetComponentData<DilemmaComponent>(definitionPrefab))
                    {
                        DilemmaEncounterId = dilemmaComponent.DilemmaEncounterId
                    });

                    var optionToEffectsBuffer = EntityManager.GetBuffer<DilemmaEncounterEffectBufferElement>(hex);
                    var optionToEffectsBufferDef = EntityManager.GetBuffer<DilemmaEncounterEffectBufferElement>(definitionPrefab);
                    optionToEffectsBuffer.CopyFrom(optionToEffectsBufferDef);
                }
            }

            var renderMesh = hexGameObject.GetComponent<MeshFilter>();
            if (config.LabelSettings != LabelSettings.None)
            {
                Bounds hexBounds = renderMesh.sharedMesh.bounds;

                Vector3 labelOffset = Vector3.up * (hexBounds.size.y + .1f);
                var label = HexGenerationUtils.InstantiateCoordsLabel(hexGameObject, config.LabelPrefab, coords, config.LabelSettings);
                label.transform.localPosition = labelOffset;
            }
        }
        var hexBuffer = EntityManager.GetBuffer<HexEntityBufferElement>(hexOrigin);
        hexBuffer.ResizeUninitialized(hexMapComponent.Dimensions.x * hexMapComponent.Dimensions.y);
        var coordToHex = new Slice2D<HexEntityBufferElement>(hexMapComponent.Dimensions.y, new NativeSlice<HexEntityBufferElement>(hexBuffer.AsNativeArray()));

        foreach ((int2 coords, Entity hex) in spawnedEntities)
        {
            coordToHex[coords] = new HexEntityBufferElement { Hex = hex };
        }

        Debug.Assert(hexMapRootGameObject.transform.childCount == map.Length);
    }

    // calling init explicitly because this system is used from non-ecs right at the start
    public void InitCoordToHexBuffer()
    {
        var hexOrigin = GetSingletonEntity<HexMapComponent>();
        EntityManager.AddBuffer<HexEntityBufferElement>(hexOrigin);
    }

    protected override void OnUpdate()
    {
        var hexOrigin = GetSingletonEntity<HexMapComponent>();
        HexGenerationUtils.DrawCubeAxesVisualization(EntityManager.GetComponentData<Translation>(hexOrigin).Value + float3values.up * .1f);
    }
}