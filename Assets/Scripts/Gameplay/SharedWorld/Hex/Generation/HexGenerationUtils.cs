﻿using Newtonsoft.Json;
using System.IO;
using System.Text;
using TMPro;
using Unity.Mathematics;
using UnityEngine;

// shared impl for GameObject and ECS hex generation
public static class HexGenerationUtils
{
    public static float2x3 CubeAxes = new float2x3
    {
        c0 = new float2(1, 0),
        c1 = new float2(-math.sqrt(3) / 2f, 3f / 2f),
        c2 = new float2(-math.sqrt(3) / 2f, -3f / 2f)
    };

    public const int kEmptyTileIndex = 0;

    public static (int[], int2) ParseTilemap(string mapFilename)
    {
        string tilemapFilename = Path.Combine(Application.streamingAssetsPath, mapFilename);

        string mapString = File.ReadAllText(tilemapFilename);
        dynamic mapJson = JsonConvert.DeserializeObject(mapString);
        dynamic layer = mapJson["layers"][0];
        int2 dimentions = new int2(layer["width"].ToObject<int>(), layer["height"].ToObject<int>());

        int[] map = layer["data"].ToObject<int[]>();
        return (map, dimentions);
    }

    public static GameObject InstantiateCoordsLabel(GameObject parent, GameObject labelPrefab, int2 offsetCoords, LabelSettings settings)
    {
        // optim: pool
        Quaternion lookDownRotation = Quaternion.LookRotation(Vector3.down, Vector3.forward);
        var label = Object.Instantiate(labelPrefab, Vector3.zero, lookDownRotation, parent.transform);

        var text = label.GetComponentInChildren<TextMeshProUGUI>();
        var sb = new StringBuilder();
        if (settings == LabelSettings.Cube || settings == LabelSettings.Both)
        {
            CubeCoords cubeCoords = HexMath.OffsetToCube(offsetCoords);
            sb.Append($"{cubeCoords}");
        }
        if (settings == LabelSettings.Offset || settings == LabelSettings.Both)
        {
            if (settings == LabelSettings.Both)
            {
                sb.Append("\n");
            }
            sb.Append($"offset: ({offsetCoords.x},{offsetCoords.y})");
        }

        text.text = sb.ToString();

        return label;
    }

    public static float3 GetGridOffset(int2 offsetCoords, HexMapComponent config)
    {
        float height = HexMath.Height(config.HexRadius);

        Vector3 localOffset = Vector3.right * offsetCoords.x * (HexMath.HorizontalOffset(config.HexRadius)) +
            Vector3.forward * offsetCoords.y * (height);

        Vector3 verticalStaggerOffset = new float3();
        if (offsetCoords.x % 2 != 0)
        {
            verticalStaggerOffset = Vector3.back * height / 2f;
        }

        return localOffset + verticalStaggerOffset;
    }

    public static int2 JsonTilemapIndexToOffsetCoords(int index, int2 dimentions)
    {
        return new int2(index % dimentions.x,
            dimentions.y - (index / dimentions.x) - 1);
    }

    public static void DrawCubeAxesVisualization(float3 position)
    {
        void DrawAxis(float2 axis2D, Color color)
        {
            const float arrowSize = .15f;

            float3 axis = axis2D.to3D();
            Debug.DrawLine(position - axis, position + axis, color);
            DebugX.DrawPoint(position + axis, color, arrowSize);
        }

        DrawAxis(CubeAxes.c0, Color.green);
        DrawAxis(CubeAxes.c1, Color.magenta);
        DrawAxis(CubeAxes.c2, Color.cyan);
    }
}
