﻿using System.Collections.Generic;
using UnityEngine;

public enum LabelSettings
{
    None, Cube, Offset, Both
}

/// <summary>
/// stored in monobehavior, since most fields cannot be put into dots component 
/// and there is no need to have both game object and DOTS configs
/// </summary>
public class HexMapConfig : MonoBehaviour
{
    public string JsonMapFilename;
    public GameObject LabelPrefab;
    public List<GameObject> HexPrefabs;
    public GameObject EmptyWorldHexPrefab;
    public GameObject EmptyBattleHexPrefab;

    public LabelSettings LabelSettings;
}
