﻿using System;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

[Serializable]
[GenerateAuthoringComponent]
public struct HexMapComponent : IComponentData
{
    public float HexRadius;
    // per-area data
    [HideInInspector] public int2 Dimensions;
}
