﻿using UnityEngine;
using Unity.Mathematics;

public class WorldBounds : MonoBehaviour
{
    [SerializeField] float2 Bounds;
    [SerializeField] float WallHeight = 10;
    // 4 elements
    GameObject[] m_walls;

    const float WallColliderDepth = 0;

    void Reset()
    {
        m_walls = new GameObject[] { SpawnWall(), SpawnWall(), SpawnWall(), SpawnWall() };
    }

    void OnValidate()
    {
        if (Bounds.x == 0 || Bounds.y == 0)
        {
            return;
        }

        if (gameObject.transform.childCount != 4)
        {
            Debug.LogError("world bounds must have 4 children - have you called Reset?");
            return;
        }

        void updateAlongWidth(int childIndex, Vector3 direction)
        {
            UpdateWall(gameObject.transform.GetChild(childIndex).gameObject, direction * Bounds.x / 2, Bounds.y);
        }
        updateAlongWidth(0, Vector3.right);
        updateAlongWidth(1, Vector3.left);

        void updateAlongHeight(int childIndex, Vector3 direction)
        {
            UpdateWall(gameObject.transform.GetChild(childIndex).gameObject, direction * Bounds.y / 2, Bounds.x);
        }
        updateAlongHeight(2, Vector3.forward);
        updateAlongHeight(3, Vector3.back);
    }

    GameObject SpawnWall()
    {
        var wall = new GameObject("BoundsWall", typeof(BoxCollider));
        wall.transform.SetParent(gameObject.transform, worldPositionStays: false); ;
        return wall;
    }

    void UpdateWall(GameObject wall, Vector3 offset, float colliderWidth)
    {
        wall.transform.localPosition = offset;
        wall.transform.rotation = Quaternion.LookRotation(wall.transform.position - gameObject.transform.position, Vector3.up);
        var collider = wall.GetComponent<BoxCollider>();
        collider.size = new Vector3(colliderWidth, WallHeight, WallColliderDepth);
    }
}
