﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct CreatureMovementComponent : IComponentData
{
    public bool IsCreatureMoveInProgress;
    public float SingleHexMoveTime;
}
