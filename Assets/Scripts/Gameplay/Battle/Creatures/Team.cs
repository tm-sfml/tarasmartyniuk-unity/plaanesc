﻿public enum Team
{
    Invalid, Player, Enemy, Count
}

public static class TeamUtils
{
    public static bool IsEnemyOf(this Team team, Team other)
    {
        return other == team.Enemy();
    }

    public static Team Enemy(this Team team)
    {
        return team switch
        {
            Team.Player => Team.Enemy,
            Team.Enemy => Team.Player,
            _ => Team.Invalid
        };
    }
}

