﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using System.IO;
using Newtonsoft.Json.Linq;

// setups editor-defined creature definition prefabs with values read from json config,
// converts them to entities on start and stores the EntityGameObjectPairs
public class CreatureDefinitions : MonobehaviorSingletonBase<CreatureDefinitions>
{
    // prefab index must correspond to its CreatureId
    [SerializeField] List<GameObject> CreaturePrefabs;
    [SerializeField] string JsonFilename;
    List<Entity> m_creatureEntityPrefabs = new List<Entity>();

    public EntityGameObjectPair GetCreaturePrefab(CreatureDefinitionId creatureId)
    {
        int index = EnumUtils.ToInt(creatureId);

        if (!m_creatureEntityPrefabs.IsValidIndex(index))
        {
            Debug.LogError($"creature entity not found for type: {creatureId}");
            return default;
        }

        if (!CreaturePrefabs.IsValidIndex(index))
        {
            Debug.LogError($"creature gameObject not found for type: {creatureId}");
            return default;
        }

        var result = new EntityGameObjectPair
        {
            Entity = m_creatureEntityPrefabs[index],
            GameObject = CreaturePrefabs[index]
        };

        return result;
    }

    void Start()
    {
        string tilemapFilename = Path.Combine(Application.streamingAssetsPath, JsonFilename);
        JArray definitions = JArray.Parse(File.ReadAllText(tilemapFilename));

        m_creatureEntityPrefabs.Capacity = CreaturePrefabs.Count;
        using var blobAssetStore = new BlobAssetStore();
        var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, blobAssetStore);

        Debug.Assert(CreaturePrefabs.Count == definitions.Count);

        for (int i = 0; i < definitions.Count; i++)
        {
            Entity entityDefPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(CreaturePrefabs[i], settings);
            m_creatureEntityPrefabs.Add(entityDefPrefab);

            var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            entityManager.SetComponentData(entityDefPrefab, new CreatureComponent
            {
                Attack = definitions[i]["Attack"].Value<int>(),
                Health = definitions[i]["Health"].Value<int>(),
            });
            entityManager.SetComponentData(entityDefPrefab, new ActionPointsComponent
            {
                OriginalMaxMovementPoints = definitions[i]["Movement"].Value<int>()
            });
            entityManager.SetComponentData(entityDefPrefab, new AttackAbilityComponent
            {
                Range = definitions[i]["Range"].Value<int>()
            });

            bool success = Enum.TryParse(definitions[i].Value<string>("DefinitionId"), out CreatureDefinitionId creatureId);
            Debug.Assert(success && EnumUtils.ToInt(creatureId) == i, "creature definition list index must be equal to its ToInt(\"DefinitionId\") value");
        }
    }
}
