﻿using Unity.Entities;
using UnityEngine;

[GenerateAuthoringComponent]
public struct CreatureComponent : IComponentData
{
    public Team Team;
    public int Attack;
    public int Health;

    public CreatureComponent(CreatureComponent other)
    {
        Team = other.Team;
        Attack = other.Attack;
        Health = other.Health;
    }
}


