﻿
using Unity.Entities;
using Unity.Mathematics;

// all action points and stats modifying battle actions logic: move and attack
public static class CreatureBattleActions
{
    public static void OnMove(CubeCoords from, CubeCoords to, Entity creature,
        EntityManager entityManager, Slice2D<HexEntityBufferElement> coordToHex, int pathLength)
    {
        // get old hex
        var oldHex = coordToHex[HexMath.CubeToOffset(from)].Hex;

        entityManager.SetComponentData(oldHex,
            new BattleHexComponent(entityManager.GetComponentData<BattleHexComponent>(oldHex))
            {
                OccupyingCreature = Entity.Null
            });

        var newHex = coordToHex[HexMath.CubeToOffset(to)].Hex;
        entityManager.SetComponentData(newHex,
            new BattleHexComponent(entityManager.GetComponentData<BattleHexComponent>(newHex))
            {
                OccupyingCreature = creature
            });

        var actionPointsComponent = entityManager.GetComponentData<ActionPointsComponent>(creature);
        actionPointsComponent.CurrentMovementPoints = math.max(0, actionPointsComponent.CurrentMovementPoints - pathLength);
        entityManager.SetComponentData(creature, actionPointsComponent);
    }
}
