﻿using System;
using Unity.Entities;

[Serializable]
public struct AbilityBufferElement : IBufferElementData
{
    public AbilityType Id;
    public int Cooldown;
    public int CooldownLeft;
}

