﻿using System.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public static class CreatureAttackAbility
{
    public static void StartAttackAbility(Entity initiatingCreature, Entity target, EntityManager entityManager)
    {
        // make initiator not interactable
        CoroutineStarter.Instance.StartCoroutine(AttackAbilityCoroutine(initiatingCreature, target, entityManager));
    }

    // target can be either creature or team health hex
    static IEnumerator AttackAbilityCoroutine(Entity initiatingCreature, Entity targetHex, EntityManager entityManager)
    {
        entityManager.SetComponentData(initiatingCreature, new IsInteractableComponent { Value = false });

        var attackAbilityComponent = entityManager.GetComponentData<AttackAbilityComponent>(initiatingCreature);
        var battleHexComponent = entityManager.GetComponentData<BattleHexComponent>(targetHex);
        Entity projectileTarget = battleHexComponent.Type == BattleHexType.TeamHealth ?
            targetHex : battleHexComponent.OccupyingCreature;
        float3 end = entityManager.GetComponentData<Translation>(projectileTarget).Value;

        var startTime = Time.realtimeSinceStartup;
        float3 start = entityManager.GetComponentData<Translation>(initiatingCreature).Value;

        var projectile = Object.Instantiate(AbilitiesGameObjectConfig.Instance.AttackProjectile, start, Quaternion.identity);

        while (true)
        {
            float timeFraction = (Time.realtimeSinceStartup - startTime) / 
                DebugAnimSpeedupComponent.GetDebugSpeed(attackAbilityComponent.ProjectileFlightDuration);
            if (timeFraction >= 1)
            {
                break;
            }

            Vector3 currentPos = Vector3.Lerp(start, end, timeFraction);

            projectile.transform.position = currentPos;
            yield return new WaitForEndOfFrame();
        }

        Object.Destroy(projectile);
        var creatureComponent = entityManager.GetComponentData<CreatureComponent>(initiatingCreature);
        Damage.DoDamageHandleKill(targetHex, creatureComponent.Attack, entityManager);

        entityManager.SetComponentData(initiatingCreature, new IsInteractableComponent { Value = true });
    }
}