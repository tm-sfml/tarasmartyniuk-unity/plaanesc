﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct AttackAbilityComponent : IComponentData
{
    public int Range;

    public float ProjectileFlightDuration;
}
