﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct IsInteractableComponent : IComponentData
{
    public bool Value;
}
