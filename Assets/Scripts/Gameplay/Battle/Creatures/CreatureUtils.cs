﻿using System;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

static class CreatureUtils
{
    public static void DestroyCreature(Entity creature, EntityManager entityManager,
        Slice2D<HexEntityBufferElement> coordToHex, HexMapComponent hexMapComponent)
    {
        int2 creatureCoords = HexMath.CubeToOffset(HexUtils.GetProjectionHexCoords(
            entityManager.GetComponentData<Translation>(creature).Value, hexMapComponent));
        var creatureHex = coordToHex[creatureCoords].Hex;


        entityManager.SetComponentData(creatureHex,
            new BattleHexComponent(entityManager.GetComponentData<BattleHexComponent>(creatureHex))
            {
                OccupyingCreature = Entity.Null
            });

        entityManager.DestroyHybrid(creature);
    }

    public static void SpawnCreature(Entity hex, Entity cardDefEntity, Team team)
    {
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        var creatureCardDef = entityManager.GetComponentData<CreatureCardDefinitionComponent>(cardDefEntity);

        EntityGameObjectPair prefab = CreatureDefinitions.Instance.GetCreaturePrefab(creatureCardDef.CreatureId);
        var translation = entityManager.GetComponentData<Translation>(hex);
        float offset = prefab.GameObject.GetComponentInChildren<MeshFilter>().sharedMesh.bounds.extents.y;
        EntityGameObjectPair spawnedHybrid = entityManager.InstantiateHybrid(prefab, translation.Value + float3values.up * offset);

        entityManager.SetComponentData(spawnedHybrid.Entity,
            new CreatureComponent(entityManager.GetComponentData<CreatureComponent>(spawnedHybrid.Entity))
            {
                Team = team
            });

        var actionPoints = entityManager.GetComponentData<ActionPointsComponent>(spawnedHybrid.Entity);
        actionPoints.FirstTimeSetup();
        entityManager.SetComponentData(spawnedHybrid.Entity, actionPoints);

        entityManager.SetComponentData(hex,
            new BattleHexComponent(entityManager.GetComponentData<BattleHexComponent>(hex))
            {
                OccupyingCreature = spawnedHybrid.Entity
            });
    }
}
