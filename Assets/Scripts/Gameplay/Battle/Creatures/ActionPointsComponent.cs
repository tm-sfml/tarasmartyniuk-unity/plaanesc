﻿using Unity.Entities;
using UnityEngine;

[GenerateAuthoringComponent]
public struct ActionPointsComponent : IComponentData
{
    public int OriginalMaxMovementPoints;
    [HideInInspector] public int CurrentMovementPoints;
    [HideInInspector] public int CurrentMaxMovementPoints;

    public ActionPointsComponent(ActionPointsComponent other) => this = other;
    public void FirstTimeSetup()
    {
        CurrentMovementPoints = OriginalMaxMovementPoints;
        CurrentMaxMovementPoints = OriginalMaxMovementPoints;
    }
}
