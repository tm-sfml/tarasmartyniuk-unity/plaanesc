﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public static class CreatureMovement
{
    public static IEnumerator MoveAlongPathCoroutine(Entity selectedCreature, List<CubeCoords> path,
        Action onMoveEnd,
        HexMapComponent hexMapComponent, EntityManager entityManager)
    {
        Entity movementEntity = EntityQueryUtils.GetSingletonEntity<CreatureMovementComponent>();
        var movementComponent = entityManager.GetComponentData<CreatureMovementComponent>(movementEntity);

        if (movementComponent.IsCreatureMoveInProgress)
        {
            Debug.LogError("launching MoveAlongPathCoroutine when m_creatureMoveInProgress is already true");
            yield break;
        }

        if (path.Count < 2)
        {
            Debug.Log("path must have at least 2 hexes for movement");
            yield break;
        }
        float currentLerpStartTime = Time.realtimeSinceStartup;
        int currentStartPosIndex = 0;

        movementComponent.IsCreatureMoveInProgress = true;
        entityManager.SetComponentData(movementEntity, movementComponent);

        while (true)
        {
            float timeFraction = (Time.realtimeSinceStartup - currentLerpStartTime) / 
                DebugAnimSpeedupComponent.GetDebugSpeed(movementComponent.SingleHexMoveTime);
            if (timeFraction >= 1)
            {
                currentStartPosIndex++;

                if (currentStartPosIndex + 1 >= path.Count)
                {
                    break;
                }

                currentLerpStartTime = Time.realtimeSinceStartup;
                timeFraction -= 1;
            }

            float2 start = HexMath.GetHexCenter(path[currentStartPosIndex], hexMapComponent.HexRadius);
            float2 end = HexMath.GetHexCenter(path[currentStartPosIndex + 1], hexMapComponent.HexRadius);

            float3 currentPos = Vector2.Lerp(start, end, timeFraction).To3D();
            currentPos.y = entityManager.GetComponentData<Translation>(selectedCreature).Value.y;
            entityManager.SetComponentData(selectedCreature, new Translation { Value = currentPos });

            yield return new WaitForEndOfFrame();
        }

        movementComponent.IsCreatureMoveInProgress = false;
        entityManager.SetComponentData(movementEntity, movementComponent);

        onMoveEnd?.Invoke();
    }

}
