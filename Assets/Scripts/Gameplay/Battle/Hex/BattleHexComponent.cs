﻿using Unity.Entities;
using UnityEngine;

[GenerateAuthoringComponent]
public struct BattleHexComponent : IComponentData
{
    public bool IsPassable;
    public BattleHexType Type;
    [HideInInspector] public Entity OccupyingCreature;

    public BattleHexComponent(BattleHexComponent other) => this = other;
}
