﻿using Unity.Entities;
using UnityEngine;

public class BattleTurnSystem : ComponentSystem
{
    public void DoFirstTurnSetup()
    {
        Entities.ForEach((ref ActionPointsComponent actionPointsComponent) => actionPointsComponent.FirstTimeSetup());

        var turnComponent = GetSingleton<BattleTurnComponent>();
        turnComponent.CurrentTurn = 1;
        turnComponent.CurrentActingTeam = Team.Player;
        SetSingleton(turnComponent);

        NewTurnReset(Team.Player);
    }

    protected override void OnUpdate()
    {
        var turnComponent = GetSingleton<BattleTurnComponent>();

        if (turnComponent.EndTurnRequested)
        {
            turnComponent.CurrentActingTeam = turnComponent.CurrentActingTeam.Enemy();
            Debug.Assert(turnComponent.CurrentActingTeam != Team.Invalid);

            NewTurnReset(turnComponent.CurrentActingTeam);
            turnComponent.CurrentTurn++;
            turnComponent.EndTurnRequested = false;
        }

        SetSingleton(turnComponent);
    }

    void NewTurnReset(Team currentActingTeam)
    {
        Entities.ForEach((ref ActionPointsComponent actionPointsComponent) =>
        {
            actionPointsComponent.CurrentMovementPoints = actionPointsComponent.CurrentMaxMovementPoints;
        });

        var battleDataEntity = BattleDataUtils.GetBattleDataEntityForTeam(currentActingTeam);
        ResetManaToMax(battleDataEntity);
        var battleDeckEntity = EntityManager.FindChildWithComponent<BattleDeckConfigComponent>(battleDataEntity);
        BattleDeck.CycleHand(battleDeckEntity, EntityManager);
    }

    void ResetManaToMax(Entity battleDataEntity) 
    {
        var manaEntity = EntityManager.FindChildWithComponent<ManaComponent>(battleDataEntity);
        Debug.Assert(manaEntity != Entity.Null);
        var manaComponent = EntityManager.GetComponentData<ManaComponent>(manaEntity);
        manaComponent.CurrentMana = manaComponent.CurrentMaxMana;
        EntityManager.SetComponentData(manaEntity, manaComponent);
    }
}