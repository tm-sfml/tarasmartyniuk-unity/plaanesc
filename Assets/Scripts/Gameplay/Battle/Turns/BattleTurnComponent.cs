﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Unity.Entities;

[GenerateAuthoringComponent]
public struct BattleTurnComponent : IComponentData
{
    // ends current actor turn (player or enemy's) and starts other actor's turn
    public bool EndTurnRequested;

    [JsonConverter(typeof(StringEnumConverter))]
    public Team CurrentActingTeam;
    public int CurrentTurn;

    public BattleTurnComponent(BattleTurnComponent other) => this = other;
}
