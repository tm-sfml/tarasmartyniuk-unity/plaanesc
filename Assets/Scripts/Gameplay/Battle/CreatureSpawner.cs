﻿//using UnityEngine;
//using System.Collections.Generic;
//using Unity.Mathematics;
//using System;
//using Unity.Entities;


//// spawns creatures on hexes in editor for testing
//// in the future will handle spawning creatures at the start battle - from some config? tilemaps again?
//public class CreatureSpawner : MonoBehaviour
//{
//    [SerializeField] HexGenerator HexGenerator;

//    [SerializeField] [HideInInspector] List<GameObject> m_spawnedCreatures = new List<GameObject>() { null, null};
//    bool m_hasSpawnedCreatureEntities;


//    // TODO: store instantiated GameObjects and delete them here before spawning new
//    public void SpawnEnemiesEditor()
//    {
//        if (HexGenerator == null)
//            Debug.Log("HexGenerator is null");
//            return;
//        }
//        if (m_spawnedCreatures == null)
//        {
//            m_spawnedCreatures = new List<GameObject>();
//        }

//        var hexMapData = HexGenerator.HexMapData;
//        CreaturePrefabHolder.Instance.SyncEntityPrefabs();

//        m_spawnedCreatures.Clear();
//        foreach (var pair in SpawnRequests)
//        {
//            EntityGameObjectPair prefab = CreaturePrefabHolder.Instance.GetCreaturePrefab(pair.Type);

//            var center = HexMath.GetHexCenter(pair.CubeCoords, hexMapData.HexRadius);

//            float offset = prefab.GameObject.GetComponentInChildren<MeshFilter>().sharedMesh.bounds.extents.y;
//            float3 pos = center.to3D() + float3values.up * offset;
//            var spawnedEnemy = Instantiate(prefab.GameObject, pos, Quaternion.identity);
//            m_spawnedCreatures.Add(spawnedEnemy);
//        }
//    }

//    public void ClearLastSpawnedCreaturesEditor()
//    {
//        foreach (var creature in m_spawnedCreatures)
//        {
//            DestroyImmediate(creature);
//        }
//        m_spawnedCreatures.Clear();
//    }

//    void Update()
//    {
//        // can't do this in Start, since this data is initialized in Start as well
//        if (m_hasSpawnedCreatureEntities || m_spawnedCreatures == null)
//        {
//            return;
//        }

//        var hexOrigin = EntityQueryUtils.GetFirstEntityWithComponent<HexMapComponent>();
//        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
//        var hexMapComponent = entityManager.GetComponentData<HexMapComponent>(hexOrigin);
//        Slice2D<HexEntityBufferElement> coordToHex = HexUtils.GetCoordToHexArray(hexOrigin);

//        using (var blobAssetStore = new BlobAssetStore())
//        {
//            for (int i = 0; i < m_spawnedCreatures.Count; i++)
//            {
//                var coords = HexUtils.GetProjectionHexCoords(m_spawnedCreatures[i].transform.position, hexMapComponent);
//                var offsetCoords = HexMath.CubeToOffset(coords);
//                if (!coordToHex.IsValidIndex(offsetCoords))
//                {
//                    Debug.LogError("editor-placed enemy has invalid coords");
//                    continue;
//                }
//                var hex = coordToHex[offsetCoords].Hex;

//                var entity = GameObjectConversionUtility.ConvertGameObjectHierarchy(m_spawnedCreatures[i], GameObjectConversionSettings.FromWorld(entityManager.World, blobAssetStore));
//                entityManager.SyncHybrid(m_spawnedCreatures[i], entity);
                
//                entityManager.SetComponentData(entity, new CreatureComponent(entityManager.GetComponentData<CreatureComponent>(entity))
//                {
//                    Team = SpawnRequests[i].Team
//                });

//                entityManager.SetComponentData(hex, new BattleHexComponent(entityManager.GetComponentData<BattleHexComponent>(hex))
//                {
//                    OccupyingCreature = entity
//                });
//            }
//        }

//        m_hasSpawnedCreatureEntities = true;
//    }
//}
