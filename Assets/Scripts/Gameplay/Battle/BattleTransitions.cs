﻿using UnityEngine;

public static class BattleTransitions
{
    public static void OnBattleEnd(Team winningTeam)
    {
        Debug.Log($"team won: {winningTeam}");
    }
}
