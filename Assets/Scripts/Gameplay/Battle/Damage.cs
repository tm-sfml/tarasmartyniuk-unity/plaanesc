﻿using Unity.Entities;
using UnityEngine;

public static class Damage
{
    public static bool IsHexDamageable(Entity hex, EntityManager entityManager)
    {
        var battleHexComponent = entityManager.GetComponentData<BattleHexComponent>(hex);
        return battleHexComponent.Type == BattleHexType.TeamHealth || battleHexComponent.OccupyingCreature != Entity.Null;
    }

    // damages hex - either the creature on it, or teamHealth that is the hex itself
    // returns bool denoting if target was killed
    public static bool DoDamage(Entity hex, int damage, EntityManager entityManager)
    {
        var battleHexComponent = entityManager.GetComponentData<BattleHexComponent>(hex);
        bool result = false;
        if (battleHexComponent.Type == BattleHexType.TeamHealth)
        {
            var health = entityManager.GetComponentData<TeamHealthComponent>(hex);
            health.Health -= damage;
            entityManager.SetComponentData(hex, health);
            result = health.Health <= 0;
        }
        else
        {
            var creature = battleHexComponent.OccupyingCreature;
            if (creature == Entity.Null)
            {
                Debug.LogError("trying to damage hex that is not team health nor has a creature");
                return false;
            }
            var creatureData = entityManager.GetComponentData<CreatureComponent>(creature);
            creatureData.Health -= damage;
            entityManager.SetComponentData(creature, creatureData);
            result = creatureData.Health <= 0;
        }
        return result;
    }

    public static void DoDamageHandleKill(Entity hex, int damage, EntityManager entityManager)
    {
        var battleHexComponent = entityManager.GetComponentData<BattleHexComponent>(hex);
        bool killed = DoDamage(hex, damage, entityManager);
        if (killed)
        {
            if (battleHexComponent.Type == BattleHexType.TeamHealth)
            {
                var teamHealth = entityManager.GetComponentData<TeamHealthComponent>(hex);
                Team winningTeam = teamHealth.Team.Enemy();
                BattleTransitions.OnBattleEnd(winningTeam);
            }
            else
            {
                var hexMapEntity = EntityQueryUtils.GetSingletonEntity<HexMapComponent>();
                CreatureUtils.DestroyCreature(battleHexComponent.OccupyingCreature, entityManager, HexUtils.GetCoordToHexArray(hexMapEntity),
                    entityManager.GetComponentData<HexMapComponent>(hexMapEntity));
            }
        }
    }

}
