﻿using Unity.Entities;

// allows access to the card definition as well as all isntanced modifiers, used only during battle
public struct BattleCardData : IComponentData
{
    // unique for each card instance (in all 3 battle deck buffers)
    public int BattleDeckId;
    public CardDefinitionId DefinitionId;
}

public struct DrawPileBufferElement : IBufferElementData
{
    public BattleCardData Value;
}

public struct HandBufferElement : IBufferElementData
{
    public BattleCardData Value;
}

public struct DiscardPileBufferElement : IBufferElementData
{
    public BattleCardData Value;
}
