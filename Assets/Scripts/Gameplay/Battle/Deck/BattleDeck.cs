﻿using Unity.Entities;
using UnityEngine;

public static class BattleDeck
{
    // inits the battleDeckEntity's drawPile(DrawPileBufferElement) with the shuffled persistent deck of DeckBufferElement
    public static void SetupDeckForBattle<TBattleDataTag>(DeckId deckId, EntityManager entityManager)
        where TBattleDataTag : struct, IComponentData
    {
        var deck = entityManager.GetBuffer<DeckBufferElement>(DeckUtils.FindDeckEntity(deckId));

        Entity battleDataEntity = EntityQueryUtils.GetSingletonEntity<TBattleDataTag>();
        Entity battleDeckEntity = entityManager.FindChildWithComponent<BattleDeckConfigComponent>(battleDataEntity);
        var drawPile = entityManager.GetBuffer<DrawPileBufferElement>(battleDeckEntity);

        drawPile.Clear();
        for (int i = 0; i < deck.Length; i++)
        {
            drawPile.Add(new DrawPileBufferElement
            {
                Value = new BattleCardData
                {
                    DefinitionId = deck[i].DefinitionId,
                    BattleDeckId = i
                }
            });
        }
        drawPile.AsNativeArray().Shuffle();
    }

    // discard current and draw new, shuffle discard into draw if not enough cards in drawPile
    public static void CycleHand(Entity battleDeckEntity, EntityManager entityManager)
    {
        var drawPile = entityManager.GetBuffer<DrawPileBufferElement>(battleDeckEntity);
        var hand = entityManager.GetBuffer<HandBufferElement>(battleDeckEntity);
        var discardPile = entityManager.GetBuffer<DiscardPileBufferElement>(battleDeckEntity);
        var config = entityManager.GetComponentData<BattleDeckConfigComponent>(battleDeckEntity);

        discardPile.AddRange(hand.Reinterpret<DiscardPileBufferElement>().AsNativeArray());
        if (drawPile.Length < config.CardDrawAmount)
        {
            hand.CopyFrom(drawPile.Reinterpret<HandBufferElement>());
            int leftToDraw = config.CardDrawAmount - drawPile.Length;
            drawPile.CopyFrom(discardPile.Reinterpret<DrawPileBufferElement>());
            drawPile.AsNativeArray().Shuffle();
            discardPile.Clear();

            DynamicBufferQueueAdapter.DequeueAndAddN(drawPile.Reinterpret<HandBufferElement>(), leftToDraw, hand);
        }
        else
        {
            hand.Clear();
            DynamicBufferQueueAdapter.DequeueAndAddN(drawPile.Reinterpret<HandBufferElement>(), config.CardDrawAmount, hand);
        }
    }

    public static void CycleHand<TeamBattleDataTag>(EntityManager entityManager)
        where TeamBattleDataTag : struct, IComponentData
    {
        Entity battleDataEntity = EntityQueryUtils.GetSingletonEntity<TeamBattleDataTag>();
        Entity battleDeckEntity = entityManager.FindChildWithComponent<DrawPileBufferElement>(battleDataEntity);
        CycleHand(battleDeckEntity, entityManager);
    }

    // searches by id in draw, hand and discard 
    public static BattleCardData GetCardFromBattleDeck(int cardBattleDeckId, Entity battleDeckEntity, EntityManager entityManager)
    {
        var drawPile = entityManager.GetBuffer<DrawPileBufferElement>(battleDeckEntity);

        int index = drawPile.FindIndex((DrawPileBufferElement card) => card.Value.BattleDeckId == cardBattleDeckId);
        if (index != -1)
        {
            return drawPile[index].Value;
        }

        var hand = entityManager.GetBuffer<HandBufferElement>(battleDeckEntity);
        index = hand.FindIndex((HandBufferElement card) => card.Value.BattleDeckId == cardBattleDeckId);
        if (index != -1)
        {
            return hand[index].Value;
        }

        var discardPile = entityManager.GetBuffer<DiscardPileBufferElement>(battleDeckEntity);
        index = discardPile.FindIndex((DiscardPileBufferElement card) => card.Value.BattleDeckId == cardBattleDeckId);
        if (index != -1)
        {
            return discardPile[index].Value;
        }

        Debug.LogError($"could not find card with battle deck id: {cardBattleDeckId}");
        return default;
    }

    public static void DiscardCard(int battleDeckId, Entity battleDeckEntity, EntityManager entityManager)
    {
        var hand = entityManager.GetBuffer<HandBufferElement>(battleDeckEntity);
        int index = hand.FindIndex((HandBufferElement card) => card.Value.BattleDeckId == battleDeckId);
        if (index == -1)
        {
            Debug.LogError("card not in hand");
            return;
        }

        var discardPile = entityManager.GetBuffer<DiscardPileBufferElement>(battleDeckEntity);
        discardPile.Add(new DiscardPileBufferElement { Value = hand[index].Value });
        hand.RemoveAtSwapBack(index);
    }
}
