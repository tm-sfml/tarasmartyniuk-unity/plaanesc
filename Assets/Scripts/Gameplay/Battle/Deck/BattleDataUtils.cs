﻿using Unity.Entities;
using UnityEngine;

public static class BattleDataUtils
{
    public static Entity GetBattleDataEntityForTeam(Team team)
    {
        return team switch
        {
            Team.Player => EntityQueryUtils.GetSingletonEntity<PlayerBattleDataTag>(),
            Team.Enemy => EntityQueryUtils.GetSingletonEntity<EnemyBattleDataTag>(),
            _ => Entity.Null
        };
    }
}
