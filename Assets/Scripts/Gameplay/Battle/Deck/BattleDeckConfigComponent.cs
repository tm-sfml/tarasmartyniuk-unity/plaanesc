﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct BattleDeckConfigComponent : IComponentData
{
    // on start of each turn
    public int CardDrawAmount;
}
