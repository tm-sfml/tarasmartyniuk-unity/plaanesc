﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public static class CardEffects
{
    public static void UseCard(Entity selectedHex, Entity cardDefEntity, Team usingTeam, EntityManager entityManager)
    {
        var cardDef = entityManager.GetComponentData<CardDefinitionComponent>(cardDefEntity);

        switch (cardDef.CardBehavior)
        {
            case CardBehavior.Creature:
                CreatureUtils.SpawnCreature(selectedHex, cardDefEntity, usingTeam);
                break;
            case CardBehavior.Spell_DirectDamageSingleTarget:
                var spellComponent = entityManager.GetComponentData<Spell_DirectDamageDefinitionComponent>(cardDefEntity);
                Debug.Assert(Damage.IsHexDamageable(selectedHex, entityManager));
                Damage.DoDamageHandleKill(selectedHex, spellComponent.Damage, entityManager);
                break;
            default:
                Debug.LogError("invalid card behavior");
                break;
        }

        var battleDataEntity = BattleDataUtils.GetBattleDataEntityForTeam(usingTeam);
        var manaEntity = entityManager.FindChildWithComponent<ManaComponent>(battleDataEntity);
        var manaComponent = entityManager.GetComponentData<ManaComponent>(manaEntity);
        Debug.Assert(manaComponent.CurrentMana >= cardDef.ManaCost);
        manaComponent.CurrentMana -= cardDef.ManaCost;
        entityManager.SetComponentData(manaEntity, manaComponent);
    }

    public static bool IsHexAllowedForCard(Entity hex, CardBehavior cardBehavior)
    {
        bool result = false;
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        var battleHexComponent = entityManager.GetComponentData<BattleHexComponent>(hex);
        switch (cardBehavior)
        {
            case CardBehavior.Creature:
                result = battleHexComponent.IsPassable && battleHexComponent.OccupyingCreature == Entity.Null;
                break;
            case CardBehavior.Spell_DirectDamageSingleTarget:
                result = Damage.IsHexDamageable(hex, entityManager);
                break;
            default:
                Debug.LogError("invalid allowedTarget");
                break;
        }
        return result;
    }
}
