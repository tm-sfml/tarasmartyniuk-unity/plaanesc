﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct TeamHealthComponent : IComponentData
{
    public int Health;
    public Team Team;
}
