﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct ManaComponent : IComponentData
{
    // amount we get on every turn start
    public int CurrentMaxMana;
    public int CurrentMana;

    public ManaComponent(ManaComponent other) => this = other;
}
